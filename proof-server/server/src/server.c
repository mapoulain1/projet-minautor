#include "server.h"

#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "player.h"
#include "utils/app.h"
#include "utils/bool.h"
#include "utils/color.h"

player_t players[MAX_CLIENTS];
int numberOfPlayers = 0;

void serverStart(int port) {
	int index = 0;
	int clientsSD[MAX_CLIENTS];
	pthread_t threadClient[MAX_CLIENTS];
	int sockfd;
	struct sockaddr_in serverAddress;
	bool running = true;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons(port);

	serverSpeak(ok, "Running on version : %d", APP_VERSION);
	serverSpeak(ok, "Started on port : %d", port);

	if (bind(sockfd, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) != 0) {
		serverSpeak(error, "Can't bind on port %d", port);
		return;
	}

	listen(sockfd, MAX_CLIENTS);
	serverSpeak(ok, "Listening for clients");

	while (running) {
		clientsSD[index] = accept(sockfd, NULL, NULL);
		if (clientsSD[index] != 0) {
			numberOfPlayers++;
			pthread_create(&threadClient[index], NULL, (void *(*)(void *))handleClient, &clientsSD[index]);
			index++;
		}
	}
	serverSpeak(ok, "Shutdown");
}

void *handleClient(int *sd) {
	char tmp[LONG_BUFFER];
	char tmpNumberOfPlayer[SHORT_BUFFER];
	char tmpPlayerID[SHORT_BUFFER];
	char tmpAppVersion[SHORT_BUFFER];
	player_t player;
	bool running = true;
	int playerID = numberOfPlayers - 1;

	printf(GRE "[THREAD %d] Started\n" RESET, playerID);
	threadSpeak(playerID, ok, "Started");

	sprintf(tmpAppVersion, "%d\n", APP_VERSION);
	if (send(*sd, tmpAppVersion, SHORT_BUFFER, 0) < 0) {
		threadSpeak(playerID, error, "Can't send app version");
		running = false;
	}

	sprintf(tmpPlayerID, "%d\n", playerID);
	if (send(*sd, tmpPlayerID, SHORT_BUFFER, 0) < 0) {
		threadSpeak(playerID, error, "Can't send player ID");
		running = false;
	}

	while (running) {
		read(*sd, tmp, LONG_BUFFER);
		playerDeserialize(&player, tmp);
		players[playerID] = player;
		sprintf(tmpNumberOfPlayer, "%d\n", numberOfPlayers);
		if (send(*sd, tmpNumberOfPlayer, SHORT_BUFFER, 0) < 0) {
			fprintf(stderr, RED "[THREAD %d] Can't send number of player\n" RESET, playerID);
			threadSpeak(playerID, error, "Can't send number of player");
			running = false;
		} else {
			for (int i = 0; i < MAX_CLIENTS; i++) {
				playerSerialize(&players[i], tmp);
				if (send(*sd, tmp, LONG_BUFFER, 0) < 0) {
					threadSpeak(playerID, error, "Can't send players positions");
					running = false;
				}
			}
		}
	}
	numberOfPlayers--;
    
	threadSpeak(playerID, ok, "Client disconnected");
	return NULL;
}

void signalHandler(int signo) {
	if (signo == SIGPIPE) {
		printf(BLU "[SERVER] Client disconnected\n" RESET);
	}
}

void serverSpeak(message_type_t type, const char *message, ...) {
	va_list args;
	va_start(args, message);
	switch (type) {
	case ok:
		printf(GRE);
		break;
	case error:
		printf(GRE);
		break;
	case warning:
		printf(GRE);
		break;
	}
	printf("[SERVER] ");
	vprintf(message, args);
	printf("\n" RESET);
	va_end(args);
}

void threadSpeak(int id, message_type_t type, const char *message, ...) {
	va_list args;
	va_start(args, message);
	switch (type) {
	case ok:
		printf(GRE);
		break;
	case error:
		printf(GRE);
		break;
	case warning:
		printf(GRE);
		break;
	}
	printf("[THREAD %d] ", id);
	vprintf(message, args);
	printf("\n" RESET);
	va_end(args);
}