#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "server.h"





int main(int argc, char const *argv[]) {
	if (argc != 2) {
		printf("Usage: %s <port>\n", argv[0]);
		return 1;
	}
	if (signal(SIGPIPE, signalHandler) == SIG_ERR){
		printf("can't catch SIGINT\n");
	}

	serverStart(atoi(argv[1]));
	return 0;
}
