#ifndef PLAYER_H_
#define PLAYER_H_

#include "utils/bool.h"

typedef struct {
	int id;
	int x;
	int y;
	int w;
	int h;
	bool connected;
} player_t;

void playerSerialize(player_t *player, char *out);
void playerDeserialize(player_t *out, char *data);

#endif