#include "player.h"

#include <stdio.h>
#include <stdlib.h>


void playerSerialize(player_t *player, char *out){
    sprintf(out,"%d %d %d %d %d %d\n",player->id,player->x,player->y,player->w,player->h,player->connected);
}

void playerDeserialize(player_t *out, char *data){
    sscanf(data,"%d %d %d %d %d %d",&(out->id),&(out->x),&(out->y),&(out->w),&(out->h),(int*)&(out->connected));
}