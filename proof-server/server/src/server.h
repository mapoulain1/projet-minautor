#ifndef SERVER_H_
#define SERVER_H_

#define MAX_CLIENTS 10
#define LONG_BUFFER 64
#define SHORT_BUFFER 8

typedef enum {
	ok,
	error,
	warning
} message_type_t;

void serverStart(int port);
void *handleClient(int *sd);
void signalHandler(int signo);
void serverSpeak(message_type_t type, const char *message, ...);
void threadSpeak(int id, message_type_t type, const char *message, ...);

#endif