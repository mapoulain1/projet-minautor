#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/graphics.h"
#include "player.h"
#include "server.h"
#include "utils/bool.h"

#define GAME_SLOWDOWN 3
#define MAX_PLAYER 10

server_t server;
player_t players[MAX_PLAYER];
int numberOfPlayer = 0;
config_t gameConfig;
int frameMS = 16;

void gameSetup(config_t config) {
	srand(time(NULL));
	gameConfig.playerID = config.playerID;
	gameConfig.port = config.port;
	strcpy(gameConfig.server, config.server);
}

void gameStart(SDL_Window *window) {
	bool running = true;
	SDL_Event event;
	SDL_Renderer *renderer = SDL_GetRenderer(window);

	server = serverConnect(gameConfig.server, gameConfig.port);
	if (!server.online) {
		return;
	}
	gameConfig.playerID = serverGetPlayerID(server);
	if (gameConfig.playerID == -1) {
		fprintf(stderr, "Error can't get player ID\n");
		return;
	}

	playerInit(&players[gameConfig.playerID], 10, 15, gameConfig.playerID);

	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					running = false;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					gameRenderGame(window);
					break;
				}
				break;
			case SDL_QUIT:
				running = false;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					running = false;
					break;
				}
				break;
			}
		}

		const Uint8 *keystates = SDL_GetKeyboardState(NULL);
		if (keystates[SDL_SCANCODE_UP])
			players[gameConfig.playerID].y -= 8;
		if (keystates[SDL_SCANCODE_DOWN])
			players[gameConfig.playerID].y += 8;
		if (keystates[SDL_SCANCODE_LEFT])
			players[gameConfig.playerID].x -= 8;
		if (keystates[SDL_SCANCODE_RIGHT])
			players[gameConfig.playerID].x += 8;

		serverSend(server, &players[gameConfig.playerID]);
		serverRead(server, players, &numberOfPlayer);

		gameRenderGame(window);
		SDL_RenderPresent(renderer);
		SDL_Delay(frameMS);
	}

	serverDisconnect(server);
	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
}

void gameRenderGame(SDL_Window *window) {
	int windowWidth, windowHeight;
	SDL_Rect rect;

	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer);
	for (int i = 0; i < MAX_CLIENTS; i++) {
		if (players[i].connected) {
			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
			if (i == 0)
				SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
			if (i == 1)
				SDL_SetRenderDrawColor(renderer, 0, 255, 0, 0);
			if (i == 2)
				SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);

			rect.x = players[i].x;
			rect.y = players[i].y;
			rect.w = players[i].w;
			rect.h = players[i].h;
			SDL_RenderFillRect(renderer, &rect);
		}
	}
}
