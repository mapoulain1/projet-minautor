#include "player.h"

#include <stdio.h>
#include <stdlib.h>

void playerInit(player_t *out, int x, int y, int id){
    out->x=x;
    out->y=y;
    out->h=50;
    out->w=50;
    out->id = id;
    out->connected = true;
}

void playerSerialize(player_t *player, char *out){
    sprintf(out,"%d %d %d %d %d %d\n",player->id,player->x,player->y,player->w,player->h,player->connected);
}

void playerDeserialize(player_t *out, char *data){
    if(sscanf(data,"%d %d %d %d %d %d",&(out->id),&(out->x),&(out->y),&(out->w),&(out->h),(int*)&(out->connected)) != 6){
        out->connected=false;
    }
}