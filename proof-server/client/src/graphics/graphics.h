#ifndef GRAPHICS_H
#define GRAPHICS_H


#include <SDL2/SDL.h>

void gcsInit(void);
void gcsFree(void);

void gcsNewWindow(const char *title, int width, int height, SDL_Window **out);
void gcsFreeWindow(SDL_Window **window);

void gcsNewRenderer(SDL_Window *window, SDL_Renderer **out);
void gcsFreeRenderer(SDL_Renderer **renderer);

void gcsClearBackground(SDL_Renderer *renderer);

#endif