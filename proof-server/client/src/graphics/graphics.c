#include "graphics.h"

#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_image.h>

void gcsInit(void) {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
		printf("ERROR SDL_INIT\n");
	if (IMG_Init(IMG_INIT_PNG) == 0)
		printf("ERROR IMG_INIT\n");

}

void gcsFree(void) {
	SDL_Quit();
	IMG_Quit();
}

void gcsNewWindow(const char *title, int width, int height, SDL_Window **out) {
	*out = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE);
}

void gcsFreeWindow(SDL_Window **window) {
	SDL_DestroyWindow(*window);
	*window = NULL;
}

void gcsNewRenderer(SDL_Window *window, SDL_Renderer **out) {
	*out = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void gcsFreeRenderer(SDL_Renderer **renderer) {
	SDL_DestroyRenderer(*renderer);
	*renderer = NULL;
}

void gcsClearBackground(SDL_Renderer *renderer) {
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
}
