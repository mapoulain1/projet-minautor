

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "game.h"
#include "graphics/graphics.h"
#include "server.h"
#include "utils/config.h"


int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	SDL_Window *window;
	SDL_Renderer *renderer;

	if(argc != 3){
		printf("Usage : ./out <IP> <PORT>\n");
		return 1;
	}

	config_t config = {"",atoi(argv[2]), 0};
	strcpy(config.server,argv[1]);

	srand(time(NULL));

	gcsInit();
	gcsNewWindow("Client", 400, 400, &window);
	gcsNewRenderer(window, &renderer);

	gameSetup(config);
	gameStart(window);

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();

	return 0;
}
