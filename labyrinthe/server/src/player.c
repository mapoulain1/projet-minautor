#include "player.h"

#include <stdio.h>
#include <stdlib.h>

void playerSerialize(player_t *player, char *out) {
	sprintf(out, "%d %.2f %.2f %d %d %d %.2f %d %d %d %d %d %u\n", player->id, player->x, player->y, player->w, player->h, player->connected, player->health, player->direction, player->lookingLeft, player->moving, player->usingSpecialPower, player->lastUsedSpecialPower, player->cdPower);
}

void playerDeserialize(player_t *out, char *data) {
	sscanf(data, "%d %f %f %d %d %d %f %d %d %d %d %d %u", &(out->id), &(out->x), &(out->y), &(out->w), &(out->h), (int *)&(out->connected), &(out->health), (int *)&(out->direction), (int *)&(out->lookingLeft), (int *)&(out->moving), (int *)&(out->usingSpecialPower), &(out->lastUsedSpecialPower), &(out->cdPower));
}
