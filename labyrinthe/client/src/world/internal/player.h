#ifndef PLAYER_H_
#define PLAYER_H_

#include "../../utils/bool.h"
#include "../../utils/direction.h"
#include "labyrinthe.h"
#include "item.h"

#include <SDL2/SDL.h>

#define PLAYER_DEFAULT_HEALTH 100
#define PLAYER_DEFAULT_ID 1
#define PLAYER_WIDTH 64
#define PLAYER_HEIGHT 80
#define PLAYER_DAMAGE 0.5
#define PLAYER_SPEED 0.04
#define PLAYER_GHOST_SPEED_BOOST 1.5
#define DUR_POWER 3000
#define CD_POWER 5000

#define PLAYER_COLLISION_HELPER_X 10
#define PLAYER_COLLISION_HELPER_Y 50

#define PLAYER_ID_GHOST 0

#define PLAYER_LIGHT_HITBOX_OFF_X 20
#define PLAYER_LIGHT_HITBOX_OFF_Y 20
#define PLAYER_LIGHT_HITBOX_WIDTH PLAYER_WIDTH * 4
#define PLAYER_LIGHT_HITBOX_HEIGHT PLAYER_HEIGHT * 2

#define PLAYER_GHOST_DISTANCE_AMBIANCE 350

typedef struct {
  float health;
  float x;
  float y;
  int w;
  int h;
  int id;
  direction_t direction;
  bool moving;
  bool lookingLeft;
  int xWorldPosition;
  int yWorldPosition;
  bool connected;
  bool usingSpecialPower;
  int lastUsedSpecialPower;
  unsigned int cdPower;
} player_t;

#include "../../graphics/camera.h"

void playerInit(player_t *out, int xWorld, int yWorld, int tileSize, int id);
void playerInflictDamage(player_t *out);
void playerKill(player_t *out);
void playerUpdateCollision(SDL_Renderer *renderer, player_t *out, labyrinthe_t *laby, direction_t direction, bool moving, camera_t *camera);
void playerApplyDamageGhost(player_t *players, int playerID);
void playerApplyDamageLight(SDL_Renderer *renderer, player_t *players, int playerID, camera_t *camera);
void playerApplySoundeffects(player_t *players, int playerID);

void playerStop(player_t *out);
void playerUseSpecialPower(player_t *out);
void playerSpecialPowerCheck(player_t *out);
player_t *playerAllocPlayers(int number);
void playerCopyFromThread(player_t *dest, player_t *thread, int playerID);

void playerSerialize(player_t *player, char *out);
void playerDeserialize(player_t *out, char *data);

void playerCollisionWithItem (player_t * player, item_t * items);


#endif
