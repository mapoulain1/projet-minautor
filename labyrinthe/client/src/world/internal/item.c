#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../../utils/error.h"
#include "../../utils/math.h"
#include "item.h"

void itemInit(item_t *out, int xWorld, int yWorld, int tileSize) {
	if (out) {
		out->w = ITEM_WIDTH;
		out->h = ITEM_HEIGHT;
		out->xWorldPosition = xWorld;
		out->yWorldPosition = yWorld;
		out->x = xWorld * tileSize + (tileSize - ITEM_WIDTH) / 2 + (randomD() * 2 - 1) * tileSize / 2;
		out->y = yWorld * tileSize + (tileSize - ITEM_HEIGHT) / 2 + (randomD() * 2 - 1) * tileSize / 2;
		out->exist = true;
	} else {
		logError(NULL_POINTER);
	}
}

void itemsList(item_t *items, labyrinthe_t *laby, int tileSize) {
	int xWorld, yWorld;
	for (int i = 0; i < MAX_ITEMS; i++) {
		xWorld = rand() % laby->size;
		yWorld = rand() % laby->size;
		itemInit(&items[i], xWorld, yWorld, tileSize);
	}
}
