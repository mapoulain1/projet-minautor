#include "player.h"

#include <SDL2/SDL.h>
#include <stdio.h>

#include "../../graphics/musicPlayer.h"
#include "../../process/collisionHandler.h"
#include "../../server/server.h"
#include "../../utils/error.h"
#include "../../utils/math.h"
#include "item.h"
#include "labyrinthe.h"

bool playSoundeffectPower = true;

void playerInit(player_t *out, int xWorld, int yWorld, int tileSize, int id) {
	if (out) {
		out->health = PLAYER_DEFAULT_HEALTH;
		out->id = id;
		out->w = PLAYER_WIDTH;
		out->h = PLAYER_HEIGHT;
		out->xWorldPosition = xWorld;
		out->yWorldPosition = yWorld;
		out->x = xWorld * tileSize + (tileSize - PLAYER_WIDTH) / 2;
		out->y = yWorld * tileSize + (tileSize - PLAYER_HEIGHT) / 2;
		out->direction = DIR_NONE;
		out->moving = false;
		out->lookingLeft = false;
		out->connected = true;
		out->usingSpecialPower = false;
		out->lastUsedSpecialPower = 0;
		out->cdPower = 10000;
	} else
		logError(NULL_POINTER);
}

void playerInflictDamage(player_t *out) {
	if (out) {
		out->health = clampDouble(out->health - PLAYER_DAMAGE, 0, PLAYER_DEFAULT_HEALTH);
		musicPlayDamage(out->id);
	}
}

void playerKill(player_t *out) {
	if (out) {
		out->health = 0;
	}
}

void playerUpdateCollision(SDL_Renderer *renderer, player_t *out, labyrinthe_t *laby, direction_t direction, bool moving, camera_t *camera) {
	if (out) {
		out->moving = moving;
		out->direction = direction;
		if (moving) {

			SDL_Rect rect = {out->x + 0.2 * PLAYER_HEIGHT, out->y + 0.8 * PLAYER_HEIGHT, 0.6 * PLAYER_WIDTH, 0.2 * PLAYER_HEIGHT};
			SDL_Rect walls[4 * 9];
			int index = 0;

			collisionGenerateRects(walls, &index, laby, out, camera);

			if (camera->renderHitbox) {
				SDL_SetRenderDrawBlendMode(renderer,SDL_BLENDMODE_BLEND);
				SDL_SetRenderDrawColor(renderer, 0, 255, 0, 200);
				SDL_RenderFillRect(renderer, &rect);
				SDL_SetRenderDrawColor(renderer, 0, 0, 255, 150);
				for (int h = 0; h < index; h++) {
					SDL_RenderFillRect(renderer, &walls[h]);
					if (SDL_HasIntersection(&rect, &walls[h])) {
						SDL_SetRenderDrawColor(renderer, 255, 0, 0, 150);
						SDL_RenderFillRect(renderer, &walls[h]);
						SDL_SetRenderDrawColor(renderer, 0, 0, 255, 150);
					}
				}
			}

			if (!collidedRectWithRects(walls, index, &rect, direction, camera, out->id) || (out->id == PLAYER_ID_GHOST && out->usingSpecialPower) || out->health <= 0) {
				if (direction == DIR_DOWN) {
					out->y += PLAYER_SPEED * camera->tileSize * (out->id == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
				}
				if (direction == DIR_UP) {
					out->y -= PLAYER_SPEED * camera->tileSize * (out->id == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
				}
				if (direction == DIR_RIGHT) {
					out->x += PLAYER_SPEED * camera->tileSize * (out->id == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
					out->lookingLeft = false;
				}
				if (direction == DIR_LEFT) {
					out->x -= PLAYER_SPEED * camera->tileSize * (out->id == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
					out->lookingLeft = true;
				}
			}
		}
	}
}

void playerApplyDamageGhost(player_t *players, int playerID) {
	if (playerID != PLAYER_ID_GHOST && players && players[PLAYER_ID_GHOST].connected && players[playerID].connected) {
		SDL_Rect knight = {0, 0, 0.6 * PLAYER_WIDTH, 0.6 * PLAYER_HEIGHT};
		SDL_Rect ghost = {players[PLAYER_ID_GHOST].x + 0.2 * PLAYER_WIDTH, players[PLAYER_ID_GHOST].y + 0.4 * PLAYER_HEIGHT, 0.6 * PLAYER_WIDTH, 0.4 * PLAYER_HEIGHT};
		knight.x = players[playerID].x + 0.2 * PLAYER_WIDTH;
		knight.y = players[playerID].y + 0.4 * PLAYER_HEIGHT;
		if (SDL_HasIntersection(&knight, &ghost)) {
			playerInflictDamage(&(players[playerID]));
		}

		if (players[playerID].health <= 0.1) {
			if (players[playerID].h > 0) {
				players[playerID].y++;
			}
			players[playerID].h = clamp(players[playerID].h - 2, 0, INT32_MAX);
			if (players[playerID].h < 30) {
				players[playerID].w = clamp(players[playerID].w - 2, 0, INT32_MAX);
				if (players[playerID].w > 0) {
					players[playerID].x++;
				}
			}
		}
	}
}

void playerApplyDamageLight(SDL_Renderer *renderer, player_t *players, int playerID, camera_t *camera) {
	if (playerID == PLAYER_ID_GHOST && players && players[PLAYER_ID_GHOST].connected && !players[PLAYER_ID_GHOST].usingSpecialPower) {
		SDL_Rect light;
		SDL_Rect ghost = {players[PLAYER_ID_GHOST].x + 0.2 * PLAYER_HEIGHT, players[PLAYER_ID_GHOST].y + 0.8 * PLAYER_HEIGHT, 0.6 * PLAYER_WIDTH, 0.2 * PLAYER_HEIGHT};

		for (int i = 1; i < MAX_CLIENTS; i++) {
			if (players[i].connected && players[i].health > 0) {
				switch (players[i].direction) {
				case DIR_UP:
					light.x = players[i].x - PLAYER_LIGHT_HITBOX_OFF_X;
					light.y = players[i].y - PLAYER_LIGHT_HITBOX_OFF_Y - PLAYER_LIGHT_HITBOX_WIDTH;
					light.w = PLAYER_LIGHT_HITBOX_HEIGHT;
					light.h = PLAYER_LIGHT_HITBOX_WIDTH;
					break;
				case DIR_DOWN:
					light.x = players[i].x - PLAYER_LIGHT_HITBOX_OFF_X;
					light.y = players[i].y + PLAYER_LIGHT_HITBOX_OFF_Y + PLAYER_HEIGHT;
					light.w = PLAYER_LIGHT_HITBOX_HEIGHT;
					light.h = PLAYER_LIGHT_HITBOX_WIDTH;
					break;
				case DIR_RIGHT:
					light.x = players[i].x + PLAYER_LIGHT_HITBOX_OFF_X + PLAYER_WIDTH;
					light.y = players[i].y - PLAYER_LIGHT_HITBOX_OFF_Y;
					light.w = PLAYER_LIGHT_HITBOX_WIDTH;
					light.h = PLAYER_LIGHT_HITBOX_HEIGHT;
					break;
				case DIR_LEFT:
					light.x = players[i].x - PLAYER_LIGHT_HITBOX_OFF_X - PLAYER_LIGHT_HITBOX_WIDTH;
					light.y = players[i].y - PLAYER_LIGHT_HITBOX_OFF_Y;
					light.w = PLAYER_LIGHT_HITBOX_WIDTH;
					light.h = PLAYER_LIGHT_HITBOX_HEIGHT;
					break;
				case DIR_NONE:
					light.x = 0;
					light.y = 0;
					light.w = 0;
					light.h = 0;
					break;
				}
				if (camera->renderHitbox) {
					SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
					SDL_SetRenderDrawColor(renderer, 240, 200, 200, 150);
					SDL_RenderFillRect(renderer, &light);
				}

				if (SDL_HasIntersection(&light, &ghost)) {
					playerInflictDamage(&(players[PLAYER_ID_GHOST]));
					if (players[PLAYER_ID_GHOST].health <= 0.1) {
						if (players[PLAYER_ID_GHOST].h > 0) {
							players[PLAYER_ID_GHOST].y++;
						}
						players[PLAYER_ID_GHOST].h = clamp(players[PLAYER_ID_GHOST].h - 2, 0, INT32_MAX);
						if (players[PLAYER_ID_GHOST].h < 30) {
							players[PLAYER_ID_GHOST].w = clamp(players[PLAYER_ID_GHOST].w - 2, 0, INT32_MAX);
							if (players[PLAYER_ID_GHOST].w > 0) {
								players[PLAYER_ID_GHOST].x++;
							}
						}
					}
				}
			}
		}
	}
}

void playerApplySoundeffects(player_t *players, int playerID) {
	if (playerID == PLAYER_ID_GHOST) {
		if (playSoundeffectPower && players[playerID].usingSpecialPower) {
			playSoundeffectPower = false;
			musicPlayBoo();
		} else {
			playSoundeffectPower = true;
		}
	} else if (players[PLAYER_ID_GHOST].connected) {
		int x1 = players[PLAYER_ID_GHOST].x;
		int y1 = players[PLAYER_ID_GHOST].y;
		int x2 = players[playerID].x;
		int y2 = players[playerID].y;
		double sum = (double)(x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
		double distance = sqrt(sum);
		if (distance < PLAYER_GHOST_DISTANCE_AMBIANCE)
			musicPlayAmbiante();
	}
}

void playerStop(player_t *out) {
	if (out) {
		out->moving = false;
	}
}

void playerUseSpecialPower(player_t *out) {
	if (out && SDL_GetTicks() - out->lastUsedSpecialPower > out->cdPower) {

		out->usingSpecialPower = true;
		out->lastUsedSpecialPower = SDL_GetTicks();
		if (out->id == PLAYER_ID_GHOST)
			musicPlayBoo();
	}
}

void playerSpecialPowerCheck(player_t *out) {
	if (out) {
		if (SDL_GetTicks() - out->lastUsedSpecialPower > DUR_POWER) {
			out->usingSpecialPower = false;
		}
	}
}

player_t *playerAllocPlayers(int number) {
	player_t *players = malloc(sizeof(player_t) * number);
	if (!players) {
		logError(ALLOC_ERROR);
	}
	return players;
}

void playerCopyFromThread(player_t *dest, player_t *thread, int playerID) {
	if (dest && thread) {
		thread[playerID] = dest[playerID];
		for (int i = 0; i < MAX_CLIENTS; i++) {
			dest[i] = thread[i];
		}
	}
}

void playerSerialize(player_t *player, char *out) {
	sprintf(out, "%d %.2f %.2f %d %d %d %.2f %d %d %d %d %d %u\n", player->id, player->x, player->y, player->w, player->h, player->connected, player->health, player->direction, player->lookingLeft, player->moving, player->usingSpecialPower, player->lastUsedSpecialPower, player->cdPower);
}

void playerDeserialize(player_t *out, char *data) {
	if (sscanf(data, "%d %f %f %d %d %d %f %d %d %d %d %d %u", &(out->id), &(out->x), &(out->y), &(out->w), &(out->h), (int *)&(out->connected), &(out->health), (int *)&(out->direction), (int *)&(out->lookingLeft), (int *)&(out->moving), (int *)&(out->usingSpecialPower), &(out->lastUsedSpecialPower), &(out->cdPower)) != 13) {
		out->connected = false;
	}
}

void playerCollisionWithItem(player_t *player, item_t *items) {
	if (player->id != PLAYER_ID_GHOST && player->connected && player->health > 0) {
		SDL_Rect rectP = {player->x + 0.2 * PLAYER_HEIGHT, player->y + 0.8 * PLAYER_HEIGHT, 0.6 * PLAYER_WIDTH, 0.2 * PLAYER_HEIGHT};
		for (int j = 0; j < MAX_ITEMS; j++) {
			if (items[j].exist) {
				SDL_Rect rectI = {items[j].x + 0.2 * ITEM_HEIGHT, items[j].y + 0.2 * ITEM_HEIGHT, 0.6 * ITEM_WIDTH, 0.6 * ITEM_HEIGHT};
				if (SDL_HasIntersection(&rectP, &rectI)) {
					items[j].exist = false;
					player->cdPower -= 1000;
				}
			}
		}
	}
}
