#ifndef PLAYERRENDERER_H_
#define PLAYERRENDERER_H_

#include <SDL2/SDL.h>

#include "../../graphics/camera.h"
#include "../internal/player.h"

#define PLAYERRENDER_NUMBER_OF_SPRITES 3
#define PLAYERRENDER_PATH_TO_TEXTURE "textures/player/"
#define PLAYERRENDER_SPRITE_WIDTH 16
#define PLAYERRENDER_SPRITE_HEIGHT 20
#define PLAYERRENDER_SPRITE_DIFFERENT 11

#define PLAYERRENDER_PATH_TO_LIGHTMASK "textures/light/light-mask.png"
#define PLAYERRENDER_LIGHTMASK_SIZE 3840
#define PLAYERRENDER_LIGHTMASK_SMOOTH 10

#define PLAYERRENDER_PATH_TO_STAR "textures/particule/stars.png"
#define PLAYERRENDER_STAR_SPRIT_HEIGHT 9
#define PLAYERRENDER_STAR_SPRIT_WIDTH 9
#define PLAYERRENDER_STAR_SPRIT_NUMBER 6
#define PLAYERRENDER_STAR_SPRIT_OFFSET_X 70
#define PLAYERRENDER_STAR_SPRIT_OFFSET_Y 35

#define PLAYERRENDER_PATH_TO_FONT "fonts/font.tff"


#define PLAYERRENDER_FRAMESKIP 5

void renderPlayerInit(SDL_Renderer *renderer);
void renderPlayerFree(void);
void renderPlayerDebug(SDL_Renderer *renderer, player_t *player);
void renderPlayer(SDL_Renderer *renderer, player_t *player);
void renderLight(SDL_Renderer *renderer, player_t *player);
void renderPowerStar(SDL_Renderer *renderer, player_t *player);
void playerRenderGameover(SDL_Renderer *renderer,int windowWidth, int windowHeight);


#endif