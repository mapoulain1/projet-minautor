#include "playerRenderer.h"

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../../server/server.h"
#include "../../utils/error.h"
#include "../../utils/math.h"
#include "healthbarRenderer.h"

struct {
	SDL_Rect sources[MAX_CLIENTS][PLAYERRENDER_NUMBER_OF_SPRITES];
	SDL_Texture *texture[MAX_CLIENTS];
	SDL_Texture *light;
	int animationFrameCount[MAX_CLIENTS];
	int currentFrame[MAX_CLIENTS];
	int angleAnimationLight[MAX_CLIENTS];
	SDL_Rect sourcesStar[PLAYERRENDER_STAR_SPRIT_NUMBER];
	SDL_Texture *textureStar;
	int animationFrameCountStar;
	int currentFrameStar;
	SDL_Surface *gameover;
	TTF_Font *font;
} playerAssets;

void renderPlayerInit(SDL_Renderer *renderer) {
	char tmp[128];
	for (int i = 0; i < MAX_CLIENTS; i++) {
		sprintf(tmp, "%splayer%d.png", PLAYERRENDER_PATH_TO_TEXTURE, i % PLAYERRENDER_SPRITE_DIFFERENT);
		playerAssets.texture[i] = IMG_LoadTexture(renderer, tmp);
		playerAssets.animationFrameCount[i] = 0;
		playerAssets.currentFrame[i] = 0;
		if (!playerAssets.texture[i])
			logError(FILE_NOT_FOUND);
		for (int h = 0; h < PLAYERRENDER_NUMBER_OF_SPRITES; h++) {
			playerAssets.sources[i][h].h = PLAYERRENDER_SPRITE_HEIGHT;
			playerAssets.sources[i][h].w = PLAYERRENDER_SPRITE_WIDTH;
			playerAssets.sources[i][h].x = h * PLAYERRENDER_SPRITE_WIDTH;
			playerAssets.sources[i][h].y = 0;
		}
		playerAssets.angleAnimationLight[i] = 0;
	}
	playerAssets.light = IMG_LoadTexture(renderer, PLAYERRENDER_PATH_TO_LIGHTMASK);
	if (!playerAssets.light)
		logError(FILE_NOT_FOUND);

	playerAssets.textureStar = IMG_LoadTexture(renderer, PLAYERRENDER_PATH_TO_STAR);
	if (!playerAssets.textureStar)
		logError(FILE_NOT_FOUND);
	for (int i = 0; i < PLAYERRENDER_STAR_SPRIT_NUMBER; i++) {
		playerAssets.sourcesStar[i].x = i * PLAYERRENDER_STAR_SPRIT_WIDTH;
		playerAssets.sourcesStar[i].y = 0;
		playerAssets.sourcesStar[i].w = PLAYERRENDER_STAR_SPRIT_WIDTH;
		playerAssets.sourcesStar[i].h = PLAYERRENDER_STAR_SPRIT_HEIGHT;
	}
	playerAssets.currentFrameStar = 0;
	playerAssets.animationFrameCountStar = 0;

	playerAssets.font = TTF_OpenFont(PLAYERRENDER_PATH_TO_FONT, 24);
	if (!playerAssets.font) {
		printf("%s\n", TTF_GetError());
		return;
	}
	playerAssets.gameover = TTF_RenderText_Solid(playerAssets.font, "GAME OVER", (SDL_Color){255, 255, 255, 255});
	if (!playerAssets.gameover) {
		printf("%s\n", TTF_GetError());
	}
}

void renderPlayerFree(void) {
	for (int i = 0; i < MAX_CLIENTS; i++) {
		SDL_DestroyTexture(playerAssets.texture[i]);
	}
	SDL_DestroyTexture(playerAssets.light);
}

void renderPlayerDebug(SDL_Renderer *renderer, player_t *player) {
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
	SDL_Rect dest = {(int)player->x, (int)player->y, PLAYER_WIDTH, PLAYER_HEIGHT};
	SDL_RenderDrawRect(renderer, &dest);
}

void renderPlayer(SDL_Renderer *renderer, player_t *player) {
	SDL_RendererFlip flip = SDL_FLIP_NONE;
	SDL_Rect dest = {(int)player->x, (int)player->y, player->w, player->h};
	int id = player->id;

	if (player->lookingLeft)
		flip = SDL_FLIP_HORIZONTAL;

	if (player->id == PLAYER_ID_GHOST && player->usingSpecialPower)
		SDL_SetTextureAlphaMod(playerAssets.texture[PLAYER_ID_GHOST], 100);

	SDL_RenderCopyEx(renderer, playerAssets.texture[id], &playerAssets.sources[id][playerAssets.currentFrame[id]], &dest, 0.0, NULL, flip);
	SDL_SetTextureAlphaMod(playerAssets.texture[PLAYER_ID_GHOST], 255);

	if ((player->id == PLAYER_ID_GHOST && playerAssets.animationFrameCount[id] == 0) || (player->moving && playerAssets.animationFrameCount[id] == 0)) {
		playerAssets.currentFrame[id] = (playerAssets.currentFrame[id] + 1) % PLAYERRENDER_NUMBER_OF_SPRITES;
	}
	playerAssets.animationFrameCount[id] = (playerAssets.animationFrameCount[id] + 1) % PLAYERRENDER_FRAMESKIP;
	if (player->health > 0) {
		healthbarRender(renderer, player);
	}
}

void playerRenderGameover(SDL_Renderer *renderer,int windowWidth, int windowHeight) {
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, playerAssets.gameover);
	SDL_Rect dest = {windowWidth / 2 - 200, windowHeight / 2 - 50, 400, 100};
	SDL_RenderCopy(renderer, texture, NULL, &dest);
}

void renderLight(SDL_Renderer *renderer, player_t *player) {
	if (player && player->id != PLAYER_ID_GHOST && player->health > 0) {

		int width, height;
		SDL_GetRendererOutputSize(renderer, &width, &height);

		SDL_Rect dest = {player->x - PLAYERRENDER_LIGHTMASK_SIZE / 2 + PLAYER_WIDTH / 2, player->y - PLAYERRENDER_LIGHTMASK_SIZE / 2 + PLAYER_HEIGHT / 2, PLAYERRENDER_LIGHTMASK_SIZE, PLAYERRENDER_LIGHTMASK_SIZE};
		switch (player->direction) {
		case DIR_UP:
			nearAngle(&playerAssets.angleAnimationLight[player->id], 180, PLAYERRENDER_LIGHTMASK_SMOOTH);
			break;
		case DIR_DOWN:
			nearAngle(&playerAssets.angleAnimationLight[player->id], 0, PLAYERRENDER_LIGHTMASK_SMOOTH);
			break;
		case DIR_LEFT:
			nearAngle(&playerAssets.angleAnimationLight[player->id], 90, PLAYERRENDER_LIGHTMASK_SMOOTH);
			break;
		case DIR_RIGHT:
			nearAngle(&playerAssets.angleAnimationLight[player->id], 270, PLAYERRENDER_LIGHTMASK_SMOOTH);
			break;
		default:
			break;
		}
		if (player->usingSpecialPower)
			SDL_SetTextureAlphaMod(playerAssets.light, 200);
		SDL_RenderCopyEx(renderer, playerAssets.light, NULL, &dest, playerAssets.angleAnimationLight[player->id], NULL, SDL_FLIP_NONE);
		SDL_SetTextureAlphaMod(playerAssets.light, 255);
	}
}

void renderPowerStar(SDL_Renderer *renderer, player_t *player) {
	if (player && player->connected && player->health > 0 && SDL_GetTicks() - player->lastUsedSpecialPower > player->cdPower) {
		SDL_Rect dest = {player->x + PLAYERRENDER_STAR_SPRIT_OFFSET_X, player->y - PLAYERRENDER_STAR_SPRIT_OFFSET_Y, PLAYERRENDER_STAR_SPRIT_WIDTH * 4, PLAYERRENDER_STAR_SPRIT_HEIGHT * 4};
		SDL_RenderCopy(renderer, playerAssets.textureStar, &(playerAssets.sourcesStar[playerAssets.currentFrameStar]), &dest);
		if (playerAssets.animationFrameCountStar == 0) {
			playerAssets.currentFrameStar = (playerAssets.currentFrameStar + 1) % PLAYERRENDER_STAR_SPRIT_NUMBER;
		}
		playerAssets.animationFrameCountStar = (playerAssets.animationFrameCountStar + 1) % PLAYERRENDER_FRAMESKIP;
	}
}