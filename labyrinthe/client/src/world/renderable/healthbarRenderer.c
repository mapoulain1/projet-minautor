#include "healthbarRenderer.h"

void healthbarRender(SDL_Renderer *renderer, player_t *player) {
	SDL_Rect border1 = {player->x + OFF_SET_X, player->y - OFF_SET_Y + SCALE_BAR, SCALE_BAR, SCALE_BAR};
	SDL_Rect border2 = {player->x + OFF_SET_X + SCALE_BAR, player->y - OFF_SET_Y, LENGHT_BAR * SCALE_BAR, SCALE_BAR};
	SDL_Rect border3 = {player->x + OFF_SET_X + SCALE_BAR, player->y - OFF_SET_Y + 2 * SCALE_BAR, LENGHT_BAR * SCALE_BAR, SCALE_BAR};
	SDL_Rect border4 = {player->x + OFF_SET_X + (LENGHT_BAR + 1) * SCALE_BAR, player->y - OFF_SET_Y + SCALE_BAR, SCALE_BAR, SCALE_BAR};
	healthbarDrawRectangle(renderer, border1, 50, 50, 50, 200);
	healthbarDrawRectangle(renderer, border2, 50, 50, 50, 200);
	healthbarDrawRectangle(renderer, border3, 50, 50, 50, 200);
	healthbarDrawRectangle(renderer, border4, 50, 50, 50, 200);

	SDL_Rect life = {player->x + OFF_SET_X + SCALE_BAR, player->y - OFF_SET_Y + SCALE_BAR, LENGHT_BAR * (player->health/100.0) * SCALE_BAR, SCALE_BAR};
	healthbarDrawRectangle(renderer, life, 200, 0, 0, 255);
}

void healthbarDrawRectangle(SDL_Renderer *renderer, SDL_Rect rect, int r, int g, int b, int a) {
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderFillRect(renderer, &rect);
}
