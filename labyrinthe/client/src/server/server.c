#include "server.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <netinet/tcp.h>

#include "../utils/app.h"
#include "../utils/color.h"

server_t serverConnect(char *hostname, int port) {
	server_t server;
	server.online = false;
	char serverVersion[SHORT_BUFFER];
	printf(GRE "[CLIENT] Connecting : %s:%d\n" RESET, hostname, port);

	server.socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (server.socketDescriptor == -1) {
		fprintf(stderr, RED "[CLIENT] Can't create socket\n" RESET);
	} else {
		server.server.sin_addr.s_addr = inet_addr(hostname);
		server.server.sin_family = AF_INET;
		server.server.sin_port = htons(port);
		if (connect(server.socketDescriptor, (struct sockaddr *)&(server.server), sizeof(server)) >= 0) {
			printf(GRE "[CLIENT] Connection established to %s:%d\n" RESET, hostname, port);
			server.online = true;
			read(server.socketDescriptor, serverVersion, SHORT_BUFFER);
			if (strcmp(serverVersion, APP_VERSION) != 0) {
				printf(RED "[CLIENT] Mismatch version <server : %s > <client : %s>\n" RESET, serverVersion, APP_VERSION);
			} else {
				printf(GRE "[CLIENT] Server running on version %s\n" RESET, APP_VERSION);
			}

		} else {
			printf(RED "[CLIENT] Can't connect to %s:%d\n" RESET, hostname, port);
		}
	}
	return server;
}

void serverDisconnect(server_t server) {
	if (server.online) {
		shutdown(server.socketDescriptor, SHUT_RDWR);
		server.online = false;
	}
}

void serverSend(server_t server, player_t *player, item_t *items) {
	if (server.online) {
		char tmp[LONG_BUFFER + MAX_ITEMS * 3];
		char *curr = tmp;
		playerSerialize(player, curr);

		curr += LONG_BUFFER;
		sprintf(curr, "%d %d %d %d %d\n", items[0].exist, items[1].exist, items[2].exist, items[3].exist, items[4].exist);

		if (send(server.socketDescriptor, tmp, LONG_BUFFER + MAX_ITEMS * 3, 0) != LONG_BUFFER + MAX_ITEMS * 3) {
			fprintf(stderr, RED "[CLIENT] Can't send data\n" RESET);
		}
	}
}

void serverRead(server_t server, player_t *players, item_t *items, int playerID) {
	if (server.online) {
		char tmp[LONG_BUFFER * (MAX_CLIENTS + 1)];
		char *curr = tmp;
		player_t tmpPlayer;

		if (recv(server.socketDescriptor, tmp, LONG_BUFFER * (MAX_CLIENTS + 1), 0) != LONG_BUFFER * (MAX_CLIENTS + 1)) {
			fprintf(stderr, "Error can't get players positions from server\n");
			return;
		}

		for (int i = 0; i < MAX_CLIENTS; i++) {
			playerDeserialize(&tmpPlayer, curr);
			curr += LONG_BUFFER;

			if (i != playerID)
				players[i] = tmpPlayer;
		}
		sscanf(curr, "%d %d %d %d %d", (int *)&(items[0].exist), (int *)&(items[1].exist), (int *)&(items[2].exist), (int *)&(items[3].exist), (int *)&(items[4].exist));
	}
}
void serverGetPlayerInfo(server_t server, config_t *config) {
	if (server.online) {
		char playerInfo[LONG_BUFFER];
		if (recv(server.socketDescriptor, playerInfo, LONG_BUFFER, 0) != LONG_BUFFER) {
			fprintf(stderr, "Error can't get player ID from server\n");
		} else {
			sscanf(playerInfo, "%d %d %d", &(config->playerID), &(config->spawnX), &(config->spawnY));
		}
	}
}

void serverGetLaby(server_t server, config_t *config) {
	if (server.online) {
		char tmp[LONG_BUFFER];
		if (recv(server.socketDescriptor, tmp, LONG_BUFFER, 0) != LONG_BUFFER) {
			fprintf(stderr, "Error can't get player ID from server\n");
		} else {
			sscanf(tmp, "%d %d %lf %d", &(config->worldSize), &(config->seed), &(config->percentKruskal), &(config->numberOfRoom));
		}
	}
}

void serverThread(server_t server, int playerID, player_t *players, bool *connection, item_t *items) {
	internal_thread_server_t *args = malloc(sizeof(internal_thread_server_t));
	args->playerID = playerID;
	args->players = players;
	args->server = server;
	args->connection = connection;
	args->items = items;
	pthread_t threadCreated;
	pthread_create(&threadCreated, NULL, (void *(*)(void *))serverThreadInternal, args);
}

void *serverThreadInternal(internal_thread_server_t *args) {
	while (true) {
		serverSend(args->server, &(args->players[args->playerID]), args->items);
		serverRead(args->server, args->players, args->items, args->playerID);
	}
}