#ifndef SERVER_H_
#define SERVER_H_

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>

#include "../utils/bool.h"
#include "../utils/config.h"
#include "../world/internal/item.h"
#include "../world/internal/player.h"

#define MAX_CLIENTS 5

#define SKIP_FRAME 2

#define LONG_BUFFER 86
#define SHORT_BUFFER 8

typedef struct {
	int socketDescriptor;
	struct sockaddr_in server;
	bool online;
} server_t;

typedef struct {
	server_t server;
	int playerID;
	player_t *players;
	bool *connection;
	item_t *items;
} internal_thread_server_t;

server_t serverConnect(char *hostname, int port);
void serverDisconnect(server_t server);
void serverSend(server_t server, player_t *player, item_t *items);
void serverRead(server_t server, player_t *players, item_t *items,int playerID);
void serverGetPlayerInfo(server_t server, config_t *config);
void serverGetLaby(server_t server, config_t *config);
void serverThread(server_t server, int playerID, player_t *players, bool *lateUpdate, item_t *items);
void *serverThreadInternal(internal_thread_server_t *args);

#endif