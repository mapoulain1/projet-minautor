#ifndef BOOL_H
#define BOOL_H

typedef enum { false, true } bool;
    
void printBoolArray(bool * array, int size);
bool *boolAlloc(void);

#endif