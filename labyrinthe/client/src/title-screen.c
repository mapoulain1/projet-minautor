#include "title-screen.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "game.h"
#include "graphics/graphics.h"
#include "graphics/musicPlayer.h"
#include "utils/app.h"
#include "utils/bool.h"
#include "utils/config.h"
#include "process/input.h"

bool fenetre = true;

SDL_Texture *chargerTexture(const char *nomImg, SDL_Renderer *renderer) {
	SDL_Surface *img = NULL;
	SDL_Texture *texture = NULL;
	img = IMG_Load(nomImg);

	if (img == NULL)
		SDL_Log("Erreur chargement image %s\n", SDL_GetError());

	texture = SDL_CreateTextureFromSurface(renderer, img);
	SDL_FreeSurface(img);

	if (texture == NULL)
		SDL_Log("Erreur chargement texture %s\n", SDL_GetError());

	return texture;
}

SDL_Texture *chargerTexte(const char *txt, const char *nomPolice, int taille, SDL_Color color, SDL_Renderer *renderer) {
	TTF_Font *font = NULL;
	font = TTF_OpenFont(nomPolice, taille);
	if (font == NULL)
		SDL_Log("Erreur chargement police %s\n", SDL_GetError());

	/* TTF_SetFontStyle(font, TTF_STYLE_BOLD | ...) */

	SDL_Surface *text_surface = NULL;
	text_surface = TTF_RenderText_Blended(font, txt, color);
	if (text_surface == NULL)
		SDL_Log("Erreur surface de texte %s\n", SDL_GetError());

	SDL_Texture *text_texture = NULL;
	text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
	if (text_texture == NULL)
		SDL_Log("Erreur texture de police %s\n", SDL_GetError());
	SDL_FreeSurface(text_surface);

	return text_texture;
}

bool gameTitle(SDL_Window *window) {
	bool running = true;
	bool halt = false;
	bool launch_game = false;

	SDL_Renderer *renderer = SDL_GetRenderer(window);
	SDL_Event event;

	SDL_Rect source = {0},
        window_dimensions = {0},
        destination = {0},
        state = {0};

	SDL_Texture *texture = chargerTexture(PATH_TITLE_SCREEN, renderer);

	SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
	SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

	musicPlayerInit();
	musicPlay();

	int nb_img = 3;
	float zoom = 16;
	int offset_x = source.w / nb_img;
	int offset_y = source.h;
	int frames = 0;

	state.x = 0;
	state.y = 0;
	state.w = offset_x;
	state.h = offset_y;

	destination.w = offset_x * zoom;
	destination.h = offset_y * zoom;
	destination.y = (window_dimensions.h - destination.h) / 2;
	destination.x = -destination.w;

	/* TEXTE */
	SDL_Color color_title = {0xff, 0xff, 0x75, 0xff};
	SDL_Color color = {255, 255, 0xed, 200};
	char *police = "./fonts/CompassGold.ttf";

	SDL_Texture *titre = chargerTexte("GHOST MAZE", "./fonts/font.ttf", 40, color_title, renderer);
	SDL_Texture *lore = chargerTexte("Des paladins nyctophobes traquent un spectre effrayant.", police, 30, color, renderer);
	SDL_Texture *texte1 = chargerTexte("Entree pour jouer", police, 30, color, renderer);
	SDL_Texture *texte2 = chargerTexte("Echap pour quitter", police, 30, color, renderer);
	SDL_Texture *texte3 = chargerTexte("Jeu par Coralie, Lilian & Maxime", police, 30, color, renderer);
	SDL_Texture *texte4 = chargerTexte("Musique par Abstraction : abstractionmusic.com/", police, 30, color, renderer);

	SDL_Rect pos_titre = {0, -50, 0, 0};
	SDL_Rect pos_lore = {0, 0, 0, 0};
	SDL_Rect pos_texte1 = {0, 0, 0, 0};
	SDL_Rect pos_texte2 = {0, 0, 0, 0};
	SDL_Rect pos_texte3 = {0, 0, 0, 0};
	SDL_Rect pos_texte4 = {0, 0, 0, 0};

	SDL_QueryTexture(titre, NULL, NULL, &pos_titre.w, &pos_titre.h);
	SDL_QueryTexture(lore, NULL, NULL, &pos_lore.w, &pos_lore.h);
	SDL_QueryTexture(texte1, NULL, NULL, &pos_texte1.w, &pos_texte1.h);
	SDL_QueryTexture(texte2, NULL, NULL, &pos_texte2.w, &pos_texte2.h);
	SDL_QueryTexture(texte3, NULL, NULL, &pos_texte3.w, &pos_texte3.h);
	SDL_QueryTexture(texte4, NULL, NULL, &pos_texte4.w, &pos_texte4.h);

	/* Background */
	SDL_Rect background;
	SDL_SetRenderDrawColor(renderer, 0xFF, 0x10, 0x10, 0xff);
	background.x = 0;
	background.y = 0;
	background.w = window_dimensions.w;
	background.h = window_dimensions.h;
	SDL_RenderFillRect(renderer, &background);

	/* Boucle d'affichage */
	while (running) {
		/* Background */
		background.w = window_dimensions.w;
		background.h = window_dimensions.h;

		/* Position fantôme */

		if (destination.x < (window_dimensions.w - destination.w) / 2 - 20)
			destination.x += 5;
		if (destination.x > (window_dimensions.w - destination.w) / 2 + 20)
			destination.x -= 5;

		destination.y = (window_dimensions.h - destination.h) / 2;
		if (frames % 10 == 0) {
			state.x += offset_x;
			state.x %= source.w;
		}

		/* Position titre */
		pos_titre.x = (window_dimensions.w - pos_titre.w) / 2;
		if (pos_titre.y < destination.y - pos_titre.h - 20)
			pos_titre.y += 5;
        if (pos_titre.y > destination.y - pos_titre.h + 20)
			pos_titre.y -= 5;
        

		/* Position lore */
		pos_lore.x = 0;
		pos_lore.y = window_dimensions.h - (pos_texte1.h + pos_texte2.h + pos_texte3.h + pos_texte4.h + pos_lore.h);
		/* Position texte */
		pos_texte1.x = 0;
		pos_texte1.y = window_dimensions.h - (pos_texte1.h + pos_texte2.h + pos_texte3.h + pos_texte4.h);
		pos_texte2.x = 0;
		pos_texte2.y = window_dimensions.h - (pos_texte2.h + pos_texte3.h + pos_texte4.h);
		pos_texte3.x = 0;
		pos_texte3.y = window_dimensions.h - (pos_texte3.h + pos_texte4.h);
		pos_texte4.x = 0;
		pos_texte4.y = window_dimensions.h - (pos_texte4.h);

		gcsClearBackground(renderer, 0);

		SDL_SetRenderDrawColor(renderer, 0x10, 0x10, 0x10, 0xff);
		SDL_RenderFillRect(renderer, &background);

		SDL_RenderCopy(renderer, texture, &state, &destination);

		SDL_RenderCopy(renderer, titre, NULL, &pos_titre);
		SDL_RenderCopy(renderer, lore, NULL, &pos_lore);
		SDL_RenderCopy(renderer, texte1, NULL, &pos_texte1);
		SDL_RenderCopy(renderer, texte2, NULL, &pos_texte2);
		SDL_RenderCopy(renderer, texte3, NULL, &pos_texte3);
		SDL_RenderCopy(renderer, texte4, NULL, &pos_texte4);

		SDL_RenderPresent(renderer);
		SDL_Delay(16);

		/* Evenements */


        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_WINDOWEVENT:
                switch (event.window.event) {
                case SDL_WINDOWEVENT_CLOSE:
                    running = false;
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    gameRenderGame(window);
                    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
                    break;
                }
                break;
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case KEY_PAUSE:
                    halt = !(halt);
                    break;
                case KEY_QUIT:
                    running = false;
                    break;
                case KEY_FULLSCREEN:
                    SDL_SetWindowFullscreen(window, fenetre ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
                    fenetre = !fenetre;
                    break;
                case KEY_VOLUME_UP:
                    musicVolumeUp();
                    break;
                case KEY_VOLUME_DOWN:
                    musicVolumeDown();
                    break;
                case KEY_VOLUME_MUTE:
                    musicVolumeMute();
                    break;
                case KEY_ACCEPT:
                    running = false;
                    launch_game = true;
                    break;
                }
                break;
            }
        }

        frames++;
    }
    gcsClearBackground(renderer, 0);

/* Libérer */
    SDL_DestroyTexture(texture);
    SDL_DestroyTexture(titre);
    SDL_DestroyTexture(texte1);
    SDL_DestroyTexture(texte2);
    SDL_DestroyTexture(texte3);
    SDL_DestroyTexture(texte4);
    SDL_DestroyTexture(lore);
    return launch_game;
}
