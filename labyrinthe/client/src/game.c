#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/camera.h"
#include "graphics/graphics.h"
#include "graphics/musicPlayer.h"
#include "process/input.h"
#include "server/server.h"
#include "title-screen.h"
#include "utils/bool.h"
#include "utils/color.h"
#include "utils/math.h"
#include "world/internal/labyrinthe.h"
#include "world/internal/player.h"
#include "world/internal/tile.h"
#include "world/renderable/itemRenderer.h"
#include "world/renderable/labyrintheRenderer.h"
#include "world/renderable/playerRenderer.h"

long ticks = 0;
int frames = 0;

config_t gameConfig;
labyrinthe_t laby;
graphe_t *graph;
graphe_t *graphKruskal;

camera_t camera;
server_t server;
player_t players[MAX_CLIENTS];
item_t items[MAX_ITEMS];

void gameSetup(config_t config) {
	srand(6);
	gameConfig = config;
}

void gameStart(SDL_Window *window) {
	bool running = true;
	bool halt = false;
	bool keyPressed;
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	direction_t direction = DIR_NONE;
	int windowWidth, windowHeight;

	SDL_GetWindowSize(window, &windowWidth, &windowHeight);

	renderLabyrintheInit(renderer);
	renderPlayerInit(renderer);
	renderItemInit(renderer);

	server = serverConnect(gameConfig.server, gameConfig.port);
	if (!server.online) {
		gameEnd(window);
		return;
	}
	serverGetLaby(server, &gameConfig);

	srand(gameConfig.seed);
	graph = graphCreerPourLaby(gameConfig.worldSize * gameConfig.worldSize);
	graphKruskal = kruskal(graph, fisherYates, gameConfig.percentKruskal);
	graphCreateRoom(graphKruskal, gameConfig.numberOfRoom);
	laby = labyrintheInit(graphKruskal);

	serverGetPlayerInfo(server, &gameConfig);

	cameraInit(&camera, gameConfig.worldSize, gameConfig.playerID, windowHeight);
	playerInit(&players[gameConfig.playerID], gameConfig.spawnX, gameConfig.spawnY, camera.tileSize, gameConfig.playerID);
	itemsList(items, &laby, camera.tileSize);

	while (running) {
		ticks = SDL_GetTicks();
		inputMisc(&halt, &running, &camera, &players[gameConfig.playerID], window, &windowWidth, &windowHeight);

		gameRenderGame(window);

		if (!halt) {
			playerStop(&players[gameConfig.playerID]);
			inputPlayerMovement(&keyPressed, &direction);
			playerSpecialPowerCheck(&players[gameConfig.playerID]);
			playerUpdateCollision(renderer, &players[gameConfig.playerID], &laby, direction, keyPressed, &camera);
			playerApplyDamageGhost(players, gameConfig.playerID);
			playerApplyDamageLight(renderer, players, gameConfig.playerID, &camera);
			playerApplySoundeffects(players, gameConfig.playerID);
			playerCollisionWithItem(&players[gameConfig.playerID], items);

			cameraUpdate(&camera, renderer, &players[gameConfig.playerID], gameConfig.worldSize, windowWidth, windowHeight);
		}

		if (frames % SKIP_FRAME == 0) {
			serverSend(server, &players[gameConfig.playerID], items);
			serverRead(server, players, items, gameConfig.playerID);
		}

		SDL_RenderPresent(renderer);
		SDL_Delay(SDL_GetTicks() - ticks < MS_SLEEP_WANTED ? MS_SLEEP_WANTED - (SDL_GetTicks() - ticks) : 0);
		frames++;
	}

	serverDisconnect(server);

	renderLabyrintheFree();
	renderPlayerFree();
	graphLiberer(&graph);
	graphLiberer(&graphKruskal);
	labyrintheFree(&laby);
	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
}

void gameRenderGame(SDL_Window *window) {
	int windowWidth, windowHeight;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer, gameConfig.playerID);
	if (camera.renderDebug) {
		renderLabyrintheDebug(renderer, &laby, &camera);
		renderPlayerDebug(renderer, &players[gameConfig.playerID]);
	} else {
		renderLabyrinthe(renderer, &laby, &camera, WALLS_RENDER_LAYER_BACK);
		renderItem(renderer, items);
		for (int i = 0; i < MAX_CLIENTS; i++) {
			if (players[i].connected && players[i].id != gameConfig.playerID)
				renderPlayer(renderer, &players[i]);
		}
		renderPlayer(renderer, &players[gameConfig.playerID]); // to always be in front of the others
		renderPowerStar(renderer, &players[gameConfig.playerID]);
		renderLabyrinthe(renderer, &laby, &camera, WALLS_RENDER_LAYER_FRONT);
		renderLight(renderer, &players[gameConfig.playerID]);
		if(players[gameConfig.playerID].health <= 0.1){
			playerRenderGameover(renderer,windowWidth, windowHeight);
		}
	}
}

void gameSignalHandler(int signo) {
	if (signo == SIGPIPE || signo == SIGSEGV)
		printf(RED "[CLIENT] Server connection lost\n" RESET);
}
