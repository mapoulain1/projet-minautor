#ifndef CAMERA_H_
#define CAMERA_H_

#include "../utils/bool.h"

#include <SDL2/SDL.h>

#define CAMERA_DEFAULT_TILESIZE 192
#define CAMERA_ZOOM_STEP 0.005

#define CAMERA_ZOOM_SLEEP_MICROSEC 10000
#define CAMERA_ZOOM_SCALE 24

typedef struct {
	SDL_Rect viewport;
	float scale;
	int tileSize;
	bool renderDebug;
	bool renderHitbox;
} camera_t;

#include "../world/internal/player.h"

void cameraInit(camera_t *out, int worldSize, int playerID, int windowSize);
void cameraUpdate(camera_t *out, SDL_Renderer *renderer, player_t *player, int worldSize, int windowWidth, int windowHeight);
void cameraToggleDebug(camera_t *out);
void cameraToggleHitboxs(camera_t *out);
void cameraZoomIn(camera_t *out);
void cameraZoomOut(camera_t *out);

void *cameraZoomInAnimatedInternal(camera_t *out);
void *cameraZoomOutAnimatedInternal(camera_t *out);

void cameraZoomInAnimated(camera_t *out);
void cameraZoomOutAnimated(camera_t *out);

#endif