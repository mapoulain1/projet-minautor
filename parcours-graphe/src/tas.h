#ifndef TAS
#define TAS

#include <stdio.h>
#include <stdlib.h>

#include "graph.h"


/*****************************************************************/
/* Les tas sont généralement représentés sous forme de tableau.  */
/* Etant donné un noeud i, on sait que :                         */
/* - son fils gauche est en 2*i+1                                */
/* - son fils droit en 2*i+2                                     */
/* - son père est en (i-1)/2                                     */
/*****************************************************************/

typedef struct sommet {
    int id;
    int dist;
} sommet_t;

typedef struct tas {
    sommet_t ** tab; // le tableau qui contient le tas
    int n; // le nombre case utilisées (aussi l'indice de la première case vide)
    int taille_max;
    int * posTas;
} tas_t;



tas_t * initTas (int taille_max);

int estPlein (tas_t * tas);

int estVide (tas_t * tas);

int enfiler (tas_t * tas, sommet_t * val);

sommet_t * defiler (tas_t * tas);

tas_t * tabToTas (sommet_t ** T, int i);

void triParTas (tas_t * tas);

void permuter (tas_t * tas, int a, int b);

void entasser (tas_t * tas, int i, int taille);

void libererTas(tas_t * tas);

sommet_t * creerSommet(int id, int dist);

#endif
