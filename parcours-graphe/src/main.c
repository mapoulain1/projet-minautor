#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "graph.h"
#include "parcours-graphe.h"
#include "distance.h"

int main() {
    srand(4);

    graphe_t * G = creerGraphe();
    int width = 4;

    graphe_t * G2 = kruskal(G, NULL, 0.2);
    
    int r = rand() % G->nbNoeuds;

    int * parent = dijkstra(G2, r);
    printf("Dijkstra parents à partir de %d :\n", r);
    for(int i = 0; i<G2->nbNoeuds; i++)
    {
	printf("%d:%d ", i, parent[i]);
    }
    printf("\n");

    printf("A* parents à partir de %d jusqu'à %d\n", r, G->nbNoeuds-1);
    int * parent2 = aEtoile(G2, r, manhattan, width, G->nbNoeuds-1);
    for(int i = 0; i<G2->nbNoeuds; i++)
    {
	printf("%d:%d ", i, parent2[i]);
    }
    printf("\n");

    printf("Parcours en profondeur à partir de %d\n", r);
    DFS(G2, r);

    /* displayGraphGraph(G2); */

    free(parent);
    free(parent2);
    libererGraphe(G);
    libererGraphe(G2);
  
    return 0;
}
