#include "tas.h"


tas_t * initTas (int taille_max) {
    tas_t * tas = (tas_t *)malloc(sizeof(tas_t));
    tas->tab = (sommet_t **)malloc(taille_max * sizeof(sommet_t *));
    tas->n = 0;
    tas->taille_max = taille_max;
    tas->posTas = (int *)malloc(taille_max * sizeof(int));
    for(int i=0; i<taille_max; i++)
    {
        tas->posTas[i] = -1;
    }
    return tas;
}



int estPlein (tas_t * tas) {
    return tas->n >= tas->taille_max - 1;
}



int estVide (tas_t * tas) {
    return tas->n == 0;
}  



int enfiler (tas_t * tas, sommet_t * val) {
    if (!estPlein(tas)) {
        int i = tas->n;
        while (i > 0 && val->dist > tas->tab[(i-1)/2]->dist) {
            tas->tab[i] = tas->tab[(i-1)/2];
            tas->posTas[tas->tab[i]->id] = i;
            i = (i-1)/2;
        }
        tas->tab[i] = val;
        tas->posTas[val->id] = i;
        (tas->n)++;
        return 0;
    }
    else {
        printf("Erreur : le tas est plein %d, %d\n", tas->n, tas->taille_max);
        return -1;
    }
}



sommet_t * defiler (tas_t * tas) {
    if (!estVide(tas)) {
        sommet_t * k = tas->tab[0]; // on récupère la valeur de la racine
        (tas->n)--;
        int s = tas->n;
        sommet_t * val = tas->tab[s];
        int i = 0;
        tas->posTas[tas->tab[i]->id] = -1;

        while ((2*i+1 < s && val->dist < tas->tab[2*i+1]->dist) || (2*i+2 < s && val->dist < tas->tab[2*i+2]->dist)) {
            if (2*i+2 < s && tas->tab[2*i+1]->dist < tas->tab[2*i+2]->dist) {
                tas->posTas[tas->tab[2*i+2]->id] = i;
                tas->tab[i] = tas->tab[2*i+2];
                i = 2*i+2;
            }
            else {
                tas->posTas[tas->tab[2*i+1]->id] = i;
                tas->tab[i] = tas->tab[2*i+1];
                i = 2*i+1;
            }
        }
        tas->tab[i] = val;
        return k;
    }
    else {
        printf("Erreur : le tas est vide\n");
        return NULL;
    }
}  



tas_t * tabToTas (sommet_t ** T, int taille) {
    tas_t * tas = initTas(taille);
    free(tas->tab);
    tas->tab = T;
    tas->n = taille;
    for (int i = tas->n/2; i >= 0; i--) {
        entasser(tas, i, tas->n);
    }
    return tas;
}


void triParTas (tas_t * tas) {
    for (int i = tas->n - 1; i >= 1; i--) {
        permuter(tas, 0, i);
        entasser(tas, 0, i);
    }
}  



void permuter (tas_t * tas, int a, int b) {
    sommet_t * tmp = tas->tab[a];
    int tmpPos = tas->posTas[tas->tab[b]->id];
    tas->tab[a] = tas->tab[b];
    tas->posTas[tas->tab[a]->id] = tas->posTas[tas->tab[b]->id];
    tas->tab[b] = tmp;
    tas->posTas[tas->tab[b]->id] = tmpPos;
}  



void entasser (tas_t * tas, int i, int taille) {
    int max = 0;
    if (2*i+1 <= taille - 1 && tas->tab[2*i+1]->dist > tas->tab[i]->dist) {
        max = 2*i+1;
    }
    else {
        max = i;
    }
    if (2*i+2 <= taille - 1 && tas->tab[2*i+2]->dist > tas->tab[max]->dist) {
        max = 2*i+2;
    }
    if (max != i) {
        permuter(tas, i, max);
        entasser(tas, max, taille);
    }
}  



void libererTas(tas_t * tas)
{
    free(tas->tab);
    free(tas);
}

sommet_t * creerSommet(int id, int dist)
{
    sommet_t * s = malloc(sizeof(sommet_t));
    s->id = id;
    s-> dist = dist;
    return s;
}
