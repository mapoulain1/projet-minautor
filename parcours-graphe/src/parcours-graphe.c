#include <time.h>
#include <math.h>

#include "parcours-graphe.h"


/* dijkstra(graphe_t * G, int r) */
/*       racine r */
/*       d[u] : pour tout les sommets u distance de r vers u */
/*       parent[u] : dernier sommet avant dans le chemin r -> u */
/*       f : fille d'attente de priorité */

/*       Init : */
/*       d[u] = infini */
/*       parent[u] = null */
/*       f = ensemble des sommets */
/*       d[r] = 0 */

/*       tant que F non vide: */
/*           extraire de f la sommet de plus petit dist */
/* 	  pour tout arc (u, v): */
/* 	      relacher(u, v) */
/*       retourner d[], parent[] */


int * parcours(graphe_t * G, int r, double (*fdist)(int a, int b, int width), int width, int dest)
{
    int n = G->nbNoeuds;
    int k; // Une arête

    tas_t * tas = initTas(n); // tas min
    
    int * dejaTraite = malloc(n * sizeof(int));
    int * dist = malloc(n * sizeof(int));
    int * parent = malloc(n * sizeof(int));
    initParcours(r, n, dist, parent, tas, dejaTraite);

    sommet_t * u = NULL;
    sommet_t * v = NULL;
    
  while (!estVide(tas))
    {
        // On va sur le sommet u non traité le plus proche de la racine
        u = defiler(tas);
        // u a été traité
        dejaTraite[u->id] = 1;
        for(k=0; k<G->nbArretes; k++)
        {
            // Rechercher les voisins de u
            if (G->i[k] == u->id )
            {
                // Actualiser la distance du nouveau noeud atteint
                relacher(u->id, G->j[k], dist, parent, tas);
                // Si le nouveau sommet n'a pas été traité
                // L'insérer dans le tas
                if (!dejaTraite[G->j[k]])
                {
                    if (fdist == NULL)
                    {
                        v = creerSommet( G->j[k], dist[G->j[k]] );
                    }
                    else
                    {
                        v = creerSommet( G->j[k], dist[G->j[k]] + fdist(dest, G->i[k], width) );
                    }
                    enfiler(tas, v);
                }
            }
            else if (G->j[k] == u->id)
            {
                relacher(u->id, G->i[k], dist, parent, tas);
                if (!dejaTraite[G->i[k]])
                {
                    if (fdist == NULL)
                    {
                        v = creerSommet( G->i[k], dist[G->i[k]] );
                    }
                    else
                    {
                        v = creerSommet( G->i[k], dist[G->i[k]] + fdist(dest, G->i[k], width) );
                    }
                    enfiler(tas, v);
                }
            }
        }
        free(u);
    }

    for (int i=0; i<tas->n; i++)
    {
        free(tas->tab[i]);
    }
  free(dist);
  free(dejaTraite);
  libererTas(tas);
  return parent;
}


void initParcours(int r, int nbNoeuds, int * dist, int * parent, tas_t * tas, int * dejaTraite)
{
  int u;
  sommet_t * s = NULL;
  for(u=0; u<nbNoeuds; u++)
    {
        dejaTraite[u] = 0;
        dist[u] = (int)INFINITY;
        parent[u] = -1;
    }
  dist[r] = 0;
  s = creerSommet(r, dist[r]);
  enfiler(tas, s);
}


void relacher(int u, int v, int * dist, int * parent, tas_t * tas)
{
  int poids = 1;
  if (dist[v] >= dist[u] + poids)
    {
        dist[v] = dist[u] + poids;
        parent[v] = u;
        // Changer les distances dans le tas à l'aide de tas->posTas
        if (tas->posTas[v] != -1)
        {
            tas->tab[ tas->posTas[v] ]->dist = dist[v];
            // entasser()
            entasser(tas, tas->posTas[v], tas->n);
        }
    }
}

int * dijkstra(graphe_t * G, int r)
{
    return parcours(G, r, NULL, 0, 0);
}


int * aEtoile (graphe_t * G, int r, double (*fdist)(int a, int b, int width), int width, int dest)
{
    return parcours(G, r, fdist, width, dest);
}

void DFS (graphe_t * G, int r)
{
  int i;
    
  int * dejaTraite = malloc(G->nbNoeuds * sizeof(int));
  for (i=0; i<G->nbNoeuds; i++)
    {
      dejaTraite[i] = 0;
    }
  for (i=0; i<G->nbNoeuds; i++)
    {
      DFS_rec(G, r, dejaTraite);
    }
  printf("\n");
}

void DFS_rec(graphe_t * G, int r, int * dejaTraite)
{
  if (!dejaTraite[r])
    {
      printf("%d ", r);
      int k;
      dejaTraite[r] = 1;
      // Pour tous les voisins
      for(k=0; k<G->nbArretes; k++)
	{
	  /* printf("i:%d | j:%d\n", G->i[k], G->j[k]); */
	  if (G->i[k] == r && !dejaTraite[G->j[k]])
	    {
	      DFS_rec(G, G->j[k], dejaTraite);
	    }
	  else if (G->j[k] == r && !dejaTraite[G->i[k]])
	    {
	      DFS_rec(G, G->i[k], dejaTraite);
	    }
	}
    }
}
