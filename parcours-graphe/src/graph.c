#include <time.h>
#include <math.h>

#include "graph.h"

int **creerMatrice() {
    int k = -1, continuer = 1;
    int **M = (int **)malloc(TAILLE_GRAPH * sizeof(int *));
    if (M != NULL) {
	while (k < TAILLE_GRAPH - 1 && continuer) {
	    k++;
	    M[k] = (int *)malloc(TAILLE_GRAPH * sizeof(int));
	    if (M[k] == NULL) {
		continuer = 0;
	    }
	}
	if (continuer) {
	    for (int i = 0; i < TAILLE_GRAPH; i++) {
		M[i][i] = 0;
		for (int j = i + 1; j < TAILLE_GRAPH; j++) {
		    M[i][j] = rand() % 2;
		    M[j][i] = M[i][j];
		}
	    }
	} else {
	    libererMat(M);
	} 
    }
    return M;
}

void libererMat (int ** M) {
    if (M != NULL) {
	for (int i=0; i<TAILLE_GRAPH; i++) {
	    if (M[i] != NULL) {
		free(M[i]);
	    }
	}
	free(M);
    }
}  

graphe_t *initGraphe() {
    graphe_t *G = (graphe_t *)malloc(sizeof(graphe_t));
    G->nbNoeuds = TAILLE_GRAPH;
    G->nbArretes = 0;
    return G;
}

void initArrete(graphe_t * G, int i, int j) {
    G->i[G->nbArretes] = i;
    G->j[G->nbArretes] = j;
    (G->nbArretes)++;
}

graphe_t *creerGraphe() {
    graphe_t *G = initGraphe();
    for (int i = 0; i < TAILLE_GRAPH; i++) {
	for (int j = i+1; j < TAILLE_GRAPH; j++) {
	    if (rand() % 2) {
		initArrete(G, i, j);
	    }
	}
    }
    return G;
}

void libererGraphe(graphe_t * G) {
    free(G);
}  

partition_t graphesConnexesMat(int **M) {
    // créer une partition de taille TAILLE_GRAPH
    partition_t P = partitionCreer(TAILLE_GRAPH);
    // la matrice étant symétrique, on ne parcourt que la matrice triangulaire supérieur, diagonale exclue
    for (int i = 0; i < TAILLE_GRAPH; i++) {
	for (int j = i + 1; j < TAILLE_GRAPH; j++) {
	    // fusionner si M[i][j] = 1 et si i et j ne sont pas deja dans la meme classe
	    if (M[i][j] == 1 && partitionRecupererClasse(&P, i) != partitionRecupererClasse(&P, j)) {
		partitionFusion(&P, i, j);
	    }
	}
    }
    return P;
}

partition_t graphesConnexesGraphe(graphe_t *G) {
    partition_t P = partitionCreer(G->nbNoeuds);
    for (int k=0; k<G->nbArretes; k++) {
	// fusionner si i et j ne sont pas dans la meme classe
	if (partitionRecupererClasse(&P, G->i[k]) != partitionRecupererClasse(&P, G->j[k])) {
	    partitionFusion(&P, G->i[k], G->j[k]);
	}
    }
    return P;
}

void displayGraphMat(int **matrix) {
    char tmp[1024];
    char filename[128];
    int random = rand();
    FILE *flot;

    sprintf(filename, "graph_%d.dot", random);

    if (matrix) {
	flot = fopen(filename, "w");
	fprintf(flot, "graph MyGraph {\n");
	for (int i = 0; i < TAILLE_GRAPH; i++) {
	    fprintf(flot, "    %d;\n", i);
	    for (int j = i + 1; j < TAILLE_GRAPH; j++) {
		if (matrix[i][j] == 1) {
		    fprintf(flot, "    %d -- %d;\n", i, j);
		}
	    }
	}
	fprintf(flot, "}\n");
	fclose(flot);

	sprintf(tmp, "dot graph_%d.dot -T" IMAGE_FORMAT " > graph_%d." IMAGE_FORMAT " ; " IMAGE_VIEWER " graph_%d." IMAGE_FORMAT, random, random, random);
	system(tmp);
	system("rm *.dot *." IMAGE_FORMAT);
    }
}


void displayGraphGraph(graphe_t * G) {
    char tmp[1024];
    char filename[128];
    int random = rand();
    FILE *flot;

    sprintf(filename, "graph_%d.dot", random);

    if (G) {
	flot = fopen(filename, "w");
	fprintf(flot, "graph MyGraph {\n");
	for (int i = 0; i < G->nbNoeuds; i++) {
	    fprintf(flot, "    %d;\n", i);
	}  
	for (int k=0; k<G->nbArretes; k++) {
	    fprintf(flot, "    %d -- %d;\n", G->i[k], G->j[k]);
	}
	fprintf(flot, "}\n");
	fclose(flot);

	sprintf(tmp, "dot graph_%d.dot -T" IMAGE_FORMAT " > graph_%d." IMAGE_FORMAT " ; " IMAGE_VIEWER " graph_%d." IMAGE_FORMAT, random, random, random);
	system(tmp);
	system("rm *.dot *." IMAGE_FORMAT);
    }
}

graphe_t * kruskal (graphe_t * g1, void (*f)(graphe_t *), double proba)
{
    graphe_t * g2 = initGraphe();

    if ((*f) != NULL) {
	(*f)(g1);
    }  

    partition_t p = partitionCreer(g1->nbNoeuds);

    int k;
    for(k = 0; k < g1->nbArretes; k++)
    {
	if ( partitionRecupererClasse(&p, g1->i[k]) != partitionRecupererClasse(&p, g1->j[k]) )
	{
	    partitionFusion(&p, g1->i[k], g1->j[k]); // Fusion des partitions
	    initArrete(g2, g1->i[k], g1->j[k]); // Ajouter l'arête au graphe 2
	}
	else
	{
	    if ( (float)rand()/(float)(RAND_MAX) < proba )
	    {
		partitionFusion(&p, g1->i[k], g1->j[k]); // Fusion des partitions
		initArrete(g2, g1->i[k], g1->j[k]); // Ajouter l'arête au graphe 2
	    }
	}
    }
    partitionLiberer(&p);
    return g2;
}


void fisherYates (graphe_t * G) {
    for (int k = G->nbArretes - 1; k >= 1; k--) {
	int l = rand() % k;
	int tmp_i = G->i[k], tmp_j = G->j[k];
	G->i[k] = G->i[l];
	G->j[k] = G->j[l];
	G->i[l] = tmp_i;
	G->j[l] = tmp_j;
    }
}
