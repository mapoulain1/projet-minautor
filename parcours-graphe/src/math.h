#ifndef MATH_H_
#define MATH_H_

int mod(int i, int m);
int clamp(int val, int min, int max);
double clampDouble(double val, double min, double max);
int array2Dto1D(int i, int j, int width);
void array1Dto2D(int value, int *iOut, int *jOut, int width);
int max(int a, int b);

#endif
