#ifndef DISTANCE
#define DISTANCE

double euclidienne(int a, int b, int width);

double tchebychev(int a, int b, int width);

double manhattan(int a, int b, int width);

#endif
