#ifndef GRAPH
#define GRAPH


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "partition.h"

#define TAILLE_GRAPH 6



/*

Avec la représentation du graphe comme un couple comprenant le nombre de noeuds et la liste des arretes :
si on considère un labyrinthe de TAILLE_GRAPH x TAILLE_GRAPH, la case de coordonnées (i; j) aura pour noeud associé 
j * TAILLE_GRAPH + i. Les numéros des noeuds allant de 0 à TAILLE_GRAPH * TAILLE_GRAPH;

*/


typedef struct graphe {
  int nbNoeuds;
  int i[TAILLE_GRAPH*TAILLE_GRAPH];
  int j[TAILLE_GRAPH*TAILLE_GRAPH];
  int nbArretes;
} graphe_t;




int ** creerMatrice ();

void libererMat (int ** M);

graphe_t * initGraphe ();

void initArrete (graphe_t * G, int i, int j);

graphe_t * creerGraphe();

void libererGraphe(graphe_t * G);

partition_t graphesConnexesMat (int ** M);

partition_t graphesConnexesGraphe (graphe_t * G);

void displayGraphMat(int **matrix);

void displayGraphGraph(graphe_t * G);

graphe_t * kruskal (graphe_t * g1, void (*f)(graphe_t *), double proba);
/**
 * @brief Retourne un arbre couvrant de g1
 *
 * @param g1 le graphe
 * @param f un pointeur de fonction à appliquer au tableau
 * @param proba la probabilité de créer une arête entre 2 sommets connexes
 *
 * @return graphe un arbre couvrant
 */

void fisherYates (graphe_t * G);

#endif
