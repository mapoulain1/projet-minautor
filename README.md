# Projet Minautore

ISIMA - Projet de fin d'année

## Organisation du projet

Le projet est hébergé sur [gitlab](https://gitlab.isima.fr/mapoulain1/projet-minautor).

### Site

[site](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/site) sont les pages du site web hébergé sur [https://perso.isima.fr/~lihiault/projet-minautor/site/index.html].

### Resources
[resources](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/resources) contient des GIF et images utilisés dans le README.

### Exercices
Le répertoire [exercices](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/)  contient les différents exercices à réaliser.

#### Ouverture fenêtres
L'exercice d'ouvertures de fenêtres est dans le répertorie [ouverture-fenetre](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/ouverture-fenetre). Une fenêtre bleue avec une couleur de fond bleue et un icone bleu.

#### X-Fenêtre
[x-fenetre](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/x-fenetre) ouvre 5 fenêtres au centre de l'écran puis les déplace pour former un X.

[x-fenetre_v2](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/x-fenetre_v2) affiche des fenêtres en diagonale puis les déplace jusqu'au bord gauche de l'écran.

#### Pavé serpent
[pave-serpent](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/pave-serpents) affiche plusieurs carré bleus de taille et transparence différente dans une fenêtre.

[pave-serpent_v2](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/pave-serpents_v2) affiche un carré rouge qui se déplace et rebondit contre les bords de la fenêtre.


#### Game of life
Le jeu de la vie est dans le répertorie [game-of-life](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/game-of-life).


<img src="resources/game-of-life.gif">
Game of life avec la règle de base de Conway.

#### Game of life V2
Le jeu de la vie est dans le répertorie [game-of-life-v2](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/game-of-life-v2).

<img src="resources/game-of-life-v2.gif">
Game of life avec la règle Walled Cities.


Usage :

```
make
./out [taille grille] [pourcentage remplissage] [index règle]
```

La taille par défaut est 140, le pourcentage est 20% et la règle est celle de Conway : B3-S23 

##### Contrôles

| Touche | Fonction |
| ------ | ------ |
| P | Pause |
| Espace | Pause |
| Haut | Augmenter la taille du pinceau |
| Bas | Diminuer la taille du pinceau |
| Gauche | Augmenter les FPS |
| Droite | Diminuer les FPS |
| Échape | Quitter |


##### Règles du jeu classique

| Index | Nom | Type | Survie | Naissance |
| ------ | ------ | ------ | ------ | ------ |
| 1 | 2 x 2 | chaotique | 1,2,5 | 3,6 |
| 2 | 34 Life | exponentiel | 3,4 | 3,4 |
| 3 | Amoeba | chaotique | 1,3,5,8 | 3,5,7 |
| 4 | Assimilation | stable | 4,5,6,7 | 3,4,5 |
| 5 | Coagulation | exponentiel | 2,3,6,7,8 | 3,7,8 |
| 6 | Conway's Life | chaotique | 2,3 | 3 |
| 7 | Coral | exponentiel | 4,5,6,7,8 | 3 |
| 8 | Day & Night  | stable | 3,4,6,7,8 | 3,6,7,8 |
| 9 | Diamoeba | chaotique | 5,6,7,8 | 3,5,7,8 |
| 10 | Flakes | expansion | 0,1,2,3,4,5,6,7,8 | 3 |
| 11 | Gnarl | exponentiel | 1 | 1 |
| 12 | High | chaotique | 2,3 | 3,6 |
| 13 | Inverse Life | chaotique | 3,4,6,7,8 | 0,1,2,3,4,7,8 |
| 14 | Long Life | stable | 5 | 3,4,5 |
| 15 | Maze | exponentiel | 1,2,3,4,5 | 3 |
| 16 | Mazectric | exponentiel | 1,2,3,4 | 3 |
| 17 | Move | stable | 2,4,5 | 3,6,8 |
| 18 | Pseudo Life | chaotique | 2,3,8 | 3,5,7 |
| 19 | Replicator | exponentiel | 1,3,5,7 | 1,3,5,7 |
| 20 | Stains | stable | 2,3,5,6,7,8 | 3,6,7,8 |
| 21 | Walled Cities | stable | 2,3,4,5 | 4,5,6,7,8 |


#### Notre chef d'oeuvre

Notre chef d'oeuvre est dans le répertorie [un-chef-d-oeuvre](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/un-chef-d-oeuvre).

Il s'agit d'un petit jeu de survie dans l'espace, à bord d'un magnifique vaisseau. Le but est simplement de survivre le plus longtemps aux attaques ennemies.

##### Contrôles

| Touche | Fonction |
| ------ | ------ |
| Page Suivante (_Page Up_) | Augmenter volume |
| Page Précédente (_Page Down_) | Diminuer volume |
| M | Éteidre la musique |
| P | Pause |
| Haut | Monter le vaisseau |
| Bas | Baisser le vaisseau |
| F8 | Afficher les hitboxs |
| F9 | Test sonnore |
| F10 | Mettre fin à la partie |
| Échape | Quitter |





<table>
    <tbody>
        <tr>
            <td>
            <img src="resources/chefdoeuvre_gameplay.gif" width="300" height="168">
            <br/>
                Une petite séquence de gameplay.
            </td>
            <td>
            <img src="resources/chefdoeuvre_gameover.gif" width="300" height="168">
            <br/>
                L'animation de gameover.
            </td>
        </tr>
        <tr>
            <td>
            <img src="resources/chefdoeuvre_hitbox.gif" width="300" height="168">
            <br/>
                Le système de hit box du jeu pour les collisions.
            </td>
            <td>
                <img src="resources/chefdoeuvre_background.gif" width="300" height="168">
                <br/>
                Le défilement parallaxe du fond, pour un rendu plus joli qu'une simple image.
            </td>
        </tr>
        <tr>
            <td>
                <img src="resources/chefdoeuvre_ship.gif" width="300" height="168">
                <br/>
                Le déplacement de notre magnifique vaisseau.
            </td>
            <td>
                <img src="resources/chefdoeuvre_enemies.gif" width="300" height="168">
                <br/>
                Les déplacements de nos affreux assaillants.
            </td>
        </tr>
    </tbody>
</table>


#### Un chef d'oeuvre

[un-chef-d-oeuvre](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/exercices/un-chef-d-oeuvre) est un petit jeu de survie dans l'espace, à bord d'un magnifique vaisseau. Le but est simplement de survivre le plus longtemps aux attaques ennemies.

### Tas

[tas](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/tas) les fonctions élémentaire de traitement d'un tas minimum.

### Partition

[partition](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/partition) contient les fonctions élémentaires sur les partitions.

### Graphes connexes

[composantes-connexes](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/composantes-connexes) regroupe les fonction de traitement de graphes.

### Parcours graphe

[parcours-graphe](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/parcours-graphe) contient les algorithme de parcours en profondeur, Dijkstra et A*.

### dijkstraVisuel
[dijkstraVisuel](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/dijkstraVisuel) permet de visualiser à l'aide d'un parcours graphique d'un labyrinthe l'exécution de l'algorithme de Dijkstra.

### Proof Server
[proofServer](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/proof-server) contient les fichiers permettant de mettre en place un serveur et des clients pour le labyrinthe.

### Labyrinthe
[labyrinthe](https://gitlab.isima.fr/mapoulain1/projet-minautor/-/tree/master/labyrinthe) est l'endroit où se trouve la partie principale de notre projet : un jeu multijoueur où des chevaliers doivent chasser un fantôme.


## Liens

### Site web

[Site web](https://perso.isima.fr/~lihiault/)

### Planning

[Planning prévisionnel](https://docs.google.com/spreadsheets/d/1m3hWujrRGWFD5-fpMUccFbEWyKcN5hB4GIqsqXXZfag/edit?usp=sharing)



[Planning réel](https://docs.google.com/spreadsheets/d/1mwoolevxGZFNck2OIJW9hnfD5uAMVuOEaPmC3RxvkMc/edit?usp=sharing)



