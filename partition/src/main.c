#include "partition.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define PARTITION_SIZE 10
#define PARTITION_BIG_SIZE 100

void exemplePartition(void) {
	//srand(time(NULL));

	partition_t p = partitionCreer(PARTITION_SIZE);
	int e1, e2;

	for (int i = 0; i < 99; i += 2) {
		e1=rand()%PARTITION_SIZE;
		e2=rand()%PARTITION_SIZE;
		partitionFusion(&p, e1,e2);
		//printf("Fusion : %d %d \n", e1, e2);
		//partitionAfficher(&p);
		//partitionAfficherPretty(&p);
		//printf("\n\n\n");
	}

	partitionAfficher(&p);
	partitionAfficherPretty(&p);

	partitionToDotFile(&p, "test.dot");
	partitionOpenImage("test.dot");

	printf("Number of classes : %d\n",partitionNumberOfClass(&p));

	partitionLiberer(&p);
	partitionCleanFiles();
}

void exempleBigData(void) {
	srand(time(NULL));

	partition_t p = partitionCreer(PARTITION_BIG_SIZE);

	for (int i = 0; i < 350; i++) {
		partitionFusion(&p, rand() % PARTITION_BIG_SIZE, rand() % PARTITION_BIG_SIZE);
	}

	partitionAfficherPretty(&p);

	partitionToDotFile(&p, "test.dot");
	partitionOpenImage("test.dot");

	partitionLiberer(&p);
	partitionCleanFiles();
}

int main(int argc, char **argv) {
	(void)argc;
	(void)argv;
	exemplePartition();
	//exempleBigData();

	return 0;
}
