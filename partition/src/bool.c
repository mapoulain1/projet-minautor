#include "bool.h"

#include <stdio.h>

void printBoolArray(bool * array, int size){
    if(array){
        for (int i = 0; i < size; i++) {
           if(array[i]){
               printf("%d  ",i );
           }
        }
        printf("\n");
    }
}