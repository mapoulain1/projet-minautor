#include "math.h"

int mod(int i, int m) {
	if (i <= -1)
		return m - 1;
	return i % m;
}

int clamp(int val, int min, int max) {
	if (val > max)
		return max;
	if (val < min)
		return min;
	return val;
}

double clampDouble(double val, double min, double max) {
	if (val > max)
		return max;
	if (val < min)
		return min;
	return val;
}



int array2Dto1D(int i, int j, int width){
	return i*width+j;
}

void array1Dto2D(int value, int *iOut, int *jOut, int width){
	*iOut = value / width;
	*jOut = value % width;
}

int max(int a, int b)
{
    return (a > b) ? a : b;
}
