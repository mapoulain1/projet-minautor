#ifndef CONFIG_H
#define CONFIG_H

typedef struct {
	int worldSize;
} config_t;

config_t configNew(int worldSize);
void configPrint(config_t config);

#endif