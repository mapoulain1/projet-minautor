#include "config.h"

#include <stdio.h>
#include <string.h>

config_t configNew(int worldSize) {
	config_t config;
	config.worldSize = worldSize;
	return config;
}

void configPrint(config_t config) {
	printf("CONFIG :\n");
	printf("World size : %d\n", config.worldSize);
}