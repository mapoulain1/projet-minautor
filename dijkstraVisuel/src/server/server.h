#ifndef SERVER_H_
#define SERVER_H_

#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>

#include "../utils/bool.h"
#include "../world/internal/player.h"

#define MAX_CLIENTS 10
#define LONGER_BUFFER 16384
#define LONG_BUFFER 64
#define SHORT_BUFFER 8


typedef struct{
	int socketDescriptor;
	struct sockaddr_in server;
    bool online;
}server_t;

server_t serverConnect(char *hostname, int port);
void serverDisconnect(server_t server);
void serverSend(server_t server,player_t *player);
void serverRead(server_t server, player_t *players);
int serverGetPlayerID(server_t server);
void serverGetLaby(server_t server, labyrinthe_t * out);


#endif