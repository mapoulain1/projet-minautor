#include "server.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../utils/app.h"
#include "../utils/color.h"

server_t serverConnect(char *hostname, int port) {
	server_t server;
	server.online = false;
	char serverVersion[SHORT_BUFFER];
	printf(GRE "[CLIENT] Connecting : %s:%d\n" RESET, hostname, port);

	server.socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (server.socketDescriptor == -1) {
		fprintf(stderr, RED "[CLIENT] Can't create socket\n" RESET);
	} else {
		server.server.sin_addr.s_addr = inet_addr(hostname);
		server.server.sin_family = AF_INET;
		server.server.sin_port = htons(port);

		if (connect(server.socketDescriptor, (struct sockaddr *)&(server.server), sizeof(server)) >= 0) {
			printf(GRE "[CLIENT] Connection established to %s:%d\n" RESET, hostname, port);
			server.online = true;
			read(server.socketDescriptor, serverVersion, SHORT_BUFFER);
			if (atoi(serverVersion) != APP_VERSION) {
				printf(RED "[CLIENT] Mismatch version <server : %d > <client : %d>\n" RESET, atoi(serverVersion), APP_VERSION);
			} else {
				printf(GRE "[CLIENT] Server running on version %d\n" RESET, APP_VERSION);
			}

		} else {
			printf(RED "[CLIENT] Can't connect to %s:%d\n" RESET, hostname, port);
		}
	}
	return server;
}

void serverDisconnect(server_t server) {
	if (server.online) {
		shutdown(server.socketDescriptor, SHUT_RDWR);
		server.online = false;
	}
}

void serverSend(server_t server, player_t *player) {
	if (server.online) {
		char tmp[LONG_BUFFER];
		playerSerialize(player, tmp);
		if (send(server.socketDescriptor, tmp, LONG_BUFFER, 0) < 0) {
			fprintf(stderr, RED "[CLIENT] Can't send data\n" RESET);
		}
	}
}

void serverRead(server_t server, player_t *players) {
	if (server.online) {
		char tmp[LONG_BUFFER];
		player_t tmpPlayer;
		for (int i = 0; i < MAX_CLIENTS; i++) {
			read(server.socketDescriptor, tmp, LONG_BUFFER);
			playerDeserialize(&tmpPlayer, tmp);
			players[tmpPlayer.id] = tmpPlayer;
		}
	}
}

int serverGetPlayerID(server_t server) {
	if (server.online) {
		char playerID[SHORT_BUFFER];
		read(server.socketDescriptor, playerID, SHORT_BUFFER);
		return atoi(playerID);
	}
	fprintf(stderr, "Error can't get player ID\n");
	return -1;
}

void serverGetLaby(server_t server, labyrinthe_t *out) {
	if (server.online) {
		char tmp[LONGER_BUFFER];
		read(server.socketDescriptor, tmp, LONGER_BUFFER);
		labytintheDeserialize(out, tmp);
		if (out->size <= 0) {
			fprintf(stderr, "Error can't get maze from server\n");
			return;
		}
	}
}