#include "player.h"

#include <SDL2/SDL.h>
#include <stdio.h>

#include "../../process/collisionHandler.h"
#include "../../utils/error.h"
#include "../../utils/math.h"
#include "labyrinthe.h"

void playerInit(player_t *out, int xWorld, int yWorld, int tileSize, int id) {
	if (out) {
		out->health = PLAYER_DEFAULT_HEALTH;
		out->id = id;
		out->w = PLAYER_WIDTH;
		out->h = PLAYER_HEIGHT;
		out->xWorldPosition = xWorld;
		out->yWorldPosition = yWorld;
		out->x = xWorld * tileSize + (tileSize - PLAYER_WIDTH) / 2;
		out->y = yWorld * tileSize + (tileSize - PLAYER_HEIGHT) / 2;
		out->direction = DIR_NONE;
		out->moving = false;
		out->lookingLeft = false;
		out->connected = true;
	} else
		logError(NULL_POINTER);
}

void playerInflictDamage(player_t *out) {
	if (out) {
		out->health -= clamp(out->health - PLAYER_DAMAGE, 0, PLAYER_DEFAULT_HEALTH);
	}
}

void playerKill(player_t *out) {
	if (out) {
		out->health = 0;
	}
}

void playerUpdateCollision(SDL_Renderer *renderer, player_t *out, labyrinthe_t *laby, direction_t direction, bool moving, camera_t *camera) {
	if (out) {
		out->moving = moving;
		if (moving) {

			SDL_Rect rect = {(int)out->x + camera->xOffSet + ((float)0.2 * PLAYER_HEIGHT / CAMERA_DEFAULT_TILESIZE) * camera->tileSize,
							 (int)out->y + camera->yOffSet + ((float)0.8*PLAYER_HEIGHT / CAMERA_DEFAULT_TILESIZE) * camera->tileSize,
							 clamp(((float)0.6*PLAYER_WIDTH / CAMERA_DEFAULT_TILESIZE) * camera->tileSize, 5, INT32_MAX),
							 clamp(((float)0.2*PLAYER_HEIGHT / CAMERA_DEFAULT_TILESIZE) * camera->tileSize, 5, INT32_MAX)};

			SDL_Rect walls[4 * 9];
			int index = 0;
			bool moved = false;

			collisionGenerateRects(walls, &index, laby, out, camera);

			if (camera->renderHitbox) {
				SDL_SetRenderDrawColor(renderer, 0, 255, 0, 100);
				SDL_RenderDrawRect(renderer, &rect);
				SDL_SetRenderDrawColor(renderer, 0, 0, 255, 0);
				for (int h = 0; h < index; h++) {
					SDL_RenderDrawRect(renderer, &walls[h]);
					if (SDL_HasIntersection(&rect, &walls[h])) {
						SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
						SDL_RenderDrawRect(renderer, &walls[h]);
						SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
					}
				}
			}

			if (!moved && !collidedRectWithRects(walls, index, &rect, direction, camera)) {
				if (direction == DIR_DOWN) {
					out->y += PLAYER_SPEED * camera->tileSize;
				}
				if (direction == DIR_UP) {
					out->y -= PLAYER_SPEED * camera->tileSize;
				}
				if (direction == DIR_RIGHT) {
					out->x += PLAYER_SPEED * camera->tileSize;
					out->lookingLeft = false;
				}
				if (direction == DIR_LEFT) {
					out->x -= PLAYER_SPEED * camera->tileSize;
					out->lookingLeft = true;
				}
				moved = true;
			}
		}
	}
}

void playerStop(player_t *out) {
	if (out) {
		out->moving = false;
	}
}

void playerSerialize(player_t *player, char *out){
    sprintf(out,"%d %.2f %.2f %d %d %d %.2f %d %d %d\n",player->id,player->x,player->y,player->w,player->h,player->connected,player->health,player->direction,player->lookingLeft,player->moving);
}

void playerDeserialize(player_t *out, char *data){
    if(sscanf(data,"%d %f %f %d %d %d %f %d %d %d",&(out->id),&(out->x),&(out->y),&(out->w),&(out->h),(int*)&(out->connected),&(out->health),(int*)&(out->direction),(int*)&(out->lookingLeft),(int*)&(out->moving)) != 10){
        out->connected=false;
    }
}