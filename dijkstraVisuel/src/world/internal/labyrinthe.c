#include "labyrinthe.h"

#include "../../utils/error.h"
#include "../../utils/math.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

labyrinthe_t labyrintheInit(graphe_t *graph) {
	labyrinthe_t laby = {0, NULL};
	int convertedI1, convertedJ1, convertedI2, convertedJ2;

	int integerSqrt = (int)sqrt(graph->nbNoeuds);
	if (integerSqrt * integerSqrt != graph->nbNoeuds) {
		fprintf(stderr, "Warning : Graph doesn't containe the right number of nodes to make a 2D square\n");
		return laby;
	}
	laby.size = integerSqrt;
	laby.data = malloc(sizeof(tile_t *) * laby.size);
	if (!laby.data) {
		logError(ALLOC_ERROR);
		laby.size = 0;
		return laby;
	}
	for (int i = 0; i < laby.size; i++) {
		laby.data[i] = malloc(sizeof(tile_t) * laby.size);
		if (!laby.data[i]) {
			free(laby.data);
			logError(ALLOC_ERROR);
			laby.size = 0;
			return laby;
		}
		for (int j = 0; j < laby.size; j++) {
			laby.data[i][j] = TILE_CLOSED;
		}
	}
	for (int h = 0; h < graph->nbArretes; h++) {
		convertedI1 = graph->i[h] / laby.size;
		convertedJ1 = graph->i[h] % laby.size;
		convertedI2 = graph->j[h] / laby.size;
		convertedJ2 = graph->j[h] % laby.size;

		if (convertedJ1 == convertedJ2 + 1 && convertedI1 == convertedI2) {
			tileRemoveFlags(&laby.data[convertedI1][convertedJ1], TILE_CLOSED_LEFT);
			tileRemoveFlags(&laby.data[convertedI2][convertedJ2], TILE_CLOSED_RIGHT);
		}
		if (convertedJ1 == convertedJ2 - 1 && convertedI1 == convertedI2) {
			tileRemoveFlags(&laby.data[convertedI1][convertedJ1], TILE_CLOSED_RIGHT);
			tileRemoveFlags(&laby.data[convertedI2][convertedJ2], TILE_CLOSED_LEFT);
		}
		if (convertedI1 == convertedI2 + 1 && convertedJ1 == convertedJ2) {
			tileRemoveFlags(&laby.data[convertedI1][convertedJ1], TILE_CLOSED_UP);
			tileRemoveFlags(&laby.data[convertedI2][convertedJ2], TILE_CLOSED_DOWN);
		}
		if (convertedI1 == convertedI2 - 1 && convertedJ1 == convertedJ2) {
			tileRemoveFlags(&laby.data[convertedI1][convertedJ1], TILE_CLOSED_DOWN);
			tileRemoveFlags(&laby.data[convertedI2][convertedJ2], TILE_CLOSED_UP);
		}
	}

	return laby;
}

void labyrintheUpdate(labyrinthe_t *out, graphe_t *graph) {
	labyrintheFree(out);
	*out = labyrintheInit(graph);
}

void labyrintheFree(labyrinthe_t *laby) {
	if (laby) {
		if (laby->data) {
			for (int i = 0; i < laby->size; i++) {
				if (laby->data[i])
					free(laby->data[i]);
			}
		}
	}
}

void labyrintheDebug(labyrinthe_t *laby) {
	if (laby) {

		for (int i = 0; i < laby->size; i++) {
			for (int j = 0; j < laby->size; j++) {
				tileDebug(laby->data[i][j]);
			}
			printf("\n");
		}
	}
}

bool labyrintheAbleToGo(labyrinthe_t *laby, int fromX, int fromY, direction_t direction) {
	if (laby) {
		switch (direction) {
		case DIR_DOWN:
			return !isTile(laby->data[fromY][fromX], TILE_CLOSED_DOWN);
		case DIR_UP:
			return !isTile(laby->data[fromY][fromX], TILE_CLOSED_UP);
		case DIR_LEFT:
			return !isTile(laby->data[fromY][fromX], TILE_CLOSED_LEFT);
		case DIR_RIGHT:
			return !isTile(laby->data[fromY][fromX], TILE_CLOSED_RIGHT);
		default:
			return true;
		}
	}
	return true;
}

void labytintheDeserialize(labyrinthe_t *out, char *data) {
	if (out && data) {
		const int len = 3;
		char *tmp = data;
		sscanf(data, "%d", &out->size);

		out->data = malloc(sizeof(tile_t *) * out->size);
		if (!out->data) {
			logError(ALLOC_ERROR);
			out->size = 0;
			return;
		}
		for (int i = 0; i < out->size; i++) {
			out->data[i] = malloc(sizeof(tile_t) * out->size);
			if (!out->data[i]) {
				free(out->data);
				logError(ALLOC_ERROR);
				out->size = 0;
				return;
			}
		}
		tmp += len + 1;
		for (int i = 0; i < out->size; i++) {
			for (int j = 0; j < out->size; j++) {
				sscanf(tmp, "%d", (int *)&(out->data[i][j]));
				tmp += len;
			}
		}
	}
}
