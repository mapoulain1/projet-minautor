#include "tile.h"

#include <stdio.h>

void tileDebug(tile_t tile) {
	printf(BYTE_TO_BINARY_PATTERN" ", BYTE_TO_BINARY(tile));
}

bool isTile(tile_t toTest, tile_t value){
	return (toTest & value)==value;
}

void tileRemoveFlags(tile_t *toModify, tile_t value){
	*toModify =*toModify & ~value;
}