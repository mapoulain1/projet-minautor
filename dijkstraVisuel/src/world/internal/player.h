#ifndef PLAYER_H_
#define PLAYER_H_



#include "../../utils/bool.h"
#include "../../utils/direction.h"
#include "labyrinthe.h"

#include <SDL2/SDL.h>

#define PLAYER_DEFAULT_HEALTH 100
#define PLAYER_DEFAULT_ID 1
#define PLAYER_WIDTH 64
#define PLAYER_HEIGHT 80
#define PLAYER_DAMAGE 2
#define PLAYER_SPEED 0.04
#define PLAYER_COLLISION_HELPER_X 10
#define PLAYER_COLLISION_HELPER_Y 50



typedef struct {
	float health;
	float x;
	float y;
	int w;
	int h;
	int id;
	direction_t direction;
	bool moving;
	bool lookingLeft;
	int xWorldPosition;
	int yWorldPosition;
	bool connected;
} player_t;


#include "../../graphics/camera.h"

void playerInit(player_t *out, int xWorld, int yWorld, int tileSize, int id);
void playerInflictDamage(player_t *out);
void playerKill(player_t *out);
void playerUpdateCollision(SDL_Renderer * renderer, player_t *out,labyrinthe_t *laby, direction_t direction, bool moving, camera_t *camera);
void playerStop(player_t *out);

void playerSerialize(player_t *player, char *out);
void playerDeserialize(player_t *out, char *data);

#endif