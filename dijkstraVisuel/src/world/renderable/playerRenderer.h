#ifndef PLAYERRENDERER_H_
#define PLAYERRENDERER_H_

#include <SDL2/SDL.h>

#include "../internal/player.h"
#include "../../graphics/camera.h"

#define PLAYERRENDER_NUMBER_OF_SPRITES 3 
#define PLAYERRENDER_PATH_TO_TEXTURE "textures/player/"
#define PLAYERRENDER_SPRITE_WIDTH 16
#define PLAYERRENDER_SPRITE_HEIGHT 20

#define PLAYERRENDER_FRAMESKIP 5


void renderPlayerInit(SDL_Renderer *renderer);
void renderPlayerFree(void);
void renderPlayerDebug(SDL_Renderer *renderer, player_t *player,camera_t *camera);
void renderPlayer(SDL_Renderer *renderer, player_t *player,camera_t *camera);



#endif