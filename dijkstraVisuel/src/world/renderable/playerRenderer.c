#include "playerRenderer.h"

#include <SDL2/SDL_image.h>

#include "../../server/server.h"
#include "../../utils/error.h"
#include "../../utils/math.h"

struct {
	SDL_Rect sources[MAX_CLIENTS][PLAYERRENDER_NUMBER_OF_SPRITES];
	SDL_Texture *texture[MAX_CLIENTS];
	int animationFrameCount[MAX_CLIENTS];
	int currentFrame[MAX_CLIENTS];
} playerAssets;

void renderPlayerInit(SDL_Renderer *renderer) {
	char tmp[128];
	for (int i = 0; i < MAX_CLIENTS; i++) {
		sprintf(tmp, "%splayer%d.png", PLAYERRENDER_PATH_TO_TEXTURE, i);
		playerAssets.texture[i] = IMG_LoadTexture(renderer, tmp);
		playerAssets.animationFrameCount[i] = 0;
		playerAssets.currentFrame[i] = 0;
		if (!playerAssets.texture[i])
			logError(FILE_NOT_FOUND);
		for (int h = 0; h < PLAYERRENDER_NUMBER_OF_SPRITES; h++) {
			playerAssets.sources[i][h].h = PLAYERRENDER_SPRITE_HEIGHT;
			playerAssets.sources[i][h].w = PLAYERRENDER_SPRITE_WIDTH;
			playerAssets.sources[i][h].x = h * PLAYERRENDER_SPRITE_WIDTH;
			playerAssets.sources[i][h].y = 0;
		}
	}
}

void renderPlayerFree(void) {
	for (int i = 0; i < MAX_CLIENTS; i++) {
		SDL_DestroyTexture(playerAssets.texture[i]);
	}
}

void renderPlayerDebug(SDL_Renderer *renderer, player_t *player, camera_t *camera) {
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
	SDL_Rect dest = {(int)player->x + camera->xOffSet, (int)player->y + camera->yOffSet, ((float)PLAYER_WIDTH / CAMERA_DEFAULT_TILESIZE) * camera->tileSize, ((float)PLAYER_HEIGHT / CAMERA_DEFAULT_TILESIZE) * camera->tileSize};
	SDL_RenderDrawRect(renderer, &dest);
}

void renderPlayer(SDL_Renderer *renderer, player_t *player, camera_t *camera) {
	SDL_RendererFlip flip = SDL_FLIP_NONE;
	SDL_Rect dest = {(int)player->x + camera->xOffSet, (int)player->y + camera->yOffSet, ((float)PLAYER_WIDTH / CAMERA_DEFAULT_TILESIZE) * camera->tileSize, ((float)PLAYER_HEIGHT / CAMERA_DEFAULT_TILESIZE) * camera->tileSize};
	int id = player->id;

	if (player->lookingLeft) {
		flip = SDL_FLIP_HORIZONTAL;
	}
	SDL_RenderCopyEx(renderer, playerAssets.texture[id], &playerAssets.sources[id][playerAssets.currentFrame[id]], &dest, 0.0, NULL, flip);

	if (player->moving && playerAssets.animationFrameCount[id] == 0) {
		playerAssets.currentFrame[id] = (playerAssets.currentFrame[id] + 1) % PLAYERRENDER_NUMBER_OF_SPRITES;
	}
	playerAssets.animationFrameCount[id] = (playerAssets.animationFrameCount[id] + 1) % PLAYERRENDER_FRAMESKIP;
}
