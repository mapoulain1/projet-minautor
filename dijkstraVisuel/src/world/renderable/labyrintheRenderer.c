#include "labyrintheRenderer.h"

#include "../../utils/error.h"
#include "../../utils/math.h"
#include <SDL2/SDL_image.h>

struct {
	SDL_Texture *textures[TILE_CLOSED];
	SDL_Texture *ground;
} labyAssets;

void renderLabyrintheInit(SDL_Renderer *renderer) {
	labyAssets.textures[TILE_CLOSED_DOWN] = IMG_LoadTexture(renderer, WALLS_PATH_TO_TEXTURE "closedDown.png");
	labyAssets.textures[TILE_CLOSED_UP] = IMG_LoadTexture(renderer, WALLS_PATH_TO_TEXTURE "closedUp.png");
	labyAssets.textures[TILE_CLOSED_LEFT] = IMG_LoadTexture(renderer, WALLS_PATH_TO_TEXTURE "closedLeft.png");
	labyAssets.textures[TILE_CLOSED_RIGHT] = IMG_LoadTexture(renderer, WALLS_PATH_TO_TEXTURE "closedRight.png");
	labyAssets.ground = IMG_LoadTexture(renderer, GROUND_PATH_TO_TEXTURE);

	if (labyAssets.textures[TILE_CLOSED_DOWN] == NULL || labyAssets.textures[TILE_CLOSED_UP] == NULL || labyAssets.textures[TILE_CLOSED_LEFT] == NULL || labyAssets.textures[TILE_CLOSED_RIGHT] == NULL || labyAssets.ground == NULL)
		logError(FILE_NOT_FOUND);
}

void renderLabyrintheFree(void) {
	SDL_DestroyTexture(labyAssets.textures[TILE_CLOSED_DOWN]);
	SDL_DestroyTexture(labyAssets.textures[TILE_CLOSED_UP]);
	SDL_DestroyTexture(labyAssets.textures[TILE_CLOSED_LEFT]);
	SDL_DestroyTexture(labyAssets.textures[TILE_CLOSED_RIGHT]);
	SDL_DestroyTexture(labyAssets.ground);
}

void renderLabyrintheDebug(SDL_Renderer *renderer, labyrinthe_t *laby, camera_t *camera) {
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	for (int i = 0; i < laby->size; i++) {
		for (int j = 0; j < laby->size; j++) {
			if (isTile(laby->data[j][i], TILE_CLOSED_DOWN)) {
				SDL_RenderDrawLine(renderer, (i)*camera->tileSize + camera->xOffSet, (j + 1) * camera->tileSize + camera->yOffSet, (i + 1) * camera->tileSize + camera->xOffSet, (j + 1) * camera->tileSize + camera->yOffSet);
			}
			if (isTile(laby->data[j][i], TILE_CLOSED_UP)) {
				SDL_RenderDrawLine(renderer, (i)*camera->tileSize + camera->xOffSet, (j)*camera->tileSize + camera->yOffSet, (i + 1) * camera->tileSize + camera->xOffSet, (j)*camera->tileSize + camera->yOffSet);
			}
			if (isTile(laby->data[j][i], TILE_CLOSED_LEFT)) {
				SDL_RenderDrawLine(renderer, (i)*camera->tileSize + camera->xOffSet, (j)*camera->tileSize + camera->yOffSet, (i)*camera->tileSize + camera->xOffSet, (j + 1) * camera->tileSize + camera->yOffSet);
			}
			if (isTile(laby->data[j][i], TILE_CLOSED_RIGHT)) {
				SDL_RenderDrawLine(renderer, (i + 1) * camera->tileSize + camera->xOffSet, (j)*camera->tileSize + camera->yOffSet, (i + 1) * camera->tileSize + camera->xOffSet, (j + 1) * camera->tileSize + camera->yOffSet);
			}
		}
	}
}

void renderLabyrinthe(SDL_Renderer *renderer, labyrinthe_t *laby, camera_t *camera, int layerRender) {
	SDL_Rect dest = {0, 0, camera->tileSize, camera->tileSize};
	SDL_Rect ground = {0, 0, camera->tileSize * 2, camera->tileSize * 2};

	SDL_RendererFlip flip;

	if (layerRender == WALLS_RENDER_LAYER_BACK) {
		for (int i = 0; i < laby->size; i += 2) {
			for (int j = 0; j < laby->size; j += 2) {
				flip = laby->data[i][j] % 3;
				ground.x = i * camera->tileSize + camera->xOffSet;
				ground.y = j * camera->tileSize + camera->yOffSet;
				SDL_RenderCopyEx(renderer, labyAssets.ground, NULL, &ground, 0, NULL, flip);
			}
		}
		for (int i = 0; i < laby->size; i++)
			for (int j = 0; j < laby->size; j++)
				if (isTile(laby->data[j][i], TILE_CLOSED_UP)) {
					dest.x = (i)*camera->tileSize + camera->xOffSet;
					dest.y = (j)*camera->tileSize + camera->yOffSet;
					SDL_RenderCopy(renderer, labyAssets.textures[TILE_CLOSED_UP], NULL, &dest);
				}

		for (int i = 0; i < laby->size; i++) {
			for (int j = 0; j < laby->size; j++) {
				dest.x = (i)*camera->tileSize + camera->xOffSet;
				dest.y = (j)*camera->tileSize + camera->yOffSet;
				if (isTile(laby->data[j][i], TILE_CLOSED_LEFT)) {
					SDL_RenderCopy(renderer, labyAssets.textures[TILE_CLOSED_LEFT], NULL, &dest);
				}
				if (isTile(laby->data[j][i], TILE_CLOSED_RIGHT)) {
					SDL_RenderCopy(renderer, labyAssets.textures[TILE_CLOSED_RIGHT], NULL, &dest);
				}
			}
		}
	}
	if (layerRender == WALLS_RENDER_LAYER_FRONT) {
		for (int i = 0; i < laby->size; i++)
			for (int j = 0; j < laby->size; j++)
				if (isTile(laby->data[j][i], TILE_CLOSED_DOWN)) {
					dest.x = (i)*camera->tileSize + camera->xOffSet;
					dest.y = (j)*camera->tileSize + camera->yOffSet;
					SDL_RenderCopy(renderer, labyAssets.textures[TILE_CLOSED_DOWN], NULL, &dest);
				}
	}
}

void renderCase(SDL_Renderer *renderer, int nbNoeud, int width, int r, int g, int b) {
	int i, j;
	array1Dto2D(nbNoeud, &i, &j, width);
	SDL_Rect rect = {i * CAMERA_DEFAULT_TILESIZE + 1, j * CAMERA_DEFAULT_TILESIZE + 1, CAMERA_DEFAULT_TILESIZE - 2, CAMERA_DEFAULT_TILESIZE - 2};

	SDL_SetRenderDrawColor(renderer, r, g, b, 255);
	SDL_RenderFillRect(renderer, &rect);
	SDL_RenderPresent(renderer);
	SDL_Delay(50);
}