#ifndef LABYRINTHERENDERER_H_
#define LABYRINTHERENDERER_H_

#include <SDL2/SDL.h>

#include "../internal/labyrinthe.h"
#include "../../graphics/camera.h"

#define WALLS_PATH_TO_TEXTURE "textures/walls/"
#define GROUND_PATH_TO_TEXTURE "textures/background/ground.png"


#define WALLS_RENDER_LAYER_BACK 0
#define WALLS_RENDER_LAYER_FRONT 1


void renderLabyrintheInit(SDL_Renderer *renderer);
void renderLabyrintheFree(void);

void renderLabyrintheDebug(SDL_Renderer * renderer, labyrinthe_t * laby,camera_t *camera);
void renderLabyrinthe(SDL_Renderer * renderer, labyrinthe_t * laby,camera_t *camera, int layerRender);

void renderCase(SDL_Renderer * renderer, int nbNoeud, int width, int r, int g, int b);


#endif