#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graph/parcours-graphe.h"
#include "graphics/camera.h"
#include "graphics/graphics.h"
#include "server/server.h"
#include "utils/bool.h"
#include "utils/math.h"
#include "world/internal/labyrinthe.h"
#include "world/internal/player.h"
#include "world/internal/tile.h"
#include "world/renderable/labyrintheRenderer.h"
#include "world/renderable/playerRenderer.h"

#include "graph/distance.h"

const int frameMS = 16;

config_t gameConfig;
labyrinthe_t laby;
graphe_t *graph;
graphe_t *graphKruskal;

camera_t camera;

void gameSetup(config_t config) {
	//srand(time(NULL));
	srand(6);
	gameConfig = config;
}

void gameStart(SDL_Window *window) {
	int from = gameConfig.worldSize / 2 + sqrt(gameConfig.worldSize);
	int to = gameConfig.worldSize - 1;

	SDL_Renderer *renderer = SDL_GetRenderer(window);

	graph = graphCreerPourLaby(gameConfig.worldSize);
	graphKruskal = kruskal(graph, fisherYates, 0.04);
	laby = labyrintheInit(graph);
	graphDisplay(graph);

	cameraInit(&camera);

	gameRenderGame(window);
	renderCase(renderer,from,sqrt(gameConfig.worldSize),0,0,255);
	renderCase(renderer,to,sqrt(gameConfig.worldSize),255,0,0);
	SDL_RenderPresent(renderer);
	SDL_Delay(1000);


    printf("from:%d | to:%d\n", from, to);
	/* dijkstra(graph, from, to, sqrt(gameConfig.worldSize), renderer); */
	aEtoile(graph, from,euclidienne, sqrt(gameConfig.worldSize),to, renderer);
    /* DFS(graph, from, sqrt(gameConfig.worldSize), renderer); */
	SDL_Delay(1000);

	graphLiberer(&graph);
	graphLiberer(&graphKruskal);
	labyrintheFree(&laby);
	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
}

void gameRenderGame(SDL_Window *window) {
	int windowWidth, windowHeight;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer);
	renderLabyrintheDebug(renderer, &laby, &camera);
}
