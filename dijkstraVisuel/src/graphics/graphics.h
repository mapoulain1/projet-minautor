#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "../utils/error.h"

#include <SDL2/SDL.h>

error_t gcsInit(void);
void gcsFree(void);

error_t gcsNewWindow(const char *title, int width, int height, SDL_Window **out);
void gcsFreeWindow(SDL_Window **window);

error_t gcsNewRenderer(SDL_Window *window, SDL_Renderer **out);
void gcsFreeRenderer(SDL_Renderer **renderer);

void gcsClearBackground(SDL_Renderer *renderer);

#endif