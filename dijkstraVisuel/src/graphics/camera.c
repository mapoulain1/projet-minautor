#include "camera.h"


#include <pthread.h>
#include <unistd.h>

void cameraInit(camera_t *out) {
	if (out) {
		out->xOffSet = 0;
		out->yOffSet = 0;
		out->tileSize = CAMERA_DEFAULT_TILESIZE;
		out->renderHitbox = false;
		out->renderDebug = false;
	}
}

void cameraUpdate(camera_t *out, player_t *player, int windowWidth, int windowHeight) {
	if (out) {
		out->xOffSet = -player->x + windowWidth / 2 - PLAYER_WIDTH / 2;
		out->yOffSet = -player->y + windowHeight / 2 - PLAYER_HEIGHT / 2;
	}
}

void cameraToggleDebug(camera_t *out) {
	if (out) {
		out->renderDebug = !out->renderDebug;
	}
}

void cameraToggleHitboxs(camera_t *out) {
	if (out) {
		out->renderHitbox = !out->renderHitbox;
	}
}

void cameraZoomIn(camera_t *out) {
	if (out) {
		out->tileSize += CAMERA_ZOOM_STEP;
	}
}

void cameraZoomOut(camera_t *out) {
	if (out) {
		out->tileSize -= CAMERA_ZOOM_STEP;
    }
}



void * cameraZoomInAnimatedInternal(camera_t *out){
	int frame = 0;
	while (frame < CAMERA_ZOOM_SCALE)
	{
		cameraZoomIn(out);
		usleep(CAMERA_ZOOM_SLEEP_MICROSEC);
		frame++;
	}
	return NULL;
}

void * cameraZoomOutAnimatedInternal(camera_t *out){
	int frame = 0;
	while (frame < CAMERA_ZOOM_SCALE)
	{
		cameraZoomOut(out);
		usleep(CAMERA_ZOOM_SLEEP_MICROSEC);
		frame++;
	}
	return NULL;
}


void cameraZoomInAnimated(camera_t *out){
	pthread_t threadAnimation;
	pthread_create (&threadAnimation, NULL, (void *(*)(void *))cameraZoomInAnimatedInternal, (void*)out);
}



void cameraZoomOutAnimated(camera_t *out){
	pthread_t threadAnimation;
	pthread_create (&threadAnimation, NULL, (void *(*)(void *))cameraZoomOutAnimatedInternal, (void*)out);
}