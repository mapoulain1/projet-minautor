#ifndef CAMERA_H_
#define CAMERA_H_

#include "../utils/bool.h"

#define CAMERA_DEFAULT_TILESIZE 40
#define CAMERA_ZOOM_STEP 1

#define CAMERA_ZOOM_SLEEP_MICROSEC 10000
#define CAMERA_ZOOM_SCALE 24


typedef struct{
    int xOffSet;
    int yOffSet;
    int tileSize;
    bool renderDebug;
    bool renderHitbox;
}camera_t;

#include "../world/internal/player.h"

void cameraInit(camera_t *out);
void cameraUpdate(camera_t * out,player_t *player,int windowWidth,int windowHeight);
void cameraToggleDebug(camera_t *out);
void cameraToggleHitboxs(camera_t *out);
void cameraZoomIn(camera_t *out);
void cameraZoomOut(camera_t *out);

void * cameraZoomInAnimatedInternal(camera_t *out);
void * cameraZoomOutAnimatedInternal(camera_t *out);

void cameraZoomInAnimated(camera_t *out);
void cameraZoomOutAnimated(camera_t *out);



#endif