#ifndef PARCOURS_GRAPHE
#define PARCOURS_GRAPHE

#include <stdio.h>
#include <stdlib.h>

#include "graph.h"
#include "tas.h"

int *dijkstra(graphe_t *G, int r, int dest, int width, SDL_Renderer *renderer);
/**
 * @brief Donne toutes les distances d'un sommet à la racine et les parents d'un sommet
 * 
 * @param G le graphe d'entrée
 * @param r la sommet racine
 * @param width la largeur du labyrinthe
 *
 * @return parent un tableau d'int des parents des sommets
 */

void initParcours(int r, int nbNoeuds, int *dist, int *parent, tas_t *tas, int *dejaTraite);
/**
 * @brief Initialisation pour ls algo de parcours
 * 
 * @param r la sommet racine
 * @param nbNoeuds le nombre de sommets
 * @param dist un tableau d'int des distances
 * @param parent un tableau d'int des parents des sommets
 * @param tas un tas minimum
 * @param dejaTraite un tableau d'entiers : 1 si deja traité, 0 sinon
 */

void relacher(int u, int v, int *dist, int *parent, tas_t * tas);
/**
 * @brief Relâche 2 sommets
 * @param u un sommet
 * @param v un sommet
 * @param dist un tableau d'int des distances
 * @param parent un tableau d'int des parents des sommets
 * @param tas un tas min
 */

int *aEtoile(graphe_t *G, int r, double (*fdist)(int a, int b, int width), int width, int dest, SDL_Renderer *renderer);
/**
 * @brief Donne toutes les distances d'un sommet à la racine et les parents d'un sommet
 * 
 * @param G le graphe d'entrée
 * @param r la sommet racine
 * @param fdist la focntion de calcul des distances
 * @param width la largeur du labyrinthe
 * @param dest la destination
 *
 * @return parent un tableau d'int des parents des sommets
 */

int *parcours(graphe_t *G, int r, double (*fdist)(int a, int b, int width), int with, int dest, SDL_Renderer *renderer);
/**
 * @brief Donne toutes les distances d'un sommet à la racine et les parents d'un sommet
 * 
 * @param G le graphe d'entrée
 * @param r la sommet racine
 * @param fdist la focntion de calcul des distances
 * @param width la largeur du labyrinthe
 *
 * @return parent un tableau d'int des parents des sommets
 */

void DFS(graphe_t *G, int r, int width, SDL_Renderer * renderer);
/**
 * @brief Effectue un parcours en profondeur
 * 
 * @param G le graphe d'entrée
 * @param r la sommet racine
 * @param width la largeur du labyrinthe
 */

void DFS_rec(graphe_t *G, int r, int *dejaTraite, int width, SDL_Renderer * renderer);
/**
 * @brief Effectue un parcours en profondeur
 * 
 * @param G le graphe d'entrée
 * @param r la sommet racine
 * @param width la largeur du labyrinthe
 * @param dejaTraite le tableau des sommets déjà traités
 */

#endif
