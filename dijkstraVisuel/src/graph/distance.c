#include "distance.h"
#include "../utils/math.h"
#include <math.h>
#include <stdlib.h>

double euclidienne(int a, int b, int width)
{
    int x1, y1, x2, y2;
    array1Dto2D(a, &x1, &y1, width);
    array1Dto2D(b, &x2, &y2, width);
    double sum = (double)(x2 - x1) * (x2 - x1)  + (y2 - y1) * (y2 - y1);
    return sqrt(sum);
}


double tchebychev(int a, int b, int width)
{
    int x1, y1, x2, y2;
    array1Dto2D(a, &x1, &y1, width);
    array1Dto2D(b, &x2, &y2, width);
    return (double) max( abs(x1 - x2), abs(y1 - y2) );
}

double manhattan(int a, int b, int width)
{
    int x1, y1, x2, y2;
    array1Dto2D(a, &x1, &y1, width);
    array1Dto2D(b, &x2, &y2, width);
    return (double) abs(x2 - x1) + abs(y2 - y1);
}
