

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <unistd.h>

#include "game.h"
#include "graphics/graphics.h"


int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	SDL_Window *window;
	SDL_Renderer *renderer;

	
	config_t config = configNew(15*15);

	gcsInit();
	gcsNewWindow("Parcours", 601, 601, &window);
	gcsNewRenderer(window, &renderer);

	gameSetup(config);
	gameStart(window);

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();

	return 0;
}
