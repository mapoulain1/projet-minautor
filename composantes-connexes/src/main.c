#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "graph.h"

int main() {
  int ** M;
  partition_t P;
  
  // on crée une matrice qui représente un graphe
  M = creerMatrice();
  for (int i = 0; i < TAILLE; i++) {
    for (int j = 0; j < TAILLE; j++) {
      printf("%d ", M[i][j]);
    }
    printf("\n");
  }
  // calcul des composantes connexes de la matrice
  P = graphesConnexesMat(M);
  partitionAfficherPretty(&P);
  displayGraphMat(M);

  // on crée un graphe sous forme d'un couple
  graphe_t * G = creerGraphe();
  //calcul des composantes connexes du graphe
  P = graphesConnexesGraphe(G);
  partitionAfficherPretty(&P);
  displayGraphGraph(G);

  // Arbre couvrant
  printf("Arbre couvrant\n");
  graphe_t * G2 = kruskal(G, NULL, 0);
  displayGraphGraph(G2);
  printf("Avec l'algorithme de Fisher-Yates\n");     
  G2 = kruskal(G, fisherYates, 0);
  displayGraphGraph(G2);

  printf("Avec une probabilité de 0.5\n");
  graphe_t * G3 = kruskal(G, NULL, 0.5);
  displayGraphGraph(G3);

  printf("Avec une probabilité de 1\n");
  graphe_t * G4 = kruskal(G, NULL, 1.0);
  displayGraphGraph(G4);

  // libération
  libererMat(M);
  partitionLiberer(&P);
  libererGraphe(G);
  libererGraphe(G2);
  libererGraphe(G3);
  libererGraphe(G4);

  
  return 0;
}
