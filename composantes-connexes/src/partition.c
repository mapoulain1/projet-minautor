#include "partition.h"

#include <stdio.h>
#include <stdlib.h>

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

/**
 * @brief Crée la partition où chaque élément est seul dans sa classe
 * 
 * @param size Le nombre d'éléments
 * @return partition_t La partition
 */
partition_t partitionCreer(int size) {
	partition_t partition;
	partition.size = size;
	partition.data = malloc(sizeof(element_t) * size);
	if (!partition.data) {
		partition.size = 0;
	} else {
		partition.heights = malloc(sizeof(int) * size);
		if (!partition.heights) {
			partition.size = 0;
			free(partition.data);
		} else {
			partition.class = malloc(sizeof(int) * size);
			if (!partition.class) {
				partition.size = 0;
				free(partition.data);
				free(partition.heights);
			} else {

				for (int i = 0; i < size; i++) {
					partition.data[i] = i;
					partition.class[i] = true;
					partition.heights[i] = 1;
				}
			}
		}
	}
	return partition;
}

/**
 * @brief Libère la mémoire pour une partition
 * 
 * @param partition La partition
 */
void partitionLiberer(partition_t *partition) {
	if (partition) {
		partition->size = 0;
		if (partition->data)
			free(partition->data);
		if (partition->heights)
			free(partition->heights);
		if (partition->class)
			free(partition->class);
	}
}

/**
 * @brief Affiche une partition
 * 
 * @param partition La partition
 */
void partitionAfficher(partition_t *partition) {
	if (partition) {
		printf("Value\t\tPartition\tHeight\t\tRoot\n");
		for (int i = 0; i < partition->size; i++) {
			if (partition->heights[i] > 1)
				printf("%d\t\t%d\t\t%d\t\t%s\n", i, partition->data[i], partition->heights[i], partition->class[i] ? "true" : "false");
			else
				printf("%d\t\t%d\t\t \t\t%s\n", i, partition->data[i], partition->class[i] ? "true" : "false");
		}
	}
}

/**
 * @brief Renvoie un identifiant unique de la classe de x 
 * 
 * @param p La partition
 * @param x Un élément de la partition
 * @return identifier_t 
 */
identifier_t partitionRecupererClasse(partition_t *p, element_t x) {
	identifier_t id = BAD_IDENTIFIER;
	if (p) {
		id = p->data[x];
		while (id != p->data[id]) {
			id = p->data[id];
		}
	}
	return id;
}

/**
 * @brief Fusionne les classes de x et y
 * 
 * @param p La partition en entrée sortie
 * @param x Un élément de la partition
 * @param y Un autre élément de la partition
 */
void partitionFusion(partition_t *p, element_t x, element_t y) {
	identifier_t classX, classY;
	if (p) {
		if (p->data[x] != y && p->data[y] != x && x != y) {
			classX = partitionRecupererClasse(p, x);
			classY = partitionRecupererClasse(p, y);
			if (classX != classY) {
				if (p->heights[classX] > p->heights[classY]) {
					p->data[classY] = p->data[classX];
					p->class[classY] = false;
					p->heights[classX] = MAX(p->heights[classY] + 1, p->heights[classX]);
				} else {
					p->data[classX] = p->data[classY];
					p->class[classX] = false;
					p->heights[classY] = MAX(p->heights[classY], p->heights[classX] + 1);
				}
			}
		}
	}
}

/**
 * @brief Liste les éléments de la classe
 * 
 * @param p La partition
 * @param id L'identifiant d'une classe
 * @return element_t* La liste des éléments de la classe
 */
element_t *partitionListerClasse(partition_t *p, identifier_t id) {
	element_t *elems = NULL;
	int index = 0;
	identifier_t currentClass;
	if (p) {
		elems = malloc(sizeof(element_t) * (p->size + 1));
		if (elems) {
			for (int i = 0; i < p->size; i++) {
				currentClass = partitionRecupererClasse(p, p->data[i]);
				if (currentClass == id) {
					elems[index] = i;
					index++;
				}
			}
			elems[index] = BAD_IDENTIFIER;
		}
	}
	return elems;
}

/**
 * @brief Renvoie la liste des classes
 * 
 * @param p La partition
 * @return identifier_t* la liste des identifiant de toutes les classes
 */
bool *partitionListerPartition(partition_t *p) {
	return p->class;
}

/**
 * @brief Afficher une partition de façon lisible
 * 
 * @param p La partition
 */
void partitionAfficherPretty(partition_t *p) {
	bool *classes = partitionListerPartition(p);
	element_t *elements;
	int index;

	for (int i = 0; i < p->size; i++) {
		if (classes[i]) {
			index = 0;
			elements = partitionListerClasse(p, i);
			printf("Classe %d : ", i);
			while (elements[index] != BAD_IDENTIFIER) {
				printf("%d  ", elements[index]);
				index++;
			}
			free(elements);
			printf("\n");
		}
	}
}

/**
 * @brief Exporte une partition en fichier .dot
 * 
 * @param p La partition
 * @param filename Le nom du fichier .dot
 */
void partitionToDotFile(partition_t *p, char filename[]) {
	FILE *flot;
	if (p && filename) {
		flot = fopen(filename, "w");
		if (!flot) {
			fprintf(stderr, "Can't open file\n");
			return;
		}
		fprintf(flot, "digraph MyGraph {\n");
		for (int i = 0; i < p->size; i++) {
			fprintf(flot, "    %d -> %d;\n", i, p->data[i]);
		}
		fprintf(flot, "}\n");
		fclose(flot);
	}
}

/**
 * @brief Génère et affiche la partition à partir du .dot
 * 
 * @param filename Fichier .dot
 */
void partitionOpenImage(char filename[]) {
	char tmp[1024];
	int random = rand();
	if (filename) {
		sprintf(tmp, "dot %s -T" IMAGE_FORMAT " > graph_%d." IMAGE_FORMAT " ; " IMAGE_VIEWER " graph_%d." IMAGE_FORMAT, filename, random, random);
		system(tmp);
	}
}

/**
 * @brief Supprime les fichiers .dot et .svg
 * 
 */
void partitionCleanFiles(void) {
	system("rm *.dot *." IMAGE_FORMAT);
}
