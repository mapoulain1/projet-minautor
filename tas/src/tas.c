#include "tas.h"


tas_t * initTas () {
  tas_t * tas = (tas_t *)malloc(sizeof(tas_t));
  tas->tab = (int *)malloc(TAILLE * sizeof(int));
  tas->n = 0;
  return tas;
}



int estPlein (tas_t * tas) {
  return tas->n >= TAILLE - 1;
}



int estVide (tas_t * tas) {
  return tas->n == 0;
}  



int enfiler (tas_t * tas, int val) {
  if (!estPlein(tas)) {
    int i = tas->n;
    while (i > 0 && val < tas->tab[(int)(i-1)/2]) {
      tas->tab[i] = tas->tab[(int)(i-1)/2];
      i = (int)(i-1)/2;
    }
    tas->tab[i] = val;
    (tas->n)++;
    return 0;
  }
  else {
    printf("Erreur : le tas est plein\n");
    return -1;
  }    
}



int defiler (tas_t * tas) {
  if (!estVide(tas)) {
    int k = tas->tab[0]; // on récupère la valeur de la racine
    (tas->n)--;
    int s = tas->n;
    int val = tas->tab[s];
    int i = 0;
    while ((2*i+1 < s && val > tas->tab[2*i+1]) || (2*i+2 < s && val > tas->tab[2*i+2])) {
      if (2*i+2 < s && tas->tab[2*i+1] > tas->tab[2*i+2]) {
	tas->tab[i] = tas->tab[2*i+2];
	i = 2*i+2;
      }
      else {
	tas->tab[i] = tas->tab[2*i+1];
	i = 2*i+1;
      }
    }
    tas->tab[i] = val;
    return k;
  }
  else {
    printf("Erreur : le tas est vide\n");
    return -1;
  }  
}  



tas_t * tabToTas (int * T, int taille) {
  tas_t * tas = initTas();
  free(tas->tab);
  tas->tab = T;
  tas->n = taille;
  for (int i = (int)tas->n/2; i >= 0; i--) {
    entasser(tas, i, tas->n);
  }  
  return tas;
}


void triParTas (tas_t * tas) {
  for (int i = tas->n - 1; i >= 1; i--) {
    permuter(tas, 0, i);
    entasser(tas, 0, i);
  }
}  



void permuter (tas_t * tas, int a, int b) {
  int tmp = tas->tab[a];
  tas->tab[a] = tas->tab[b];
  tas->tab[b] = tmp;
}  



void entasser (tas_t * tas, int i, int taille) {
  int min = 0;
  if (2*i+1 <= taille - 1 && tas->tab[2*i+1] < tas->tab[i]) {
    min = 2*i+1;
  }
  else {
    min = i;
  }
  if (2*i+2 <= taille - 1 && tas->tab[2*i+2] < tas->tab[min]) {
    min = 2*i+2;
  }
  if (min != i) {
    permuter(tas, i, min);
    entasser(tas, min, taille);
  }  
}  



void libererTas(tas_t * tas)
{
    free(tas->tab);
    free(tas);
}
