#ifndef TAS
#define TAS

#include <stdio.h>
#include <stdlib.h>

#define TAILLE 100000


/*****************************************************************/
/* Les tas sont généralement représentés sous forme de tableau.  */
/* Etant donné un noeud i, on sait que :                         */
/* - son fils gauche est en 2*i+1                                */
/* - son fils droit en 2*i+2                                     */
/* - son père est en (i-1)/2                                     */
/*****************************************************************/


typedef struct tas {
  int * tab; // le tableau qui contient le tas
  int n; // le nombre case utilisées (aussi l'indice de la première case vide)
} tas_t;


tas_t * initTas ();

int estPlein (tas_t * tas);

int estVide (tas_t * tas);

int enfiler (tas_t * tas, int val);

int defiler (tas_t * tas);

tas_t * tabToTas (int * T, int i);

void triParTas (tas_t * tas);

void permuter (tas_t * tas, int a, int b);

void entasser (tas_t * tas, int i, int taille);

void libererTas(tas_t * tas);

#endif
