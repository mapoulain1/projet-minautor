#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tas.h"


int cmpfunc (const void * a, const void * b) {
   return ( *(int*)b - *(int*)a );
}


int main () {
  
  printf("On part d'un tas vide :\n");
  tas_t * tas = initTas();
  printf("on enfile 1, sortie = %d\n", enfiler(tas, 1));
  printf("on enfile 7, sortie = %d\n", enfiler(tas, 7));
  printf("on enfile 2, sortie = %d\n", enfiler(tas, 2));
  printf("on enfile 4, sortie = %d\n", enfiler(tas, 4));
  printf("on enfile 8, sortie = %d\n", enfiler(tas, 8));
  printf("on enfile 5, sortie = %d\n", enfiler(tas, 5));
  printf("on defile : %d\n", defiler(tas));
  printf("on defile : %d\n", defiler(tas));
  printf("on defile : %d\n", defiler(tas));
  printf("on defile : %d\n", defiler(tas));
  printf("on defile : %d\n", defiler(tas));
  printf("on defile : %d\n", defiler(tas));

  
  printf("\n///\n\nOn part d'un tableau :\n");
  int tab[] = {10, 21, 12, 43, 4, 15};
  int taille = 6;
  for (int i=0; i<taille; i++) {
    printf("%d ", tab[i]);
  }
  printf("\n\non le transforme en tas et on défile :\n");
  tas_t * tas2 = tabToTas(tab, taille);
  while (tas2->n > 0) {
    for (int i=0; i<tas2->n; i++) {
      printf("%d ", tas2->tab[i]);
    }
    printf("\non defile : %d\n\n", defiler(tas2));
  }

  int tab2[100000];
  taille = 100000;
  printf("///\n\nAvec un tableau de taille %d :\n", taille);
  for (int i = 0; i < taille; i++) {
    tab2[i] = i;
  }  
  clock_t tps_av = clock();
  tas_t * tas3 = tabToTas(tab2, taille);
  triParTas(tas3);
  clock_t tps_ap = clock();
  printf("Il a fallu %f s avec notre algorithme pour trier le tableau\n", (double) (tps_ap - tps_av) / CLOCKS_PER_SEC);

  tps_av = clock();
  qsort(tab2, taille, sizeof(int), cmpfunc);
  tps_ap = clock();
  printf("Il a fallu %f s avec qsort pour trier le tableau\n", (double) (tps_ap - tps_av) / CLOCKS_PER_SEC);
  
  libererTas(tas);
  free(tas2);
  free(tas3);
  
  return 0;
}  
