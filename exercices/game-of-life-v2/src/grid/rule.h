#ifndef RULE_H_
#define RULE_H_

#include "../utils/bool.h"

#define TWOXTWO "125", "36"
#define THREEFOURLIFE "34", "34"
#define AMOEBA "1358", "357"
#define ASSIMILATION "4567", "345"
#define COAGULATION "23678", "378"
#define CONWAYSLIFE "23", "3"
#define CORAL "45678", "3"
#define DAYNIGHT "34678", "3678"
#define DIAMOEBA "5678", "3578"
#define FLAKES "012345678", "3"
#define GNARL "1", "1"
#define HIGHLIFE "23", "36"
#define INVERSELIFE "34678", "0123478"
#define LONGLIFE "5", "345"
#define MAZE "12345", "3"
#define MAZECTRIC "1234", "3"
#define MOVE "245", "368"
#define PSEUDOLIFE "238", "357"
#define REPLICATOR "1357", "1357"
#define STAINS "235678", "3678"
#define WALLEDCITIES "2345", "45678"

typedef struct {
	bool survivor[9];
	bool born[9];
} rule_t;

rule_t ruleNew(char survivor[], char born[]);
void rulePrint(rule_t rule);

#endif