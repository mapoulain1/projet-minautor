#ifndef GRID_H_
#define GRID_H_

#include "../utils/error.h"
#include "rule.h"
#include "tile.h"

#define DEFAULT_GRID_SIZE 140
#define DEFAULT_PERCENT 20

typedef struct {
	tile_t **array;
	tile_t **tmp;
	int size;
} grid_t;

error_t gridNew(grid_t *out, int size);
void gridFree(grid_t *out);
void gridRand(grid_t *out, int percent);
void gridProcess(grid_t *out, rule_t rule);
void gridSetTile(grid_t *out, int x, int y, tile_t state);
void gridSetTileBrush(grid_t *out, int x, int y, tile_t state, int brushsize);

#endif