#include "renderer.h"

void renderGame(grid_t grid, SDL_Renderer *renderer, int windowWidth) {
	int tileWidth = windowWidth / grid.size;
	for (int i = 0; i < grid.size; i++) {
		for (int j = 0; j < grid.size; j++) {
			renderTile(grid.array[i][j], i * tileWidth, j * tileWidth, renderer, tileWidth);
		}
	}
}

void renderTile(tile_t tile, int x, int y, SDL_Renderer *renderer, int tileWidth) {
	if (tile == ALIVE) {
		SDL_Rect rect;
		rect.x = x;
		rect.y = y;
		rect.w = rect.h = tileWidth;
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		SDL_RenderFillRect(renderer, &rect);
	}
}
