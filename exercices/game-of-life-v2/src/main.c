

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <unistd.h>

#include "game.h"
#include "graphics/graphics.h"
#include "grid/rule.h"
#include "utils/args.h"
#include "utils/config.h"
#include "utils/math.h"

int main(int argc, char const *argv[]) {

	SDL_Window *window;
	SDL_Renderer *renderer;

	config_t config = parseArgs(argc, argv);

	configPrint(config);

	gcsInit();
	gcsNewWindow("Game Of Life", 700, 700, &window);
	gcsNewRenderer(window, &renderer);

	gameSetup(config);
	gameStart(window);

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();

	return 0;
}
