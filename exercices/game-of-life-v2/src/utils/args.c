#include "args.h"

#include "../grid/grid.h"

#include <stdio.h>
#include <stdlib.h>

config_t parseArgs(int argc, char const *argv[]) {
	config_t config;
	if (argc == 4) {
		switch (atoi(argv[3])) {
		case 1:
			config = configNew(atoi(argv[1]), atoi(argv[2]), TWOXTWO);
			break;
		case 2:
			config = configNew(atoi(argv[1]), atoi(argv[2]), THREEFOURLIFE);
			break;
		case 3:
			config = configNew(atoi(argv[1]), atoi(argv[2]), AMOEBA);
			break;
		case 4:
			config = configNew(atoi(argv[1]), atoi(argv[2]), ASSIMILATION);
			break;
		case 5:
			config = configNew(atoi(argv[1]), atoi(argv[2]), COAGULATION);
			break;
		case 6:
			config = configNew(atoi(argv[1]), atoi(argv[2]), CONWAYSLIFE);
			break;
		case 7:
			config = configNew(atoi(argv[1]), atoi(argv[2]), CORAL);
			break;
		case 8:
			config = configNew(atoi(argv[1]), atoi(argv[2]), DAYNIGHT);
			break;
		case 9:
			config = configNew(atoi(argv[1]), atoi(argv[2]), DIAMOEBA);
			break;
		case 10:
			config = configNew(atoi(argv[1]), atoi(argv[2]), FLAKES);
			break;
		case 11:
			config = configNew(atoi(argv[1]), atoi(argv[2]), GNARL);
			break;
		case 12:
			config = configNew(atoi(argv[1]), atoi(argv[2]), HIGHLIFE);
			break;
		case 13:
			config = configNew(atoi(argv[1]), atoi(argv[2]), INVERSELIFE);
			break;
		case 14:
			config = configNew(atoi(argv[1]), atoi(argv[2]), LONGLIFE);
			break;
		case 15:
			config = configNew(atoi(argv[1]), atoi(argv[2]), MAZE);
			break;
		case 16:
			config = configNew(atoi(argv[1]), atoi(argv[2]), MAZECTRIC);
			break;
		case 17:
			config = configNew(atoi(argv[1]), atoi(argv[2]), MOVE);
			break;
		case 18:
			config = configNew(atoi(argv[1]), atoi(argv[2]), PSEUDOLIFE);
			break;
		case 19:
			config = configNew(atoi(argv[1]), atoi(argv[2]), REPLICATOR);
			break;
		case 20:
			config = configNew(atoi(argv[1]), atoi(argv[2]), STAINS);
			break;
		case 21:
			config = configNew(atoi(argv[1]), atoi(argv[2]), WALLEDCITIES);
			break;
		}
	} else {
		config = configNew(DEFAULT_GRID_SIZE, DEFAULT_PERCENT, CONWAYSLIFE);
	}

	return config;
}
