#ifndef CONFIG_H
#define CONFIG_H

#include "../grid/rule.h"

typedef struct {
	int worldSize;
	int percent;
	rule_t rule;
} config_t;

config_t configNew(int worldSize, int percent, char born[], char survivor[]);
void configPrint(config_t config);

#endif