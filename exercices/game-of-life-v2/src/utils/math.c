#include "math.h"

int mod(int i, int m) {
	if (i <= -1)
		return m - 1;
	return i % m;
}

int clamp(int val, int min, int max) {
	if (val > max)
		return max;
	if (val < min)
		return min;
	return val;
}