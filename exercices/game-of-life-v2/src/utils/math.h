#ifndef MATH_H_
#define MATH_H_

int mod(int i, int m);
int clamp(int val, int min, int max);

#endif