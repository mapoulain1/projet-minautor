#ifndef ARGS_H_
#define ARGS_H_

#include "config.h"

config_t parseArgs(int argc, char const *argv[]);

#endif