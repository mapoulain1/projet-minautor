#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/graphics.h"
#include "graphics/renderer.h"
#include "grid/grid.h"
#include "utils/bool.h"
#include "utils/math.h"


#define GAME_SLOWDOWN 3

config_t gameConfig;
grid_t grid;
int brushSize = 1;
int frameMS = 16;

void gameSetup(config_t config) {
	srand(time(NULL));
	gameConfig = config;
	gridNew(&grid, config.worldSize);
	gridRand(&grid, config.percent);
}

void gameStart(SDL_Window *window) {
	bool running = true;
	bool halt = false;
	int tick = GAME_SLOWDOWN;
	SDL_Event event;
	SDL_Renderer *renderer = SDL_GetRenderer(window);

	while (running) {
		gameRenderGame(window);
		tick = (tick + 1) % GAME_SLOWDOWN;

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					running = false;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					gameRenderGame(window);
					break;
				}
				break;
			case SDL_QUIT:
				running = false;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_p:
					halt = !halt;
					break;
				case SDLK_SPACE:
					halt = !halt;
					break;
				case SDLK_UP:
					brushSize = clamp(brushSize + 1, 1, __INT32_MAX__);
					printf("Brush size : %d\n", brushSize);
					break;
				case SDLK_DOWN:
					brushSize = clamp(brushSize - 1, 1, __INT32_MAX__);
					printf("Brush size : %d\n", brushSize);
					break;
				case SDLK_LEFT:
					frameMS = clamp(frameMS - 1, 1, __INT32_MAX__);
					printf("FPS : %d\n", (int)(1.0/frameMS*1000));
					break;
				case SDLK_RIGHT:
					frameMS = clamp(frameMS + 1, 1, __INT32_MAX__);
					printf("FPS : %d\n", (int)(1.0/frameMS*1000));
					break;
				case SDLK_ESCAPE:
					running = false;
					;
					break;
				}
				break;
			}
		}
		if (!halt && tick == 0)
			gridProcess(&grid, gameConfig.rule);
		handleMouse(window);
		SDL_RenderPresent(renderer);
		SDL_Delay(frameMS);
	}

	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
	gridFree(&grid);
}

void gameRenderGame(SDL_Window *window) {
	int windowWidth, windowHeight;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer);
	renderGame(grid, renderer, windowWidth);
}

void handleMouse(SDL_Window *window) {
	int windowWidth, windowHeight;
	int x, y;
	int tileSize;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_PumpEvents();
	tileSize = windowWidth / gameConfig.worldSize;
	if (SDL_GetMouseState(&x, &y) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
		gridSetTileBrush(&grid, x / tileSize, y / tileSize, ALIVE, brushSize);
	}
	if (SDL_GetMouseState(&x, &y) & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
		gridSetTileBrush(&grid, x / tileSize, y / tileSize, DEAD, brushSize);
	}
}