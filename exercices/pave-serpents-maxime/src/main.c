#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <math.h>

#include "windowMaker.h"

int SDL_RenderDrawCircle(SDL_Renderer *renderer, int x, int y, int radius) {
	int offsetx, offsety, d;
	int status;

	offsetx = 0;
	offsety = radius;
	d = radius - 1;
	status = 0;

	while (offsety >= offsetx) {
		status += SDL_RenderDrawPoint(renderer, x + offsetx, y + offsety);
		status += SDL_RenderDrawPoint(renderer, x + offsety, y + offsetx);
		status += SDL_RenderDrawPoint(renderer, x - offsetx, y + offsety);
		status += SDL_RenderDrawPoint(renderer, x - offsety, y + offsetx);
		status += SDL_RenderDrawPoint(renderer, x + offsetx, y - offsety);
		status += SDL_RenderDrawPoint(renderer, x + offsety, y - offsetx);
		status += SDL_RenderDrawPoint(renderer, x - offsetx, y - offsety);
		status += SDL_RenderDrawPoint(renderer, x - offsety, y - offsetx);

		if (status < 0) {
			status = -1;
			break;
		}

		if (d >= 2 * offsetx) {
			d -= 2 * offsetx + 1;
			offsetx += 1;
		} else if (d < 2 * (radius - offsety)) {
			d += 2 * offsety - 1;
			offsety -= 1;
		} else {
			d += 2 * (offsety - offsetx - 1);
			offsety -= 1;
			offsetx += 1;
		}
	}

	return status;
}

int main() {
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Rect rect = {0, 0, 50, 50};

	if (0 != SDL_Init(SDL_INIT_VIDEO)) {
		fprintf(stderr, "Error SDL_Init : %s", SDL_GetError());
		return -1;
	}

	rect.x = 400 - 25;
	rect.y = 400 - 25;

	if (0 == init(&window, &renderer, 800, 800)) {

		for (int i = 0; i < 360; i++) {
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderClear(renderer);
			rect.x = cos((i * 3.1415) * 180) * i + 400 - 25;
			rect.y = sin((i * 3.1415) * 180) * i + 400 - 25;
			SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
			SDL_RenderFillRect(renderer, &rect);
			SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
			SDL_RenderDrawCircle(renderer, 400, 400, i);

			SDL_RenderPresent(renderer);
			SDL_Delay(4);
		}

		for (int i = 359; i >= 0; i--) {
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderClear(renderer);
			rect.x = cos(((359 - i) * 3.1415) * 180) * i + 400 - 25;
			rect.y = sin(((359 - i) * 3.1415) * 180) * i + 400 - 25;
			SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
			SDL_RenderFillRect(renderer, &rect);
			SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
			SDL_RenderDrawCircle(renderer, 400, 400, i);

			SDL_RenderPresent(renderer);
			SDL_Delay(4);
		}
	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	return 0;
}
