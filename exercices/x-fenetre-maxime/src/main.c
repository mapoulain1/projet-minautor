#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "windowMaker.h"

#define NUMBER_OF_WINDOWS 5

int main() {
	SDL_Window *windows[NUMBER_OF_WINDOWS];
	SDL_Renderer *renderer[NUMBER_OF_WINDOWS];
	SDL_DisplayMode size;

	if (0 != SDL_Init(SDL_INIT_VIDEO)) {
		fprintf(stderr, "Error SDL_Init : %s", SDL_GetError());
		return -1;
	}

	SDL_GetCurrentDisplayMode(0, &size);

	printf("Display : %dx%d\n", size.w, size.h);

	for (int i = 0; i < NUMBER_OF_WINDOWS; i++) {
		init(&windows[i], &renderer[i], 100, 100);
		SDL_SetWindowPosition(windows[i], (size.w / NUMBER_OF_WINDOWS) * i, size.h - 100);
		SDL_SetRenderDrawColor(renderer[i], 255, 255, 255, 255);
		SDL_RenderClear(renderer[i]);
		SDL_RenderPresent(renderer[i]);
		printf("Windows : %d\n", i);
		SDL_Delay(300);
	}

	for (int j = 0; j < NUMBER_OF_WINDOWS; j++) {
		for (int i = 0; i < 40; i++) {
			SDL_SetWindowPosition(windows[j], (size.w / NUMBER_OF_WINDOWS) * j, size.h - 100 - i * 10);
			SDL_SetRenderDrawColor(renderer[j], 255, 255, 255, 255);
			SDL_RenderClear(renderer[j]);
			SDL_RenderPresent(renderer[j]);
			SDL_Delay(10);
		}
	}


	for (int j = 0; j < NUMBER_OF_WINDOWS; j++) {
		for (int i = 40; i > 0; i--) {
			SDL_SetWindowPosition(windows[j], (size.w / NUMBER_OF_WINDOWS) * j, size.h - 100 - i * 10);
			SDL_SetRenderDrawColor(renderer[j], 255, 255, 255, 255);
			SDL_RenderClear(renderer[j]);
			SDL_RenderPresent(renderer[j]);
			SDL_Delay(10);
		}
	}


	for (int i = 0; i < NUMBER_OF_WINDOWS; i++) {
		SDL_DestroyWindow(windows[i]);
		SDL_DestroyRenderer(renderer[i]);
		SDL_Delay(300);
	}

	return 0;
}
