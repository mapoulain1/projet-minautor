

#include <SDL2/SDL.h>
#include <stdlib.h>

#include "game.h"
#include "graphics/graphics.h"
#include "utils/config.h"
#include "utils/math.h"

int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	config_t config = configNew(140, 25, "3", "23");
	configPrint(config);

	SDL_Window *window;
	SDL_Renderer *renderer;

	gcsInit();
	gcsNewWindow("Game Of Life", 700, 700, &window);
	gcsNewRenderer(window, &renderer);

	gameSetup(config);
	gameStart(window);

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();

	return 0;
}
