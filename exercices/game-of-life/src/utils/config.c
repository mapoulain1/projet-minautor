#include "config.h"

#include <stdio.h>

#include "../grid/rule.h"

config_t configNew(int worldSize, int percent, char born[], char survivor[]) {
	config_t config;
	config.worldSize = worldSize;
	config.percent = percent;
	config.rule = ruleNew(born, survivor);
	return config;
}

void configPrint(config_t config) {
	printf("CONFIG :\n");
	printf("World size : %d\n", config.worldSize);
	rulePrint(config.rule);
}