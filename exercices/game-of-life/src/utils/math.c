#include "math.h"

int mod(int i, int m) {
	if (i == -1)
		return m - 1;
	return i % m;
}