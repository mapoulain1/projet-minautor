#include "graphics.h"

#include "../utils/error.h"

#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_image.h>

error_t gcsInit(void) {
	error_t error = OK;
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
		error = SDL_INIT_FAILED;
	return error;
}

void gcsFree(void) {
	SDL_Quit();
}

error_t gcsNewWindow(const char *title, int width, int height, SDL_Window **out) {
	error_t error = OK;
	*out = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE);

	if (*out == NULL) {
		error = SDL_WINDOW_FAILED;
		logError(SDL_WINDOW_FAILED);
	}
	return error;
}

void gcsFreeWindow(SDL_Window **window) {
	SDL_DestroyWindow(*window);
	*window = NULL;
}

error_t gcsNewRenderer(SDL_Window *window, SDL_Renderer **out) {
	error_t error = OK;
	*out = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (*out == NULL) {
		error = SDL_ERROR;
		logError(SDL_ERROR);
	}
	return error;
}

void gcsFreeRenderer(SDL_Renderer **renderer) {
	SDL_DestroyRenderer(*renderer);
	*renderer = NULL;
}

void gcsClearBackground(SDL_Renderer *renderer) {
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
}
