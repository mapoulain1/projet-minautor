#ifndef RENDERER_H_
#define RENDERER_H_

#include "../grid/grid.h"
#include <SDL2/SDL.h>

void renderGame(grid_t grid, SDL_Renderer *renderer, int windowWidth);
void renderTile(tile_t tile, int x, int y, SDL_Renderer *renderer, int tileWidth);

#endif