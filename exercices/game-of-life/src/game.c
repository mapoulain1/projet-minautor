#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/graphics.h"
#include "graphics/renderer.h"
#include "grid/grid.h"
#include "utils/bool.h"

#define MS_FRAME 30

config_t gameConfig;
grid_t grid;

void gameSetup(config_t config) {
	srand(time(NULL));
	gameConfig = config;
	gridNew(&grid, config.worldSize);
	gridRand(&grid, config.percent);
}

void gameStart(SDL_Window *window) {
	bool running = true;
	SDL_Event event;
	SDL_Renderer *renderer = SDL_GetRenderer(window);

	while (running) {
		gameRenderGame(window);

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					running = false;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					gameRenderGame(window);
					break;
				}
				break;
			case SDL_QUIT:
				running = false;
				break;
			}
		}

		gridProcess(&grid, gameConfig.rule);
		SDL_RenderPresent(renderer);
		SDL_Delay(MS_FRAME);
	}

	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
	gridFree(&grid);
}

void gameRenderGame(SDL_Window *window) {
	int windowWidth, windowHeight;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer);
	renderGame(grid, renderer, windowWidth);
}
