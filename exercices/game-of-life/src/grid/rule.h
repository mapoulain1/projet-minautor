#ifndef RULE_H_
#define RULE_H_

#include "../utils/bool.h"

typedef struct {
	bool survivor[9];
	bool born[9];
} rule_t;

rule_t ruleNew(char born[], char survivor[]);
void rulePrint(rule_t rule);

#endif