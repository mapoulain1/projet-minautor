#include "grid.h"

#include <stdlib.h>

#include "../utils/math.h"

error_t gridNew(grid_t *out, int size) {
	error_t error = OK;
	out->size = size;
	out->array = malloc(size * sizeof(tile_t *));
	out->tmp = malloc(size * sizeof(tile_t *));

	if (out->array == NULL || out->tmp == NULL) {
		error = ALLOC_ERROR;
	} else {
		for (int i = 0; i < size; i++) {
			out->array[i] = malloc(size * sizeof(tile_t));
			out->tmp[i] = malloc(size * sizeof(tile_t));
			if (out->array[i] == NULL || out->tmp[i] == NULL) {
				error = ALLOC_ERROR;
				break;
			}
		}
	}
	return error;
}

void gridFree(grid_t *out) {
	if (out) {
		for (int i = 0; i < out->size; i++) {
			free(out->array[i]);
			free(out->tmp[i]);
		}
		free(out->array);
		free(out->tmp);
		out->size = 0;
	}
}

void gridRand(grid_t *out, int percent) {
	if (out) {
		for (int i = 0; i < out->size; i++) {
			for (int j = 0; j < out->size; j++) {
				out->array[i][j] = (rand() % 100) < percent ? 1 : 0;
			}
		}
	}
}

void gridProcess(grid_t *out, rule_t rule) {
	int numberOfNeighboor;
	int m = out->size;
	if (out) {
		for (int i = 0; i < out->size; i++) {
			for (int j = 0; j < out->size; j++) {
				numberOfNeighboor = out->array[mod(i - 1, m)][mod(j - 1, m)] + out->array[mod(i - 1, m)][mod(j, m)] + out->array[mod(i - 1, m)][mod(j + 1, m)] + out->array[mod(i, m)][mod(j - 1, m)] + out->array[mod(i, m)][mod(j + 1, m)] + out->array[mod(i + 1, m)][mod(j - 1, m)] + out->array[mod(i + 1, m)][mod(j, m)] + out->array[mod(i + 1, m)][mod(j + 1, m)];
				if (out->array[i][j] == ALIVE)
					out->tmp[i][j] = rule.survivor[numberOfNeighboor] ? ALIVE : DEAD;
				else
					out->tmp[i][j] = rule.born[numberOfNeighboor] ? ALIVE : DEAD;
			}
		}
		for (int i = 0; i < out->size; i++) {
			for (int j = 0; j < out->size; j++) {
				out->array[i][j] = out->tmp[i][j];
			}
		}
	}
}
