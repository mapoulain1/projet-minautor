#include "rule.h"

#include "../utils/string.h"
#include <stdio.h>
#include <string.h>

rule_t ruleNew(char born[], char survivor[]) {
	rule_t rule;
	int lenBorn = strlen(born);
	int lenSurvivor = strlen(survivor);
	for (int i = 0; i < 9; i++) {
		rule.born[i] = false;
		rule.survivor[i] = false;
	}
	for (int i = 0; i < lenBorn; i++) {
		rule.born[ctoi(born[i])] = true;
	}
	for (int i = 0; i < lenSurvivor; i++) {
		rule.survivor[ctoi(survivor[i])] = true;
	}
	return rule;
}

void rulePrint(rule_t rule) {
	printf("Born : ");
	for (int i = 0; i < 9; i++) {
		if (rule.born[i]) {
			printf("%d ", i);
		}
	}
	printf("\nSurvive : ");
	for (int i = 0; i < 9; i++) {
		if (rule.survivor[i]) {
			printf("%d ", i);
		}
	}
	printf("\n");
}