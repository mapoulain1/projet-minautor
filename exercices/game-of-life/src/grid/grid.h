#ifndef GRID_H_
#define GRID_H_

#include "../utils/error.h"
#include "rule.h"
#include "tile.h"

typedef struct {
	tile_t **array;
	tile_t **tmp;
	int size;
} grid_t;

error_t gridNew(grid_t *out, int size);
void gridFree(grid_t *out);
void gridRand(grid_t *out, int percent);
void gridProcess(grid_t *out, rule_t rule);

#endif