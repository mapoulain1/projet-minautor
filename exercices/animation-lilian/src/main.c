#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

void end_sdl(char ok, char const* msg, SDL_Window * window, SDL_Renderer * renderer)
{
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
    strncpy(msg_formated, msg, 250);                                 
    l = strlen(msg_formated);                                        
    strcpy(msg_formated + l, " : %s\n");                     

    SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
    exit(EXIT_FAILURE);                                              
  } 
}


SDL_Texture * chargerTexture (const char * nomImg, SDL_Window * window, SDL_Renderer * renderer)
{
    SDL_Surface * img = NULL;
    SDL_Texture * texture = NULL;
    img = IMG_Load(nomImg);

    if (img == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    texture = SDL_CreateTextureFromSurface(renderer, img);
    SDL_FreeSurface(img);

    if (texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);
    
    return texture;
}


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_DisplayMode screen;
    SDL_Event event;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
				    
    SDL_GetCurrentDisplayMode(0, &screen);

    window = SDL_CreateWindow("Animation Lilian", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w * 0.66, screen.h * 0.66, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    /* initSDL(window, renderer, screen, "Animation Lilian"); */

    SDL_Texture * texture = chargerTexture("./textures/Plebes_Anim_Ace_Spade_1x.png", window, renderer);
    
    SDL_bool running = SDL_TRUE;

    SDL_Rect
	source = {0},
	window_dimensions = {0},
	destination = {0},
	state = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);

    int nb_img = 8;
    float zoom = 4;
    int offset_x = source.w / nb_img;
    int offset_y = source.h;

    state.x = 0;
    state.y = 0;
    state.w = offset_x;
    state.h = offset_y;

    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;

    destination.y = (window_dimensions.h - destination.h) /2;

    int x = (window_dimensions.w - destination.w) /2;

    while (running)
    {
	destination.x = x;
	state.x += offset_x;
	state.x %= source.w;
	
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, &state, &destination);
	SDL_RenderPresent(renderer);
	SDL_Delay(100);
	while (running && SDL_PollEvent(&event))
	{
	    switch (event.type)
	    {
	    case SDL_QUIT:
		running = SDL_FALSE;
		break;
	    case SDL_KEYDOWN:
		switch (event.key.keysym.sym)
		{
		case SDLK_LEFT:
		    x--;
		    break;
		case SDLK_RIGHT:
		    x++;
		}
	    default:
		break;
	    }
	}
    }
    SDL_RenderClear(renderer);

    SDL_DestroyTexture(texture);
    IMG_Quit();
    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}
