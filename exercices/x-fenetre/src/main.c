#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <SDL2/SDL.h>


#include "windowMaker.h"


int main () {
  SDL_Window *window1 = NULL;
  SDL_Window *window2 = NULL;
  SDL_Window *window3 = NULL;
  SDL_Window *window4 = NULL;
  SDL_Window *window5 = NULL;
  SDL_Renderer *renderer = NULL;
  SDL_DisplayMode size;
  const struct timespec time[] = {{0, 50000000L}};
  const struct timespec time2[] = {{0, 500000000L}};

  if (0 == init(&window1, &renderer, 250, 250)) {
    SDL_GetCurrentDisplayMode(0, &size);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, NULL);
    SDL_RenderPresent(renderer);
    for (int i=0; i<50; i++) {
      SDL_SetWindowPosition(window1, size.w/2 - 150 - i * 5, size.h/2 - 150 - i * 5);
      nanosleep(time, NULL);
    }  
  }
  if (0 == init(&window2, &renderer, 250, 250)) {
    SDL_RenderFillRect(renderer, NULL);
    SDL_RenderPresent(renderer);
    for (int i=0; i<50; i++) {
      SDL_SetWindowPosition(window2, size.w/2 - 150 - i * 5, size.h/2 - 100 + i * 5);
      nanosleep(time, NULL);
    }
  }
  if (0 == init(&window3, &renderer, 250, 250)) {
    SDL_RenderFillRect(renderer, NULL);
    SDL_RenderPresent(renderer);
    for (int i=0; i<50; i++) {
      SDL_SetWindowPosition(window3, size.w/2 - 100 + i * 5, size.h/2 - 150 - i * 5);
      nanosleep(time, NULL);
    }
  }
  if (0 == init(&window4, &renderer, 250, 250)) {
    SDL_RenderFillRect(renderer, NULL);
    SDL_RenderPresent(renderer);
    for (int i=0; i<50; i++) {
      SDL_SetWindowPosition(window4, size.w/2 - 100 + i * 5, size.h/2 - 100 + i * 5);
      nanosleep(time, NULL);
    }
  }
  if (0 == init(&window5, &renderer, 250, 250)) {
    SDL_RenderFillRect(renderer, NULL);
    SDL_RenderPresent(renderer);
  }  

  nanosleep(time2, NULL);
  SDL_DestroyWindow(window1);
  nanosleep(time2, NULL);
  SDL_DestroyWindow(window2);
  nanosleep(time2, NULL);
  SDL_DestroyWindow(window3);
  nanosleep(time2, NULL);
  SDL_DestroyWindow(window4);
  nanosleep(time2, NULL);
  SDL_DestroyWindow(window5);
  nanosleep(time2, NULL);

  return 0;
}  


