#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "windowMaker.h"

#define SPRITE_HEIGHT 426
#define SPRITE_WIDTH 240
#define NUMBER_OF_SPRITES 12
#define COLS 8
#define ROWS 2

int main() {
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Rect sources[NUMBER_OF_SPRITES];
	SDL_Rect dest = {300 - SPRITE_WIDTH / 2, 300 - SPRITE_HEIGHT / 2, SPRITE_WIDTH / 3, SPRITE_HEIGHT / 3};
	SDL_Texture *texture;
	int index = 0;

	if (0 == init(&window, &renderer, 1300, 600)) {
		texture = IMG_LoadTexture(renderer, "textures/spritesheet.png");
		if (!texture) {
			printf("Can't load image\n");
		} else {
			for (int i = 0; i < COLS; i++) {
				for (int j = 0; j < ROWS; j++) {
					if (index < NUMBER_OF_SPRITES) {
						sources[index].x = SPRITE_WIDTH * i;
						sources[index].y = SPRITE_HEIGHT * j + 1;
						sources[index].h = SPRITE_HEIGHT;
						sources[index].w = SPRITE_WIDTH;
						index++;
					}
				}
			}
		}

		for (int i = 0; i < NUMBER_OF_SPRITES*2; i++) {
			SDL_SetRenderDrawColor(renderer,0,0,0,0);
			SDL_RenderClear(renderer);
			SDL_RenderCopy(renderer, texture, &sources[i%9], &dest);
			
			SDL_RenderPresent(renderer);
			SDL_Delay(150);
		}		

		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(window);
	}

	return 0;
}
