#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char ** argv)
{
  (void) argc;
  (void) argv;

  if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
      SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
    }
  SDL_DisplayMode DM;
  SDL_GetCurrentDisplayMode(0, &DM);
  int width = DM.w;
  int height = DM.h;
  printf("%i | %i\n", width, height);

  int n = 5;
  int decal_w = width / n;
  int decal_h = height / n;

  // Créer fenêtres
  int i = 0, k = 0;
  SDL_Window ** win_gauche = malloc(n*sizeof(SDL_Window *));
  for (i=0; i<n; i++)
    {
      win_gauche[i] = SDL_CreateWindow(
				       "Fenêtre gauche",
				       decal_w * i, decal_h * i,
				       200, 100,
				       SDL_WINDOW_RESIZABLE);
      if(win_gauche[i] == NULL)
	{
	  SDL_Log("Error : SDL window gauche %i creation - %s\n", i, SDL_GetError());
	  for (k=0; k<i; k++)
	    {
	      SDL_DestroyWindow(win_gauche[i]);
	      SDL_Quit();
	      exit(EXIT_FAILURE);
	    }
	}
    }

  SDL_Delay(1000);

  // Déplacer fenêtres

  int w = width, h = height;
  for(i=0; i<n; i++)
    {
      SDL_GetWindowPosition(win_gauche[i], &w, &h);
      while(w > 0)
	{
	  w--;
	  SDL_SetWindowPosition(win_gauche[i], w, h);
	  SDL_Delay(10);
	}
    }
	  
  
  SDL_Delay(3000);

  for (i=0; i<n; i++)
    {
      SDL_DestroyWindow(win_gauche[i]);
      SDL_Quit();
      exit(EXIT_FAILURE);
    }
  SDL_Quit();
  return 0;
}
