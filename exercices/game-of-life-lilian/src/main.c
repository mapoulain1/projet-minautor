#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <time.h>

#define N 30
#define  M 30

#define POURC 40

#define MORT 0
#define VIE 1

int grille[N][M];

void end_sdl(char ok, char const* msg, SDL_Window * window, SDL_Renderer * renderer)
{
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
    strncpy(msg_formated, msg, 250);                                 
    l = strlen(msg_formated);                                        
    strcpy(msg_formated + l, " : %s\n");                     

    SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
    exit(EXIT_FAILURE);                                              
  } 
}

void init()
{
    int i, j;
    for(i=0; i<N; i++)
    {
	for(j=0; j<M; j++)
	{
	    grille[i][j] = MORT;
	    if (rand() % 100 < POURC) grille[i][j] = VIE;
	}
    }
}

void affGrille()
{
    int i, j;
    for(i=0; i<N; i++)
    {
	for(j=0; j<M; j++)
	{
	    printf("%d ", grille[i][j]);
	}
	printf("\n");
    }
    printf("\n");
}

void nvGeneration()
{
    int i, j;
    int temp[N][M];
    for(i=0; i<N; i++)
    {
	for(j=0; j<M; j++)
	{
	    temp[i][j] = grille[i][j];
	}
    }
    
    int nb_voisins;
    for(i=0; i<N; i++)
    {
	for(j=0; j<M; j++)
	{
	    nb_voisins = grille[(i+1)%N][j%M] + grille[(i-1)%N][j%M] + grille[i%N][(j-1)%M] + grille[i%N][(j+1)%M] + grille[(i+1)%N][(j+1)%M] + grille[(i+1)%N][(j-1)%M] + grille[(i-1)%N][(j+1)%M] + grille[(i-1)%N][(j-1)%M];
	    if (grille[i][j] == VIE)
	    {
		if (nb_voisins < 2 || nb_voisins > 3)
		{
		    temp[i][j] = MORT;
		}
	    }
	    else
	    {
		if (nb_voisins == 3)
		{
		    temp[i][j] = VIE;
		}
	    }
	}
    }

    for(i=0; i<N; i++)
    {
	for(j=0; j<M; j++)
	{
	    grille[i][j] = temp[i][j];
	}
    }
}

void affFenetre(SDL_Window * window, SDL_Renderer * renderer)
{
    int w = 0, h = 0;
    SDL_GetWindowSize(window, &w, &h);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    int cellule_w = w / N;
    int cellule_h = h / M;
    SDL_Rect rect;
    int i,j;
    for (i=0; i<N; i++)
    {
	for(j=0; j<M; j++)
	{
	    if (grille[i][j] == VIE)
	    {
		rect.x = i*cellule_w;
		rect.y = j*cellule_h;
		rect.w = cellule_w;
		rect.h = cellule_h;
		SDL_RenderFillRect(renderer, &rect);
	    }
	}
    }
    SDL_RenderPresent(renderer);
    /* affGrille(); */
    SDL_Delay(300);
    nvGeneration();
}
	    
    

int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;
    
    SDL_Window * window = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_DisplayMode screen;
    int running = 1;

    srand(time(NULL));

    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
    
    SDL_GetCurrentDisplayMode(0, &screen);
    
    window = SDL_CreateWindow("Game of life", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 500, 500, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    init();

    while(running)
    {
	affFenetre(window, renderer);
    }
    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}
