#ifndef IMAGE_H_
#define IMAGE_H_


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


SDL_Texture *loadPicture (const char path[], SDL_Renderer *renderer);
int printPicture (SDL_Renderer *renderer, SDL_Texture *img, int x, int y);


#endif
