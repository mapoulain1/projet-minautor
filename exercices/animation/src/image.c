#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


SDL_Texture *loadPicture (const char path[], SDL_Renderer *renderer) {
  /* Permet d'ouvrir des images autre que BITMAP comme PNG, JPG, ... */
  SDL_Texture *texture = NULL;
  SDL_Surface *tmp = IMG_Load(path);
  if (!tmp) {
    printf("Error IMG_Load : %s", SDL_GetError());
    return NULL;
  }
  texture = SDL_CreateTextureFromSurface(renderer, tmp);
  SDL_FreeSurface(tmp);
  if (NULL == texture) {
    fprintf(stderr, "Error SDL_CreateTextureFromSurface : %s", SDL_GetError());
    return NULL;
  }
  return texture;
}


int printPicture (SDL_Renderer *renderer, SDL_Texture *img, int x, int y) {
  /* affiche une image sur le renderer */
  if (NULL == img) {
    return -1;
  }
  // on l'affiche à des coordonnées précises
  if (x != -1 && y != -1) {
    SDL_Point size;
    SDL_QueryTexture(img, NULL, NULL, &size.x, &size.y);
    SDL_Rect dst;
    dst.x = x;
    dst.y = y;
    dst.h = size.x;
    dst.w = size.y;
    SDL_RenderCopy(renderer, img, NULL, &dst);
  }
  // l'image prend tout l'écran
  else {
    SDL_RenderCopy(renderer, img, NULL, NULL);
  }
  SDL_RenderPresent(renderer);
  return 0;
}
