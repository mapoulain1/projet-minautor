
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


int init(SDL_Window **window, SDL_Renderer **renderer, int w, int h) {
  /* Fonction qui initialise la fenetre */
  // init video
  if (0 != SDL_Init(SDL_INIT_VIDEO)) {
    fprintf(stderr, "Error SDL_Init : %s", SDL_GetError());
    return -1;
  }
  // init fenetre
  *window = SDL_CreateWindow("Titre fenetre", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_SHOWN);
  if (NULL == window) {
    fprintf(stderr, "Error SDL_CreateWindow : %s", SDL_GetError());
    return -1;
  }
  // init renderer
  *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED || SDL_RENDERER_PRESENTVSYNC);
  if (NULL == renderer) {
    fprintf(stderr, "Error SDL_CreateRenderer : %s", SDL_GetError());
    return -1;
  }
  // init icon
  SDL_Surface *icon = NULL;
  icon = SDL_LoadBMP("textures/icone.bmp");
  if (NULL == icon) {
    fprintf(stderr, "Error SDL_LoadBMP : %s", SDL_GetError());
    return -1;
  }
  SDL_SetWindowIcon(*window, icon);
  return 0;
}
