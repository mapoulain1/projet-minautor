#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <SDL2/SDL.h>


#include "windowMaker.h"
#include "image.h"



int main () {
  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  if (0 == init(&window, &renderer, 1024, 780)) {
    const struct timespec time[] = {{0, 500000000L}};
    SDL_Texture *gauche = loadPicture("textures/gauche.png", renderer);
    SDL_Texture *droit = loadPicture("textures/droit.png", renderer);
    SDL_Texture *fond = loadPicture("textures/fond.jpg", renderer);
    printPicture(renderer, fond, -1, -1);
    printPicture(renderer, droit, 450, 650);
    sleep(1);
    printPicture(renderer, fond, -1, -1);
    printPicture(renderer, gauche, 450, 650);
    sleep(1);
    printPicture(renderer, fond, -1, -1);
    printPicture(renderer, droit, 450, 650);
    for (int i=0; i<10; i++) {
      printPicture(renderer, fond, -1, -1);
      printPicture(renderer, droit, 450 + 5 * i, 650 - 5 * i);
      nanosleep(time, NULL);
    }
    for (int i=0; i<6; i++) {
      SDL_SetTextureAlphaMod(droit, 250 - 50 * i);
      printPicture(renderer, fond, -1, -1);
      printPicture(renderer, droit, 500, 600);
      nanosleep(time, NULL);
    }
    sleep(1);
     SDL_DestroyTexture(droit);
     SDL_DestroyTexture(gauche);
     SDL_DestroyTexture(fond);
  }
  return 0;
}  


