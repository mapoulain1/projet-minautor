#include "math.h"

float clamp(float val, float min, float max) {
	if (val > max)
		return max;
	if (val < min)
		return min;
	return val;
}