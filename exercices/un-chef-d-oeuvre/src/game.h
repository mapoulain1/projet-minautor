#ifndef GAME
#define GAME

#include "utils/config.h"
#include "utils/bool.h"


#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

void gameSetup(config_t config);

void gameStart(SDL_Window *window);

void gameRenderGame(SDL_Window *window, int windowWidth, int windowHeight, bool gameover, bool pause);

void gameEnd(SDL_Window *window);

#endif