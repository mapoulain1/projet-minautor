#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/graphics.h"
#include "graphics/musicPlayer.h"
#include "graphics/textRenderer.h"
#include "utils/bool.h"
#include "world/background.h"
#include "world/collision.h"
#include "world/enemy.h"
#include "world/healthbar.h"
#include "world/missile.h"
#include "world/spaceship.h"

#define MS_FRAME 12

config_t gameConfig;

void gameSetup(config_t config) {
	gameConfig = config;
}

void gameStart(SDL_Window *window) {
	srand(time(NULL));
	bool running = true;
	bool pause = false;
	bool showCollision = false;
	bool gameover = false;
	bool *missilesActivated;
	bool *enemiesDead;
	orientation_t shipOrientation = ORIENTATION_NORMAL;
	SDL_Event event;
	SDL_Rect shipRect;
	SDL_Rect spaceshipRect;
	SDL_Rect *enemiesRect;
	SDL_Rect *missilesRect;
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	int windowHeight, windowWidth;
	int enemyHit = -1;

	SDL_GetWindowSize(window, &windowWidth, &windowHeight);

	musicPlayerInit();
	musicPlay();

	textRendererInit();

	backgroundInit(renderer, windowHeight, windowWidth);
	spaceshipInit(renderer, windowHeight, windowWidth);
	enemiesInit(renderer, windowHeight, windowWidth);
	missilesInit(renderer);

	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					running = false;
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					SDL_GetWindowSize(window, &windowWidth, &windowHeight);
					gameRenderGame(window, windowWidth, windowHeight, gameover, pause);
					break;
				}
				break;
			case SDL_QUIT:
				running = false;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_PAGEUP:
					musicVolumeUp();
					break;
				case SDLK_PAGEDOWN:
					musicVolumeDown();
					break;
				case SDLK_m:
					musicVolumeMute();
					break;
				case SDLK_p:
					pause = !pause;
					break;
				case SDLK_F8:
					showCollision = !showCollision;
					break;
				case SDLK_F9:
					musicPlayDamage();
					break;
				case SDLK_F10:
					spaceshipKill();
					break;
				case SDLK_ESCAPE:
					running = false;
					break;
				case SDLK_SPACE:
					if (!gameover) {
						spaceshipRect = spaceshipRectGetter();
						missileShoot(spaceshipRect.x, spaceshipRect.y, spaceshipRect.w, spaceshipRect.h);
						musicPlayPewpew();
					}
					break;
				}
			}
		}

		const Uint8 *keystates = SDL_GetKeyboardState(NULL);

		gameRenderGame(window, windowWidth, windowHeight, gameover, pause);

		shipOrientation = ORIENTATION_NORMAL;
		if (keystates[SDL_SCANCODE_UP])
			shipOrientation = ORIENTATION_UP;
		if (keystates[SDL_SCANCODE_DOWN])
			shipOrientation = ORIENTATION_DOWN;

		if (!pause && !gameover) {
			backgroundUpdate(windowWidth);
			shipRect = spaceshipUpdate(windowHeight, shipOrientation);
			enemiesRect = enemiesUpdate(windowWidth, windowHeight);
			missilesUpdate(windowWidth, &missilesRect, &missilesActivated);
			enemiesDead = enemiesDeadGetter();

			if (collided(shipRect, enemiesRect, enemiesDead, renderer, showCollision)) {
				spaceshipTakeDamage();
				musicPlayDamage();
			}
			enemyHit = collidedMissiles(enemiesRect, enemiesDead, missilesRect, missilesActivated, renderer, showCollision);
			if (enemyHit != -1) {
				enemiesKill(enemyHit);
				musicPlayDamage();
			}
			for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
			   if(((double)rand()/RAND_MAX)<0.05)
					enemiesRevive(i,windowWidth,windowHeight);
			}

			if (spaceshipGetLife() <= 0)
				gameover = true;
		}

		SDL_RenderPresent(renderer);
		SDL_Delay(MS_FRAME);
	}

	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
	backgroundFree();
	spaceshipFree();
	enemiesFree();
	musicPlayerFree();
}

void gameRenderGame(SDL_Window *window, int windowWidth, int windowHeight, bool gameover, bool pause) {
	static int score = 0;
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer);
	backgroundRender(renderer);
	spaceshipRender(renderer);
	enemiesRender(renderer);
	drawHealthbar(spaceshipGetLife(), renderer);
	missilesRender(renderer);
	if (gameover) {
		textRendererGameOver(renderer, windowWidth, windowHeight);
		musicVolumeZero();
		musicPlayGameover();
		spaceshipThrusterSetter(false);
		SDL_Rect rect = spaceshipRectGetter();
		rect.h--;
		rect.w--;
		rect.x++;
		spaceshipRectSetter(rect);

	} else if (!pause) {
		score += 1;
	}
	textRendererScore(renderer, windowWidth, score);
}
