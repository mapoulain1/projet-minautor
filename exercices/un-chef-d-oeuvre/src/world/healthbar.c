#include "healthbar.h"



void drawHealthbar(float percentLife, SDL_Renderer *renderer){
    SDL_Rect border1 = {POSITION_X,POSITION_Y+SCALE_BAR,SCALE_BAR,SCALE_BAR};
    SDL_Rect border2 = {POSITION_X+SCALE_BAR,POSITION_Y,LENGHT_BAR*SCALE_BAR,SCALE_BAR};
    SDL_Rect border3 = {POSITION_X+SCALE_BAR,POSITION_Y+2*SCALE_BAR,LENGHT_BAR*SCALE_BAR,SCALE_BAR};
    SDL_Rect border4 = {POSITION_X+(LENGHT_BAR+1)*SCALE_BAR,POSITION_Y+SCALE_BAR,SCALE_BAR,SCALE_BAR};
    drawRectangle(border1,renderer, 50,50,50,200);
    drawRectangle(border2,renderer, 50,50,50,200);
    drawRectangle(border3,renderer, 50,50,50,200);
    drawRectangle(border4,renderer, 50,50,50,200);

    SDL_Rect life = {POSITION_X+SCALE_BAR,POSITION_Y+SCALE_BAR,LENGHT_BAR*percentLife*SCALE_BAR,SCALE_BAR};
    drawRectangle(life,renderer, 200,0,0,255);


}

void drawRectangle(SDL_Rect rect, SDL_Renderer *renderer, int r, int g, int b,int a){
    SDL_SetRenderDrawColor(renderer,r,g,b,a);
    SDL_RenderFillRect(renderer,&rect);
}