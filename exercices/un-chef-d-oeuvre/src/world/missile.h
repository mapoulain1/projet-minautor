#ifndef MISSILE_H_
#define MISSILE_H_

#include "../utils/error.h"
#include "../utils/bool.h"
#include <SDL2/SDL.h>

#define MISSILE_PATH "textures/missiles/Bullets.png"
#define SPRITE_WIDTH 112
#define SPRITE_HEIGHT 48
#define NB_MISSILES 4


error_t missilesInit(SDL_Renderer * renderer);

void missilesRender (SDL_Renderer * renderer);

void missilesFree();

void missilesUpdate(int windowWidth,SDL_Rect ** out, bool **outActivated);

void missileShoot(int spaceshipX, int spaceshipY, int spaceshipWidth, int spaceshipHeight);

#endif
