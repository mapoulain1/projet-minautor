#ifndef THRUSTER_H_
#define THRUSTER_H_

#include "../utils/error.h"
#include <SDL2/SDL.h>

#define THRUSTER_PATH "textures/thruster/thrusterSheet.png"
#define SHEET_COLS 10
#define SHEET_ROWS 6
#define SPRITE_WIDTH 84
#define SPRITE_HEIGHT 9

#define NUMBER_OF_SPRITES 60
#define SPACE_BETWEEN_THRUSTER 55


error_t thrusterInit(SDL_Renderer *renderer, int x, int y);
void thrusterRender(SDL_Renderer * renderer);
void thrusterFree(void);
void thrusterUpdate(int x, int y);




#endif