#include "collision.h"

#include "enemy.h"
#include "missile.h"

bool collided(SDL_Rect ship, SDL_Rect *enemies, bool *enemiesDead, SDL_Renderer *renderer, bool showCollision) {
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	bool collision = false;
	SDL_Rect shipReduced;
	SDL_Rect ennemyReduced;
	shipReduced.x = ship.x + COLLISION_HELPER_X;
	shipReduced.y = ship.y + COLLISION_HELPER_Y;
	shipReduced.w = ship.w - COLLISION_HELPER_X * 2;
	shipReduced.h = ship.h - COLLISION_HELPER_Y * 2;
	if (showCollision) {
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, 120);
		SDL_RenderFillRect(renderer, &shipReduced);
	}

	for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
		if (!enemiesDead[i]) {
			ennemyReduced.x = enemies[i].x + COLLISION_HELPER_X;
			ennemyReduced.y = enemies[i].y + COLLISION_HELPER_Y;
			ennemyReduced.w = enemies[i].w - COLLISION_HELPER_X * 2;
			ennemyReduced.h = enemies[i].h - COLLISION_HELPER_Y * 2;
			if (showCollision) {
				SDL_SetRenderDrawColor(renderer, 255, 0, 0, 180);
				SDL_RenderFillRect(renderer, &ennemyReduced);
			}
			if (!collision && SDL_HasIntersection(&shipReduced, &ennemyReduced))
				collision = true;
		}
	}
	return collision;
}

int collidedMissiles(SDL_Rect *enemies, bool *enemiesDead, SDL_Rect *lasers, bool *activated, SDL_Renderer *renderer, bool showCollision) {
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	SDL_Rect ennemyReduced;
	for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
		if (!enemiesDead[i]) {
			ennemyReduced.x = enemies[i].x + COLLISION_HELPER_X;
			ennemyReduced.y = enemies[i].y + COLLISION_HELPER_Y;
			ennemyReduced.w = enemies[i].w - COLLISION_HELPER_X * 2;
			ennemyReduced.h = enemies[i].h - COLLISION_HELPER_Y * 2;
			for (int j = 0; j < NB_MISSILES; j++) {
				if (activated[i]) {
					if (showCollision) {
						SDL_SetRenderDrawColor(renderer, 0, 0, 255, 120);
						SDL_RenderFillRect(renderer, &lasers[j]);
					}
					if (SDL_HasIntersection(&lasers[j], &ennemyReduced) == SDL_TRUE) {
						return i;
					}
				}
			}
		}
	}
	return -1;
}