#include "enemy.h"

#include "../utils/bool.h"
#include "../utils/math.h"
#include "../utils/string.h"
#include <SDL2/SDL_image.h>

struct {
	SDL_Rect rects[NUMBER_OF_ENEMIES];
	SDL_Texture *texture[NUMBER_OF_ENEMIES];
	float internalX[NUMBER_OF_ENEMIES];
	float internalY[NUMBER_OF_ENEMIES];
	float speedVerti[NUMBER_OF_ENEMIES];
	float speedHori[NUMBER_OF_ENEMIES];
	bool dead[NUMBER_OF_ENEMIES];
} enemies;

error_t enemiesInit(SDL_Renderer *renderer, int windowHeight, int windowWidth) {

	char tmp[MAX_STRING_SIZE];
	for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
		enemies.internalX[i] = 0;
		sprintf(tmp, "%s%d.png", ENEMY_PATH, i);
		enemies.texture[i] = IMG_LoadTexture(renderer, tmp);
		if (enemies.texture[i] == NULL) {
			logError(FILE_NOT_FOUND);
		}
		SDL_QueryTexture(enemies.texture[i], NULL, NULL, &enemies.rects[i].w, &enemies.rects[i].h);
		enemies.rects[i].w /= 6;
		enemies.rects[i].h /= 6;

		enemies.internalX[i] = rand() % (windowWidth / 2) + windowWidth / 2;
		enemies.internalY[i] = rand() % (windowHeight - enemies.rects[i].h);

		enemies.rects[i].x = enemies.internalX[i];
		enemies.rects[i].y = enemies.internalY[i];

		enemies.speedVerti[i] = ((double)rand() / RAND_MAX) * VERTI_SPEED - VERTI_SPEED_OFFSET;
		enemies.speedHori[i] = ((double)rand() / RAND_MAX) * HORI_SPEED + HORI_SPEED_OFFSET;

		enemies.dead[i] = false;
	}
	return OK;
}

void enemiesRender(SDL_Renderer *renderer) {
	for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
		if (!enemies.dead[i])
			SDL_RenderCopy(renderer, enemies.texture[i], NULL, &enemies.rects[i]);
	}
}

void enemiesFree(void) {
	for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
		SDL_DestroyTexture(enemies.texture[i]);
	}
}

SDL_Rect *enemiesUpdate(int windowWidth, int windowHeight) {
	for (int i = 0; i < NUMBER_OF_ENEMIES; i++) {
		if (!enemies.dead[i]) {
			if ((double)rand() / RAND_MAX * 100 < CHANGE_DIRECTION_CHANCE) {
				enemies.speedVerti[i] = ((double)rand() / RAND_MAX) * VERTI_SPEED - VERTI_SPEED_OFFSET;
			}

			enemies.internalX[i] -= enemies.speedHori[i];
			enemies.internalY[i] += enemies.speedVerti[i];

			if (enemies.internalX[i] < -enemies.rects[i].w) {
				enemies.internalX[i] = windowWidth;
			}

			enemies.internalX[i] = clamp(enemies.internalX[i], -enemies.rects[i].w, windowWidth);
			enemies.internalY[i] = clamp(enemies.internalY[i], 0, windowHeight - enemies.rects[i].h);

			enemies.rects[i].x = enemies.internalX[i];
			enemies.rects[i].y = enemies.internalY[i];
		}
	}
	return enemies.rects;
}

void enemiesKill(int index) {
	enemies.dead[index] = true;
}

void enemiesRevive(int index, int windowWidth, int windowsHeight) {
	if (enemies.dead[index]) {
		enemies.dead[index] = false;
		enemies.internalX[index] = windowWidth + 25;
		enemies.internalY[index] = (((double)rand()/RAND_MAX) *0.8 +0.1 ) * windowsHeight;

		enemies.rects[index].x = enemies.internalX[index];
		enemies.rects[index].y = enemies.internalY[index];
	}
}

bool *enemiesDeadGetter(void) {
	return enemies.dead;
}