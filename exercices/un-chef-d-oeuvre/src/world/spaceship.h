#ifndef SPACESHIP_H_
#define SPACESHIP_H_

#include "../utils/error.h"
#include "../utils/bool.h"
#include <SDL2/SDL.h>

#define SPACESHIP_PATH "textures/ship/spaceship"
#define THRUSTER_OFFSET 35
#define DAMAGE_TAKEN 0.006


typedef enum {ORIENTATION_NORMAL, ORIENTATION_UP, ORIENTATION_DOWN}orientation_t;



SDL_Rect spaceshipRectGetter(void);
void spaceshipRectSetter(SDL_Rect rect);
error_t spaceshipInit(SDL_Renderer *renderer, int windowHeight, int windowWidth);
void spaceshipRender(SDL_Renderer * renderer);
void spaceshipFree(void);
SDL_Rect spaceshipUpdate(int windowHeight, orientation_t orientation);
float spaceshipGetLife(void);
void spaceshipTakeDamage(void);
void spaceshipKill(void);
void spaceshipThrusterSetter(bool state);

#endif