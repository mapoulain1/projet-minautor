#ifndef HEALTHBAR_H_
#define HEALTHBAR_H_

#include <SDL2/SDL.h>

#define SCALE_BAR 12
#define LENGHT_BAR 25
#define POSITION_X 50
#define POSITION_Y 20


void drawHealthbar(float percentLife, SDL_Renderer *renderer);

void drawRectangle(SDL_Rect rect, SDL_Renderer *renderer, int r, int g, int b,int a);


#endif