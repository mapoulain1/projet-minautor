#include "spaceship.h"

#include "../utils/string.h"
#include "../utils/bool.h"
#include "../utils/math.h"
#include <SDL2/SDL_image.h>
#include "thruster.h"

struct
{
	float internalY;
	SDL_Rect rect;
	SDL_Texture *texture[3];
	orientation_t orientation;
	float life;
	bool thrusterEnabled;
} ship;




SDL_Rect spaceshipRectGetter(void){
	return ship.rect;
}

void spaceshipRectSetter(SDL_Rect rect){
	ship.rect=rect;
}


error_t spaceshipInit(SDL_Renderer *renderer, int windowHeight, int windowWidth) {
	char tmp[MAX_STRING_SIZE];
	for (int i = 0; i < 3; i++) {
		sprintf(tmp, "%s%d.png", SPACESHIP_PATH, i);
		ship.texture[i] = IMG_LoadTexture(renderer, tmp);
		if (ship.texture[i] == NULL) {
			logError(FILE_NOT_FOUND);
			return FILE_NOT_FOUND;
		}
	}
	SDL_QueryTexture(ship.texture[0], NULL, NULL, &ship.rect.w, &ship.rect.h);
	ship.rect.w /= 6;
	ship.rect.h /= 6;
	ship.rect.x = windowWidth / 10;
	ship.internalY = windowHeight / 2 + ship.rect.h / 2;
	ship.rect.y = ship.internalY;
	ship.life=1;
	ship.thrusterEnabled = true;
	return thrusterInit(renderer,ship.rect.x - THRUSTER_OFFSET, ship.rect.y);
}

void spaceshipRender(SDL_Renderer *renderer) {
	switch (ship.orientation) {
	case ORIENTATION_NORMAL:
		SDL_RenderCopy(renderer, ship.texture[0], NULL, &ship.rect);
		break;
	case ORIENTATION_UP:
		SDL_RenderCopy(renderer, ship.texture[1], NULL, &ship.rect);
		break;
	case ORIENTATION_DOWN:
		SDL_RenderCopy(renderer, ship.texture[2], NULL, &ship.rect);
		break;
	}
	if(ship.thrusterEnabled)
    	thrusterRender(renderer);
}

void spaceshipFree(void) {
	for (int i = 0; i < 3; i++) {
		SDL_DestroyTexture(ship.texture[i]);
	}
    thrusterFree();
}

SDL_Rect spaceshipUpdate(int windowHeight, orientation_t orientation) {
	ship.orientation = orientation;

    if(orientation == ORIENTATION_UP)
        ship.internalY -= 1.5;
        
    if(orientation == ORIENTATION_DOWN)
        ship.internalY += 1.5;
    ship.internalY=clamp(ship.internalY,0,windowHeight-ship.rect.h);
	ship.rect.y = ship.internalY;
    thrusterUpdate(ship.rect.x - THRUSTER_OFFSET, ship.rect.y + ship.rect.h/2 - 4);
    return ship.rect;
}

float spaceshipGetLife(void){
	return ship.life;
}
void spaceshipTakeDamage(void){
	ship.life=clamp(ship.life-DAMAGE_TAKEN,0,1);
}

void spaceshipKill(void){
	ship.life=-0.001;
}

void spaceshipThrusterSetter(bool state){
	ship.thrusterEnabled=state;
}