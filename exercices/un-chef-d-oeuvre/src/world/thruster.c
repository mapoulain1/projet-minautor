#include "thruster.h"

#include <SDL2/SDL_image.h>



struct
{
	SDL_Rect source[60];
	SDL_Rect destUp;
	SDL_Rect destDown;
	SDL_Texture *texture;
} flame;

error_t thrusterInit(SDL_Renderer *renderer, int x, int y) {
	int spriteNumber = 0;
	flame.texture = IMG_LoadTexture(renderer, THRUSTER_PATH);
	if (flame.texture == NULL) {
		logError(FILE_NOT_FOUND);
		return FILE_NOT_FOUND;
	}

	flame.destUp.x = x;
	flame.destUp.y = y + SPACE_BETWEEN_THRUSTER / 2;
	flame.destUp.w = SPRITE_WIDTH;
	flame.destUp.h = SPRITE_HEIGHT;

	flame.destDown.x = x;
	flame.destDown.y = y - SPACE_BETWEEN_THRUSTER / 2;
	flame.destDown.w = SPRITE_WIDTH;
	flame.destDown.h = SPRITE_HEIGHT;

	for (int i = 0; i < SHEET_COLS; i++) {
		for (int j = 0; j < SHEET_ROWS; j++) {
			flame.source[spriteNumber].w = SPRITE_WIDTH;
			flame.source[spriteNumber].h = SPRITE_HEIGHT;
			flame.source[spriteNumber].x = i * SPRITE_WIDTH;
			flame.source[spriteNumber].y = j * SPRITE_HEIGHT;

			spriteNumber++;
		}
	}

	return OK;
}

void thrusterRender(SDL_Renderer *renderer) {
	SDL_RenderCopy(renderer, flame.texture, &flame.source[SDL_GetTicks() % NUMBER_OF_SPRITES], &flame.destUp);
    SDL_RenderCopy(renderer, flame.texture, &flame.source[SDL_GetTicks() % NUMBER_OF_SPRITES], &flame.destDown);
}

void thrusterFree(void) {
	SDL_DestroyTexture(flame.texture);
}

void thrusterUpdate(int x, int y) {
	flame.destUp.x = x;
	flame.destUp.y = y + SPACE_BETWEEN_THRUSTER / 2;
	flame.destDown.x = x;
	flame.destDown.y = y - SPACE_BETWEEN_THRUSTER / 2;
}
