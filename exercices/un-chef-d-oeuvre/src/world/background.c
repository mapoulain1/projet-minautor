#include "background.h"

#include "../utils/string.h"
#include <SDL2/SDL_image.h>


struct {
	SDL_Rect rects[NUMBER_OF_BACKGROUNDS];
	SDL_Texture *texture[NUMBER_OF_BACKGROUNDS];
	float internalX[NUMBER_OF_BACKGROUNDS];
} backgrounds;

error_t backgroundInit(SDL_Renderer *renderer, int windowHeight, int windowWidth) {
	char tmp[MAX_STRING_SIZE];
	for (int i = 0; i < NUMBER_OF_BACKGROUNDS; i++) {
		backgrounds.internalX[i] = 0;
		sprintf(tmp, "%s%d.png", BACKGROUND_PATH, i);
		backgrounds.texture[i] = IMG_LoadTexture(renderer, tmp);
		if (backgrounds.texture[i] == NULL) {
			logError(FILE_NOT_FOUND);
		}
		SDL_QueryTexture(backgrounds.texture[i], NULL, NULL, &backgrounds.rects[i].w, &backgrounds.rects[i].h);
		backgrounds.rects[i].w *= 3;
		backgrounds.rects[i].h *= 3;

		backgrounds.rects[i].x = rand() % windowWidth;
		backgrounds.rects[i].y = rand() % (windowHeight-backgrounds.rects[i].h/3);
	}
	backgrounds.rects[0].x = 0;
	backgrounds.rects[0].y = 0;

	return OK;
}

void backgroundRender(SDL_Renderer *renderer) {
	for (int i = 0; i < NUMBER_OF_BACKGROUNDS; i++) {
		SDL_RenderCopy(renderer, backgrounds.texture[i], NULL, &backgrounds.rects[i]);
	}
}

void backgroundFree(void) {
	for (int i = 0; i < NUMBER_OF_BACKGROUNDS; i++) {
		SDL_DestroyTexture(backgrounds.texture[i]);
	}
}

void backgroundUpdate(int windowWitdh) {
	backgrounds.internalX[0] -= 0.2;
	backgrounds.internalX[1] -= 0.4;
	backgrounds.internalX[2] -= 0.8;
	backgrounds.internalX[3] -= 1.4;
	backgrounds.internalX[4] -= 2.6;
	if(backgrounds.internalX[0]< -backgrounds.rects[0].w/2)
		backgrounds.internalX[0]=0;
	
	backgrounds.rects[0].x = backgrounds.internalX[0];
	for (int i = 1; i < NUMBER_OF_BACKGROUNDS; i++) {
		backgrounds.rects[i].x = backgrounds.internalX[i];
		if (backgrounds.internalX[i] < -backgrounds.rects[i].w) {
			backgrounds.internalX[i] = windowWitdh;
		}
	}
}