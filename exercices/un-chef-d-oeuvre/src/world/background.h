#ifndef BACKGROUND_H_
#define BACKGROUND_H_

#include "../utils/error.h"
#include <SDL2/SDL.h>


#define BACKGROUND_PATH "textures/background/background"
#define NUMBER_OF_BACKGROUNDS 5

error_t backgroundInit(SDL_Renderer *renderer, int windowHeight, int windowWidth);
void backgroundRender(SDL_Renderer * renderer);
void backgroundFree(void);
void backgroundUpdate(int windowWitdh);





#endif