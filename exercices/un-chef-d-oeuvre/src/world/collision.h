#ifndef COLLISION_H_
#define COLLISION_H_


#include "../utils/bool.h"
#include <SDL2/SDL.h>


#define COLLISION_HELPER_X 45
#define COLLISION_HELPER_Y 25

bool collided(SDL_Rect ship, SDL_Rect *enemies, bool *enemiesDead, SDL_Renderer *renderer, bool showCollision);
int collidedMissiles(SDL_Rect *enemies, bool *enemiesDead, SDL_Rect *lasers, bool *activated, SDL_Renderer *renderer, bool showCollision);


#endif