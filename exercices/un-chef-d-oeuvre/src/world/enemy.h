#ifndef ENEMY_H_
#define ENEMY_H_


#include "../utils/error.h"
#include "../utils/bool.h"
#include <SDL2/SDL.h>

#define NUMBER_OF_ENEMIES 5
#define ENEMY_PATH "textures/enemies/enemy"


#define VERTI_SPEED 2
#define VERTI_SPEED_OFFSET 1
#define HORI_SPEED 4
#define HORI_SPEED_OFFSET 0.4

#define CHANGE_DIRECTION_CHANCE 1


error_t enemiesInit(SDL_Renderer *renderer, int windowHeight, int windowWidth);
void enemiesRender(SDL_Renderer * renderer);
void enemiesFree(void);
SDL_Rect* enemiesUpdate(int windowWitdh, int windowHeight);
void enemiesKill(int index);
void enemiesRevive(int index, int windowWidth, int windowsHeight);
bool *enemiesDeadGetter(void);

#endif