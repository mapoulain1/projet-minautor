#include "missile.h"

#include "../utils/bool.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

struct
{
	SDL_Rect rect[NB_MISSILES];
	bool visible[NB_MISSILES];
	SDL_Texture *texture;
} missile;

error_t missilesInit(SDL_Renderer *renderer) {
	missile.texture = IMG_LoadTexture(renderer, MISSILE_PATH);
	if (missile.texture == NULL) {
		logError(FILE_NOT_FOUND);
		return FILE_NOT_FOUND;
	}

	for (int i = 0; i < NB_MISSILES; i++) {
		missile.visible[i] = false;
		missile.rect[i].w = SPRITE_WIDTH / 4;
		missile.rect[i].h = SPRITE_HEIGHT / 4;
		missile.rect[i].x = -SPRITE_WIDTH;
		missile.rect[i].y = 0;
	}
	return OK;
}

void missilesRender(SDL_Renderer *renderer) {
	for (int i = 0; i < NB_MISSILES; i++) {
		if (missile.visible[i])
			SDL_RenderCopy(renderer, missile.texture, NULL, &missile.rect[i]);
	}
}

void missilesFree() {
	SDL_DestroyTexture(missile.texture);
}

void missilesUpdate(int windowWidth, SDL_Rect **out, bool **outActivated) {
	for (int i = 0; i < NB_MISSILES; i++) {
		if (missile.visible[i]) {
			missile.rect[i].x += 6;
			if (missile.rect[i].x >= windowWidth) {
				missile.visible[i] = false;
			}
		}
	}
	*out = missile.rect;
	*outActivated = missile.visible;
}

void missileShoot(int spaceshipX, int spaceshipY, int spaceshipWidth, int spaceshipHeight) {
	for (int i = 0; i < NB_MISSILES; i++) {
		if (!missile.visible[i]) {
			missile.visible[i] = true;
			missile.rect[i].y = spaceshipY + spaceshipHeight / 2 - SPRITE_HEIGHT / 8;
			missile.rect[i].x = spaceshipX + spaceshipWidth;
            break;
		}
	}
}
