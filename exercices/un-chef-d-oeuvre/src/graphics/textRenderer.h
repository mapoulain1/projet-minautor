#ifndef TEXTRENDERER_H_
#define TEXTRENDERER_H_


#define PATH_TO_FONT "fonts/font.ttf"


#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

void textRendererInit(void);
void textRendererGameOver(SDL_Renderer *renderer, int windowWidth, int windowHeight);
void textRendererScore(SDL_Renderer *renderer, int windowWidth, int scoreValue);
void textRendererFree(void);

#endif