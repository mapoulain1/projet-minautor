#include "musicPlayer.h"

#include "../utils/bool.h"
#include "../utils/math.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

struct {
	Mix_Music *music;
	Mix_Chunk *gameover;
	Mix_Chunk *damage;
	Mix_Chunk *lasers[3];
	
} player;

int volume = MIX_MAX_VOLUME;
bool muted = false;

error_t musicPlayerInit(void) {
	
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) {
		logError(SDL_ERROR);
		return SDL_ERROR;
	}
	Mix_AllocateChannels(3);
	player.music = Mix_LoadMUS(PATH_TO_MAIN_THEME);
	player.gameover = Mix_LoadWAV(PATH_TO_GAMEOVER);
	player.damage = Mix_LoadWAV(PATH_TO_DAMAGE);
	player.lasers[0] = Mix_LoadWAV(PATH_TO_LASER_1);
	player.lasers[1] = Mix_LoadWAV(PATH_TO_LASER_2);
	player.lasers[2] = Mix_LoadWAV(PATH_TO_LASER_3);

	return OK;
}

void musicPlay(void) {
	Mix_PlayMusic(player.music, -1);
}

void musicPause(void) {
	Mix_PauseMusic();
}

void musicPlayerFree(void) {
	Mix_FreeMusic(player.music);
	Mix_FreeChunk(player.gameover);
	Mix_FreeChunk(player.damage);
	Mix_FreeChunk(player.lasers[0]);
	Mix_FreeChunk(player.lasers[1]);
	Mix_FreeChunk(player.lasers[2]);

	Mix_CloseAudio();
}

void musicVolumeDown(void) {
	volume = clamp(volume - VOLUME_STEP, 0, MIX_MAX_VOLUME);
	Mix_VolumeMusic(volume);
}

void musicVolumeUp(void) {
	volume = clamp(volume + VOLUME_STEP, 0, MIX_MAX_VOLUME);
	Mix_VolumeMusic(volume);
}

void musicVolumeMute(void) {
	muted = !muted;
	Mix_VolumeMusic(muted ? 0 : volume);
}

void musicVolumeZero(void) {
	Mix_VolumeMusic(0);
}

void musicPlayGameover(void) {
	static bool alreadyPlayed=false;
	if (!alreadyPlayed && !Mix_Playing(0)){
		Mix_PlayChannel(0, player.gameover, 0);
		alreadyPlayed=true;
	}
}

void musicPlayDamage(void) {
	if (!Mix_Playing(1))
		Mix_PlayChannel(1, player.damage, 0);
}

void musicPlayPewpew(void) {
	if (!Mix_Playing(2))
		Mix_PlayChannel(2, player.lasers[SDL_GetTicks()%3], 0);
}

