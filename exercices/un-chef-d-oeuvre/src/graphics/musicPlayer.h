#ifndef MUSICPLAYER_H_
#define MUSICPLAYER_H_

#define PATH_TO_MAIN_THEME "audio/musics/mainTheme.mp3"
#define PATH_TO_GAMEOVER "audio/sounds/gameover.wav"
#define PATH_TO_DAMAGE "audio/sounds/damage.wav"
#define PATH_TO_LASER_1 "audio/sounds/laser1.wav"
#define PATH_TO_LASER_2 "audio/sounds/laser2.wav"
#define PATH_TO_LASER_3 "audio/sounds/laser3.wav"




#define VOLUME_STEP 10


#include "../utils/error.h"




error_t musicPlayerInit(void);
void musicPlay(void);
void musicPause(void);
void musicPlayerFree(void);
void musicVolumeDown(void);
void musicVolumeUp(void);
void musicVolumeMute(void);
void musicVolumeZero(void);
void musicPlayGameover(void);
void musicPlayDamage(void);
void musicPlayPewpew(void);



#endif