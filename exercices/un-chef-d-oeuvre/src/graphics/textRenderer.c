#include "textRenderer.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "../utils/string.h"

struct {
	SDL_Surface *gameover;
	SDL_Surface *score;
	TTF_Font *font;
} textRenderer;

void textRendererInit(void) {
	textRenderer.font = TTF_OpenFont(PATH_TO_FONT, 24);
	if (!textRenderer.font) {
		printf("%s\n", TTF_GetError());
		return;
	}

	textRenderer.gameover = TTF_RenderText_Solid(textRenderer.font, "GAME OVER", (SDL_Color){255, 255, 255, 255});
	if (!textRenderer.gameover) {
		printf("%s\n", TTF_GetError());
	}
}

void textRendererGameOver(SDL_Renderer *renderer, int windowWidth, int windowHeight) {
	if (textRenderer.font) {
		SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, textRenderer.gameover);
		SDL_Rect dest = {windowWidth / 2 - 200, windowHeight / 2-50, 400, 100};
		SDL_RenderCopy(renderer, texture, NULL, &dest);
		SDL_DestroyTexture(texture);
	}
}

void textRendererScore(SDL_Renderer *renderer, int windowWidth, int scoreValue) {
	if (textRenderer.font) {
		char tmp[MAX_STRING_SIZE];
		sprintf(tmp, "%5d", scoreValue);

		textRenderer.score = TTF_RenderText_Solid(textRenderer.font, tmp, (SDL_Color){255, 255, 255, 255});
		if (!textRenderer.score) {
			printf("%s\n", TTF_GetError());
		}

		SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, textRenderer.score);
		SDL_Rect dest = {windowWidth / 2, 15, 120, 50};
		SDL_RenderCopy(renderer, texture, NULL, &dest);
		SDL_DestroyTexture(texture);
	}
}

void textRendererFree(void) {
	SDL_FreeSurface(textRenderer.gameover);
	SDL_FreeSurface(textRenderer.score);
	TTF_CloseFont(textRenderer.font);
}
