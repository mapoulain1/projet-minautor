

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <unistd.h>

#include "game.h"
#include "graphics/graphics.h"
#include "utils/config.h"

int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	SDL_Window *window;
	SDL_Renderer *renderer;
	config_t config = {};

	gcsInit();
	gcsNewWindow("The Chef D'oeuvre", 1200, 675, &window);
	gcsNewRenderer(window, &renderer);

	gameSetup(config);
	gameStart(window);

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();

	return 0;
}
