#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL2/SDL.h>


#include "windowMaker.h"



int main () {
  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  if (0 == init(&window, &renderer, 1024, 780)) {
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.h = 1024;
    rect.w = 780;
    int i = 0;
    Uint8 a = 255;
    Uint8 r = 0;
    Uint8 g = 0;
    Uint8 b = 0;
    SDL_RenderFillRect(renderer, &rect);
    SDL_RenderPresent(renderer);
    while (i < 5) {
      rect.x = 250 + 100 * i;
      rect.y = 150 + 100 * i;
      rect.h = 200 - 20 * i;
      rect.w = 200 - 20 * i;
      b += 50;
      SDL_SetRenderDrawColor(renderer, r, g, b, a);
      SDL_RenderFillRect(renderer, &rect);
      SDL_RenderPresent(renderer);
      sleep(1);
      i++;
    }
  }
  return 0;
}  


