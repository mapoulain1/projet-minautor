#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <string.h>

void end_sdl(char ok, char const* msg, SDL_Window * window, SDL_Renderer * renderer)
{
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
    strncpy(msg_formated, msg, 250);                                 
    l = strlen(msg_formated);                                        
    strcpy(msg_formated + l, " : %s\n");                     

    SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
    exit(EXIT_FAILURE);                                              
  } 
}

void draw(SDL_Renderer * renderer, int i, int j)
{
  SDL_Rect rectangle;

  SDL_SetRenderDrawColor(renderer, 0xef, 0x53, 0x50, 255);
  rectangle.x = i;
  rectangle.y = j;
  rectangle.w = 400;
  rectangle.h = 225;
  SDL_RenderFillRect(renderer, &rectangle);
}


int main(int argc, char ** argv)
{
  (void) argc;
  (void) argv;

  int i = 0;
  int j = 0;

  SDL_Window * window = NULL;
  SDL_Renderer * renderer = NULL;
  SDL_DisplayMode screen;

  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
				    
  SDL_GetCurrentDisplayMode(0, &screen);

  window = SDL_CreateWindow("Premier dessin", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w * 0.66, screen.h * 0.66, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

  int dir_x = 1;
  int dir_y = 1;
  int w = 1600, h = 900;

  SDL_Event event;
  SDL_bool running = SDL_TRUE;
  
  while(running)
    {
      SDL_GetWindowSize(window, &w, &h);
      if (i + 400 > w)
	{
	  dir_x = -1;
	}
      else
	{
	  if (i < 0) dir_x = 1;
	}
      if (j+255 > h)
	{
	  dir_y = -1;
	}
      else
	{
	  if (j < 0) dir_y = 1;
	}
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
      SDL_RenderClear(renderer);
      draw(renderer, i, j);
      SDL_RenderPresent(renderer);
      SDL_Delay(10);
      i = i + dir_x;
      j = j + dir_y;
      while (running && SDL_PollEvent(&event))
	{
	    switch (event.type)
	    {
	    case SDL_QUIT:
		running = SDL_FALSE;
		break;
	    default:
		break;
	    }
	}
    }

  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;
}
