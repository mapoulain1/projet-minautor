#!/bin/bash



RED='\033[0;31m'
GREEN='\033[0;32m'
RESET='\033[0m'

projects="exercices/animation/ exercices/animation-maxime/ exercices/animation-lilian/ exercices/pave-serpents/ exercices/pave-serpents-maxime/ exercices/pave-serpents-lilian/ exercices/game-of-life-v2/ exercices/un-chef-d-oeuvre/"


for p in $projects; do
    make clean -C "$p" 2>&1 >/dev/null
    make -C "$p" 2>&1 >/dev/null
    echo "$p"
    if [[ "$?" -eq 0 ]]; then
        printf "${GREEN}OK${RESET}\n"
    else
        printf "${RED}ERROR${RESET}\n"
    fi
    echo
done

for p in $projects; do
    read
    cd "$p"
    ./out
    cd ../..
done



